#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Contains code taking care of database schema changes.
"""

import logging
logger = logging.getLogger("dscng.schema_update")
logging.basicConfig(level=logging.DEBUG)
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'

from dsc_storage.psql.db_management import update_db_schema_version_record

class DBSchemaUpdater(object):
    
    @classmethod
    def sync_db_schema(cls, conn, goal_version):
        """
        Compares the database schema version with the goal version and performs
        required actions
        """
        db_version = conn.get_db_schema_version()
        if db_version == goal_version:
            # nothing to do
            logger.info("The database schema already has the desired version "
                         "(%d)", goal_version)
            return True
        elif db_version < goal_version:
            logger.info("The database schema needs updating. Performing "
                         "update from %d to %d", db_version, goal_version)
            ok = cls.update_db_schema(conn, db_version, goal_version)
            return ok
        else:
            # the db schema is newer than the code - we have a problem
            logger.critical("Sorry, but the database schema is newer than the "
                             "code (database: %d, code: %d).\n"
                             "This means that you have to update the code to a "
                             "newer version.\n"
                             "There is nothing this script can do for you :(",
                             db_version, goal_version)
            return False
            
    @classmethod
    def update_db_schema(cls, conn, curr_version, goal_version):
        """
        Performs update based on supplied version numbers 
        """
        assert curr_version < goal_version
        while curr_version < goal_version:
            logger.debug("Updating from %d to %d",
                          curr_version, curr_version+1)
            meth_name = "_update_from_%d_to_%d" % (curr_version, curr_version+1)
            if hasattr(cls, meth_name):
                meth = getattr(cls, meth_name)
                if callable(meth):
                    # ok, we really have a method
                    ok = meth(conn)
                    if ok:
                        curr_version += 1
                        update_db_schema_version_record(conn, curr_version)
                    else:
                        logger.error("Update from %d to %d was not successful",
                                      curr_version, curr_version+1)
                        return False
                else:
                    logger.error("No method available to update from %d to %d",
                                  curr_version, curr_version+1)
                    return False
            else:
                logger.error("No method available to update from %d to %d",
                              curr_version, curr_version+1)
                return False
        return True
            
        
    @classmethod
    def _update_from_0_to_1(cls, conn):
        """
        Dummy test function
        """
        print "Updating from 0 to 1"
        return True

    @classmethod
    def _update_from_1_to_2(cls, conn):
        """
        From servers to server groups
        """
        print
        print "** Sorry, but there is no automatic method for this update. **"
        print "** Please recreate the database schema and reimport your data.**"
        print
        return False

    @classmethod
    def _update_from_2_to_3(cls, conn):
        """
        Add missing src-port-zero query classification.
        """
        cursor = conn.cursor()
        cursor.execute("""UPDATE dscng_dimension
        SET meta = '{"values": ["ok", "non-auth-tld", "root-servers.net", "localhost", "a-for-a", "a-for-root", "rfc1918-ptr", "funny-qclass", "funny-qtype", "src-port-zero"]}'
        WHERE id = 10""")
        conn.commit()
        cursor.close()

        return True


def main():
    import sys
    from dsc_storage.psql.version import DB_SCHEMA_VERSION
    from dsc_storage.psql.connection import \
                        create_connection_manager_from_settings
    from dsc.dsc import settings
    # create connection manager
    conn = create_connection_manager_from_settings(settings,
                                                   ignore_version_error=True)
    # perform the update to the current version
    ok = DBSchemaUpdater.sync_db_schema(conn, DB_SCHEMA_VERSION)
    if not ok:
        print >> sys.stderr, "Update to version %d was not successful!" %\
            DB_SCHEMA_VERSION
    else:
        print >> sys.stderr, "Update to version %d was successful." %\
            DB_SCHEMA_VERSION
            
if __name__ == "__main__":
    main()