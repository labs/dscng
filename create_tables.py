#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
A helper script for creation of the database structure for DSCng
"""

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
from dsc.dsc import settings

from dsc_storage.psql import db_management as dbmgmt
from dsc_storage.psql.connection import create_connection_manager_from_settings

# some basic constants
BASE_PATH = os.path.abspath(os.path.dirname(__file__))
BASE_SQL_FILE = BASE_PATH + "/data/db.sql"

  
def main():
    """
    The main function - in fact the whole code of the module
    """
    import logging
    # parse options
    from optparse import OptionParser
    opsr = OptionParser(usage="python %prog [options]")
    opsr.add_option("-n", "--no-constraints", action="store_true",
                    dest="no_constraints", default=False,
                    help="Do not create database constrains - can speed up "\
                         "initial import of data. You have to run with "\
                         "--constraints-only (-c) when your are finished with "\
                         "the import.")
    opsr.add_option("-c", "--constraints-only", action="store_true",
                    dest="constraints_only", default=False,
                    help="Only add database constraints - used as second step "\
                         "when --no-constraints (-n) was used in the first "\
                         "one.")
    opsr.add_option("-d", "--data-types-only", action="store_true",
                    dest="data_types_only", default=False,
                    help="Only sync the data types - do not touch the database"\
                         " structure at all")
    opsr.add_option("-t", "--test", action="store_true",
                    dest="test", default=False,
                    help="Only print the commands what would be run - "\
                         "do not do anything to the database")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Print extra info about what the script is doing")
    (options, _args) = opsr.parse_args()
    
    # setup logging
    level = logging.INFO
    if options.verbose:
        level = logging.DEBUG
    logging.basicConfig(level=level, format="%(levelname)s: %(message)s")
    
    # check if the arguments make sense together
    if options.constraints_only and options.no_constraints:
        opsr.error("The --no-constraints (-n) and --constraints-only (-c) "\
                   "arguments are mutually exclusive - you cannot use both at "\
                   "once.")
    
    # go ahead with the actual work
    conn = create_connection_manager_from_settings(settings,
                                                   ignore_version_error=True)
    if not options.data_types_only:
        if options.constraints_only:
            # we do only this small part
            logging.info("Creating constraints only")
            dbmgmt.import_base_sql_file(conn, BASE_SQL_FILE, 
                                        part=dbmgmt.SQL_IMPORT_CONSTRAINTS,
                                        test_only=options.test)
        else:
            # we do also the dropping and creating of views and additional
            # tables
            other = dbmgmt.check_other_connections(conn)
            if other:
                logging.warn("%d other non-idle connection(s) to the database - "
                             "process might get blocked",
                             len(other))
            dbmgmt.drop_additional(conn, test_only=options.test)
            run_mode = dbmgmt.SQL_IMPORT_ALL
            if options.no_constraints:
                logging.info("Skipping constraints creation")
                run_mode = dbmgmt.SQL_IMPORT_STRUCTURE
            else:
                logging.info("Creating both structure and constraints")
            dbmgmt.import_base_sql_file(conn, BASE_SQL_FILE, part=run_mode,
                                        test_only=options.test)
            dbmgmt.create_additional(conn, test_only=options.test)

    # create data types and dimensions
    logging.info("Creating data types")
    from dsc_storage.psql.data_types import DataTypeManager
    dtm = DataTypeManager(conn)
    dtm.load_data_type_defs_from_json(BASE_PATH + "/data/data_types.json",
                                      BASE_PATH + "/data/dimensions.json")
    
    conn.close()

if __name__ == "__main__":
    main()
