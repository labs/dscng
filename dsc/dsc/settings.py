from django.core.urlresolvers import reverse_lazy
import os
import sys

# Django settings for dsc project.

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASE_ENGINE = 'postgresql_psycopg2'
# these are defaults that have to be overridden in settings_local.py
DATABASE_NAME = 'dsc'
DATABASE_USER = 'dsc'
DATABASE_PASSWORD = '**SECRET**'
DATABASE_HOST = 'localhost'
DATABASE_PORT = ''

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Prague'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# PATH_BASE defines the base path for this dsc instance (i.e. project root)
PATH_BASE = os.path.dirname(os.path.abspath(__file__ + "/.."))

# SCRIPT_NAME of the DSC instance, should be same as WSGIScriptAlias in
# the Apache configuration (without trailing slash).
SCRIPT_NAME = '/'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = PATH_BASE + '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PATH_BASE + '/plots/static',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = '**** CHANGE ME ****'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.tz',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    # 'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'plots.context_processors.url_base',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'dsc.urls'

WSGI_APPLICATION = 'dsc.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    #'/usr/share/pyshared/django/contrib/admin/templates/',
    PATH_BASE + '/plots/templates',
    PATH_BASE + '/plots/templatetags/templates',
    PATH_BASE + '/debug_toolbar/templates',
)

AUTH_USER_MODEL = 'plots.User'

USE_AUTHENTICATION = True

LOGIN_URL = reverse_lazy("home")

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'plots',
)

INTERNAL_IPS = ('127.0.0.1',)

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

TEST_RUNNER = 'plots.test_tools.DscTestRunner'

try:
    from settings_local import *
except ImportError:
    print >> sys.stderr, "Local settings were not found - using the defaults."
    print >> sys.stderr, "This is probably an error!"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': DATABASE_HOST,
        'PORT': DATABASE_PORT,
    }
}

if len(sys.argv) > 1 and 'runserver' in sys.argv[1]:
    SCRIPT_NAME = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
MEDIA_URL = SCRIPT_NAME.rstrip('/') + '/media/'

# URL prefix for static files.
STATIC_URL = SCRIPT_NAME.rstrip('/') + '/static/'

if DEBUG:
    try:
        import django_extensions
    except ImportError:
        pass
    else:
        INSTALLED_APPS += ('django_extensions',)