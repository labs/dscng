# This is an example file. Rename it to settings_local.py and change it
# as needed

# This file contains settings that are unique to your DSCng installation
# Please change anything that is different in your setup

# These people might receive some mail in case of problems with the web
ADMINS = (
    ('John Smith', 'example.mail@example.domain'),
)
MANAGERS = ADMINS

# Database definition
# If your database is hosted on another server, please change the DATABASE_HOST
# You should of cause change at least the DATABASE_PASSWORD
DATABASE_NAME = 'dsc'
DATABASE_USER = 'dsc'
DATABASE_PASSWORD = '**CHANGE ME**!'
DATABASE_HOST = 'localhost'
DATABASE_PORT = ''

# SCRIPT_NAME of the DSC instance, should be same as WSGIScriptAlias in
# the Apache configuration (without trailing slash).
SCRIPT_NAME = '/dscng'

# Set to False to disable authentication and make the site public.
USE_AUTHENTICATION = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Prague'

# Make this unique, and don't share it with anybody.
# This key is used for hashing purposes and should be unique,
# unpredictable and long
SECRET_KEY = '*** CHANGE ME ***!'

# If you want to see debugging info in case of an error, set DEBUG to True
# otherwise leave it on False as it is more secure - it does not reveal
# details of bugs to the outer world
DEBUG = False
TEMPLATE_DEBUG = DEBUG

# In production mode (i.e. DEBUG = False) it's necessary to add all the
# allowed host names of your server to this list. For more information
# and syntax, see official Django docs:
# https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']