"use strict";
/*jsl:option explicit*/

/* use the DscUI object resides here */

DailyData.prototype = new DscUI();
DailyData.constructor = DailyData;

function DailyData() {
    DscUI.call(this);
    this.visualizationDataUrl = "daily_data";
    this.dateReady = false;
    this._busyTimer = null;
    this.csvDataUrl = "get_daily_data_csv";
    this.defaultDataType = "qtype_vs_tld";
}

DailyData.prototype.init = function(element) {
    this.topElement = element;
    /* create the page layout */
    $(this.topElement).append("<div id='layout-top'>DSCng</div>");
    $(this.topElement).append("<div id='layout-left'></div>");
    $(this.topElement).append("<div id='layout-main'>" +
            "<div id='layout-main-top'></div>" +
            "<div id='layout-main-center'></div>" +
            "</div>");
    $(this.topElement).append("<div id='layout-bottom'>Footer</div>");
    this.outerLayout = $(this.topElement).layout({
        center__paneSelector: "#layout-main",
        west__paneSelector: "#layout-left",
        north__paneSelector: "#layout-top",
        south__paneSelector: "#layout-bottom",
        west__size: 120,
        east__size: 200,
        spacing_open: 2, // ALL panes
        spacing_closed: 8, // ALL panes
        north__spacing_open: 0,
        south__spacing_open: 0,
        north__maxSize: 100,
        south__maxSize: 100,
        initClosed: false,
        north__initHidden: true,
        south__initHidden: true,
        east__initClosed: true,
        applyDefaultStyles: true,
        slidable: false
    });
    this.innerLayout = $("#layout-main").layout({
        center__paneSelector: "#layout-main-center",
        north__paneSelector: "#layout-main-top",
        north__resizable: false,
        spacing_open: 2, // ALL panes
        spacing_closed: 8, // ALL panes
        initClosed: false,
        applyDefaultStyles: true,
        south__minSize: 120,
        south__size: 240,
        south__initClosed: true,
        slidable: false
    });
    /* fill in the elements that we need later */
    /* list of server names */
    $("#layout-left").append(
            "<div id='server_list_parent'>" +
            "<div class='ui-widget-header'>Servers</div>" +
            "<div id='server_list'></div>" +
            "</div>");
    /* list of available data types */
    $("#layout-main-top").append("<div id='data_type_list'>" +
            "<div id='data_type_div'" +
            "class='ui-widget-header'>Data types</div>" +
            "<span id='data_type_select_div'></span>" +
            "<span id='date_pickers'><label for='start_date_input'>From:</label>" +
            "<input id='start_date_input'></input>" +
            "<label for='end_date_input'>To:</label>" +
            "<input id='end_date_input'></input></span>" +
            "</div>");
    $("#start_date_input")
        .datepicker({
            dateFormat: "yy-mm-dd"
        })
        .change(function(){
            var input = $("#start_date_input");
            try {
                var startDate = $.datepicker.parseDate("yy-mm-dd", input.val());
            }
            catch (exc) {
                alert("Invalid start date.");
                return;
            }
            var endDateInput = $("#end_date_input");
            var endDate = $.datepicker.parseDate("yy-mm-dd", endDateInput.val());
            if (startDate > endDate) {
                endDateInput.val(input.val());
            }
            this.updateDetails();
    }.bind(this));

    $("#end_date_input")
        .datepicker({
            dateFormat: "yy-mm-dd"
        })
        .change(function () {
            var input = $("#end_date_input");
            try {
                var endDate = $.datepicker.parseDate("yy-mm-dd", input.val());
            }
            catch (exc) {
                alert("Invalid end date.");
                return;
            }
            var startDateInput = $("#start_date_input");
            var startDate = $.datepicker.parseDate("yy-mm-dd", startDateInput.val());
            if (startDate > endDate) {
                startDateInput.val(input.val());
            }
            this.updateDetails();
        }.bind(this));

    /* the chart itself */
    $("#layout-main-center").append(
            "<div id='chart_parent'>" +
            "<div id='chart_drag_handle' class='right'>" +
            "<span id='chart_menu'></span></div>" +
            "<div id='chart_div'></div>" +
            "<div id='chart_additional'></div>" +
            "</div>");
    $('#chart_menu')
        // reload button
        .append($("<span>Reload</span>").button().click(
                this.updateDetails.bind(this))
            .css({"font-size": "8pt"}))
        // CSV download
        .append($("<span>CSV</span>").button().click(
                this.downloadChartAsCSV.bind(this))
            .css({"font-size": "8pt"}));
    /* a widget for details about a concrete time */
    /* call some async requests for data */
    /* fetch available date range and prefil the date picker */
    jQuery.getJSON(this.getDataSourceUrl("available_date_range"),
                    {},
                    this.availableDateRangeLoadCallback.bind(this));
    /* fetch available data types */
    jQuery.getJSON(this.getDataSourceUrl("data_types"),
                    {daily: true},
                    this.dataTypesLoadCallback.bind(this));
    /* fetch servers */
    jQuery.getJSON(this.getDataSourceUrl("servers"),
                    this.serversLoadCallback.bind(this));
};

DailyData.prototype.initializeGraph = function(){
    /* this method checks if all necessary data is loaded 
     * - the visualization API, the servers and the data types
     * and then loads the initial data
     */
    this.updateDetails();
};

DailyData.prototype.availableDateRangeLoadCallback = function(data){
    if (data.min_date != null && data.max_date != null) {
        var min_date = $.datepicker.parseDate("yy-mm-dd", data.min_date);
        var max_date = $.datepicker.parseDate("yy-mm-dd", data.max_date);
        $("#start_date_input, #end_date_input")
            .datepicker("option", "minDate", min_date)
            .datepicker("option", "maxDate", max_date)
            .datepicker("setDate", max_date);
        this.dateReady = true;
    } else {
        this.dateReady = null;
    }
};

DailyData.prototype.handleQueryResponse = function(response){
    if (this.checkForRedirectRequest(response)) return;
    this.deactivateBusyIndicator();
    var extra = response.extra;
    /* check if the data type we received matches the currently chosen
     * otherwise we have probably received data that were requested
     * earlier and are no longer needed - we log it and leave 
     */
    if (extra.data_type != $("#data_type_select").val()) {
        console.log("wrong data type in response - maybe stale request");
        return;
    }

    var el = document.getElementById("chart_div");
    /* because we use Date in the JSON, we need to evaluate, not just parse */
    var avail_height = $(el).parent().parent().innerHeight() -
                       $("#chart_drag_handle").outerHeight() - 50;
    var data = null; // used later on to hold the data to display
    if (extra.chart_type == 'xy') {
        /* XY flot chart */
        data = response.table_data;
        $(el).height(avail_height);
        $(el).width($(el).width() - 10);
        $.plot($(el), data, {
            series: {
                lines: {
                    show: true,
                    lineWidth: 1
                },
                shadowSize: 0
            }
        });
    }
    else {
        var table_data = null;
        eval("table_data = " + response.table_data);
        data = new google.visualization.DataTable(table_data, 0.6);
        var barChart = new google.visualization.BarChart(el);
        var min_height = 30+20*table_data.rows.length;
        barChart.draw(data, {
            width: $(el).width() - 10,
            height: min_height > avail_height ? min_height : avail_height,
            title: null,
            isStacked: true,
            //legend: 'none',
            chartArea: {
                right: 10,
                top: 20
            },
            vAxis: {
                title: null //'server'
            },
            hAxis: {
                title: (extra.per_second ? 'packets/s' : 'packets')
                //logScale: true
            }
        });
    }
};

DailyData.prototype.chartParamsToGetParams = function(){
    if (this.isPageReady()) {
        var data_type_id = document.getElementById("data_type_select").value;
        var url = "data_type=" + data_type_id;
        /* see which servers are active */
        var servers = "";
        $('#server_list').find(':input[id|="server"]:checked').each(function(){
            servers += this.id + ",";
        });
        url += "&servers=" + servers;
        /* start date */
        var start_date = $('#start_date_input').val();
        var end_date = $('#end_date_input').val();
        url += "&start_date="+start_date;
        url += "&end_date="+end_date;
        return url;
    } else
        return null;
};

DailyData.prototype.handleQueryError = function(jqXHR, textStatus, errorThrown){
    this.deactivateBusyIndicator();
    this.showErrorMessageInChart("Error occured when fetching data: " + errorThrown);
};

DailyData.prototype.activateBusyIndicator = function(){
    if (this._busyTimer != null)
        return;

    var el = document.getElementById("chart_div");
    $(el).html("Please wait...");
    var busy_mess_counter = 0;
    var outer_this = this;
    var show_mess = function() {
        var text = "";
        if (busy_mess_counter == 0)
            text = " it takes longer than expected...";
        else if (busy_mess_counter == 1)
            text = " it should be any second now...";
        else if (busy_mess_counter == 2)
            text = " sorry, please be patient... :(";
        $(el).append(text);
        busy_mess_counter++;
        clearTimeout(outer_this._busyTimer);
        outer_this._busyTimer = setTimeout(show_mess, 2000*busy_mess_counter);
    };
    this._busyTimer = setTimeout(show_mess, 2000);
};

DailyData.prototype.deactivateBusyIndicator = function(){
    if (this._busyTimer != null) {
        clearTimeout(this._busyTimer);
        this._busyTimer = null;
    }
    $("#chart_div").html("");
};

DailyData.prototype.isPageReady = function() {
    return DscUI.prototype.isPageReady.call(this) && this.dateReady;
};