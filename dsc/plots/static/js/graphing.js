"use strict";
/*jsl:option explicit*/

/* use the DscUI object resides here */

Graphing.prototype = new DscUI();
Graphing.constructor = Graphing;

function Graphing() {
    DscUI.call(this);
    this.barChart = null;
    this.chart = null;
    this.trackingTimeout = 10; // seconds between updates when tracking is active
    this.csvDataUrl = "get_minute_data_csv";
    this.visualizationDataUrl = "minute_data";
    this.defaultDataType = "server_traffic";
    this._lastChartWidth = null;
    this._lastChartHeight = null;
    this._lastDataChangeTime = null;
    this._lastDataType = null;
    this._lastZoom = null;
    // arrays of data being displayed
    this.clearDataArrays();
    this.lastXAxisRange = [];
    this.xAxisChangedTimeout = null;
    this.doingOfflineZoom = false;
    this.needStoreState = false;
}

Graphing.prototype.init = function(element) {
    this.topElement = element;
    /* create the page layout */
    $(this.topElement).append("<div id='layout-top'>DSCng</div>");
    $(this.topElement).append("<div id='layout-left'></div>");
    $(this.topElement).append("<div id='layout-main'>" +
            "<div id='layout-main-top'></div>" +
            "<div id='layout-main-center'></div>" +
            "<div id='layout-main-bottom'></div>" +
            "</div>");
    $(this.topElement).append("<div id='layout-right'></div>");
    $(this.topElement).append("<div id='layout-bottom'>Footer</div>");
    this.outerLayout = $(this.topElement).layout({
        center__paneSelector: "#layout-main",
        west__paneSelector: "#layout-left",
        east__paneSelector: "#layout-right",
        north__paneSelector: "#layout-top",
        south__paneSelector: "#layout-bottom",
        west__size: 120,
        east__size: 200,
        spacing_open: 2, // ALL panes
        spacing_closed: 8, // ALL panes
        north__spacing_open: 0,
        south__spacing_open: 0,
        north__maxSize: 100,
        south__maxSize: 100,
        initClosed: false,
        north__initHidden: true,
        south__initHidden: true,
        east__initClosed: true,
        applyDefaultStyles: true,
        center__onresize: this.adjustMinuteGraphSize.bind(this),
        slidable: false
    });
    this.innerLayout = $("#layout-main").layout({
        center__paneSelector: "#layout-main-center",
        north__paneSelector: "#layout-main-top",
        south__paneSelector: "#layout-main-bottom",
        north__resizable: false,
        spacing_open: 2, // ALL panes
        spacing_closed: 8, // ALL panes
        initClosed: false,
        applyDefaultStyles: true,
        south__minSize: 120,
        south__size: 240,
        south__initClosed: true,
        center__onresize: this.adjustMinuteGraphSize.bind(this),
        slidable: false
    });
    /* fill in the elements that we need later */
    /* list of server names */
    $("#layout-left").append(
            "<div id='server_list_parent'>" +
            "<div class='ui-widget-header'>Servers</div>" +
            "<div id='server_list'></div>" +
            "</div>");
    /* list of available data types */
    $("#layout-main-top").append("<div id='data_type_list'>" +
            "<div id='data_type_div'" +
            "class='ui-widget-header'>Data types</div>" +
            "<span id='data_type_select_div'></span>" +
            "<span id='busy_indicator'></span>" +
            "<span id='range_select_div' class='right'>" +
            "<input type='radio' name='range_radio' value='last_day' " +
            "id='radio_last_day'/><label for='radio_last_day'>Last day</label>" +
            "<input type='radio' name='range_radio' value='last_week' " +
            "id='radio_last_week' checked='checked'/>" +
            "<label for='radio_last_week'>Last week</label>" +
            "<input type='radio' name='range_radio' value='last_month' " +
            "id='radio_last_month'/><label for='radio_last_month'>Last month</label>" +
            "<input type='radio' name='range_radio' value='all' id='radio_all'/>" +
            "<label for='radio_all'>All</label>" +
            "</span>" +
            "<span id='tracking_switch' class='right'>" +
            "<input type='checkbox' name='track_updates' " +
            "id='track_updates'/><label for='track_updates'>Track updates</label>" +
            "</span>" +
            "</div>");
    $("#range_select_div").buttonset().css({"font-size": "8pt"});
    $(":radio[name='range_radio']").change(this.changeRange.bind(this));
    /* the chart itself */
    $("#layout-main-center").
        append($("<div id='chart_parent'>" +
                 "<div id='chart_drag_handle' class='right'>" +
                 "<span id='chart_menu'></span></div>" +
                 "<div id='chart_div'></div>" +
                 "<div id='chart_additional'></div>" +
                 "</div>").css({float: "left"})).
        append($("<div id='label_div'></div>").
                 css({float: 'right', width: '200px', "min-width": "200px",
                     overflow: 'auto'}));
    /* buttons for reload and CSV download */
    $('#chart_menu').append($("<span>Reload</span>").button()
            .click(function () {
                this.reloadCurrentView();
            }.bind(this))
            .css({"font-size": "8pt"}))
        .append($("<span>CSV</span>")
            .button()
            .click(this.downloadChartAsCSV.bind(this))
            .css({"font-size": "8pt"}));
    $("#chart_additional").append("<div id='series_div'></div>")
        .append("<input type='checkbox' id='split_by_server' disabled='disabled' " +
            "title='Allows splitting single series by server. Is active only" +
            " when there is only one series visible' />" +
            "<label for='split_by_server'>Split by server</label>")
        .append("<span class='right'><select id='count_scale_select'>" +
            "<option value='pps' selected='selected'>Packets / s</option>" +
            "<option value='ppm'>Packets / min</option>" +
            "<option value='rel_sum1'>Relative: sum=1</option>" +
            "<option value='rel_max1'>Relative: max=1</option>" +
            "</select></span>").
    append("<div id='chart_settings'></div>");
    $("#series_div").css({"border-bottom":"solid 1px #bbb"});
    $("#chart_settings").append("<input type='checkbox'" +
            "name='show_error_bands' id='show_error_bands' checked='checked' />" +
            "<label for='show_error_bands'>Error bands</label>").
    append("<input type='checkbox' name='stack_data' id='stack_data' />" +
            "<label for='stack_data'>Stacked</label>").
    append("<input type='checkbox' name='highlight_series'" +
            "id='highlight_series' />" +
            "<label for='highlight_series'>Highlight closest</label>");
    $("#split_by_server").bind('change', this.splitByServerToggled.bind(this));
    $("#count_scale_select").bind('change', this.countScaleChanged.bind(this));
    $("#show_error_bands").bind('change', this.showErrorBandsToggled.bind(this));
    $("#stack_data").bind('change', this.stackDataChanged.bind(this));
    $("#highlight_series").bind('change', this.highlightSeriesChanged.bind(this));
    /* a widget for details about a concrete time */
    $("#layout-right").append(
            "<div id='time_details_parent'>" +
            "<div id='time_details_drag_handle'" +
            "class='ui-widget-header'>Details</div>" +
            "<div id='time_details'></div>" +
            "</div>");
    /* a widget for bar chart */
    $("#layout-main-bottom").append(
            "<div id='bar_chart_parent'>" +
            "<div id='bar_chart_drag_handle'" +
            "class='ui-widget-header'>Bar chart</div>" +
            "<div id='bar_chart'></div>" +
            "<div id='bar_chart-info'></div>" +
            "</div>");
    /* call some async requests for data */
    /* fetch available data types */
    jQuery.getJSON(this.getDataSourceUrl("data_types"),
                    this.dataTypesLoadCallback.bind(this));
    /* fetch servers */
    jQuery.getJSON(this.getDataSourceUrl("servers"),
                    this.serversLoadCallback.bind(this));
    /* the bar chart for details display */
    this.barChart = new google.visualization.BarChart(
                            document.getElementById('bar_chart'));
    /* start periodic ticker */
    setInterval(this.periodicTasks.bind(this), this.trackingTimeout * 1000);
};

Graphing.prototype.initializeGraph = function(){
    /* this method checks if all necessary data is loaded
     * - the visualization API, the servers and the data types
     * and then loads the initial data
     */
    this.updateDetails('last_week');
};

Graphing.prototype.computeIdealGraphWidth = function(){
    return $("#layout-main-center").innerWidth() - 40 //
            -$("#label_div").outerWidth(true);
};

Graphing.prototype.computeIdealGraphHeight = function(){
    return $("#layout-main-center").innerHeight() -30 - //
           $("#chart_drag_handle").outerHeight(true) -  //
           $("#chart_additional").outerHeight(true);
};

Graphing.prototype.adjustMinuteGraphSize = function(){
    var gr_width = this.computeIdealGraphWidth();
    var gr_height = this.computeIdealGraphHeight();
    if (this.chart != null && (this._lastChartWidth != gr_width
        || this._lastChartHeight != gr_height)) {
        this._lastChartWidth = gr_width;
        this._lastChartHeight = gr_height;
        //console.log('resizing', gr_width, gr_height);
        /* the resize itself */
        this.chart.resize(gr_width, gr_height);
        /* resize the parent as well to make the buttons etc. move as well */
        $("#chart_parent").width(gr_width);
    }
};

Graphing.prototype.createMinuteGraph = function(data, labels){
    /* this size is just an approximation because once the
     * additional section gets filled, it would not match
     * anymore.
     * This is dealt with later on by adjustMinuteGraphSize
     */
    var gr_width = this.computeIdealGraphWidth();
    var gr_height = this.computeIdealGraphHeight();
    this.chart = new Dygraph(document.getElementById('chart_div'), data, {
        rollPeriod: 1,
        stepPlot: false,
        width: gr_width,
        height: gr_height,
        labelsSeparateLines: true,
        showRoller: true,
        drawCallback: this.drawCallbackUpdate.bind(this),
        zoomCallback: this.changeZoom.bind(this),
        clickCallback: this.showTimeDetails.bind(this),
        connectSeparatedPoints: true,
        stackedGraph: false,
        errorBars: true,
        customBars: true,
        maxNumberWidth: 8,
        digitsAfterDecimal: 1,
        highlightSeriesBackgroundAlpha: 0.7,
        labelsDiv: document.getElementById("label_div"),
        //labelsDivWidth: 200,
        legend: "always",
        panEdgeFraction: 0.00001,
        labels: labels,
        dateWindow: this.getDetailDateWindow()
    });
    /* resize the parent as well to make the buttons etc. move as well */
    $("#chart_parent").width(gr_width);
    /* replace the default unzoom function */
    Dygraph.prototype.doUnzoom_ = this.previousZoom.bind(this);
};

Graphing.prototype.deactivateBusyIndicator = function(){
    // deactivate busy indicator
    $('#busy_indicator').empty();
};

Graphing.prototype.activateBusyIndicator = function(){
    // show the busy indicator
    $('#busy_indicator').html(
            "<img src='"+this.TOP_URL+"static/img/rotator.gif'/>");
};

Graphing.prototype.updateSeriesList = function (labels) {
        this.dataSeries = [];
        var additional = $('#series_div');
        /* empty and repopulate additional */
        additional.empty();
        var num_checked = 0;
        for (var i = 1; i < labels.length; i++) {
            var checkbox_id = i-1;
            var should_check = this.chart.visibility()[i - 1];
            /* here we create the checkbox and set the series visibility */
            var new_inp = $('<input type=checkbox id="' + checkbox_id +
                    '"/>').prop("checked", should_check);
            if (should_check) num_checked++;
            new_inp.appendTo(additional);
            var to_call = function(obj, el) {
                            return function() {obj.toggleSeriesVisibility(el);};
                            }(this, new_inp);
            new_inp.click(to_call);
            var label = $('<span class="series">'+labels[i]+'</span>').
                        css({cursor: 'pointer'});
            to_call = function(obj, el) {
                            return function() {obj.selectOnlyThisSeries(el);};
                        }(this, new_inp);
            label.click(to_call);
            additional.append(label);
            this.dataSeries.push(labels[i]);
        }
        if (num_checked != 1)
            $("#split_by_server").prop('disabled', true);
        else
            $("#split_by_server").prop('disabled', false);
};

/*
 * takes data and performs an update and redisplay of the chart data
 */
Graphing.prototype.updateData = function(data, opts){
    opts.file = data;
    opts.fillGraph = false; /* must be here, it is incompatible with errorBars */
    this.chart.setSelection(false, null); /* we need to do this in order to
                            ensure proper redwawing of highlighted series */
    this.chart.updateOptions(opts);
    /* readjust the view using current settings */
    this.showErrorBandsToggled();
    this.stackDataChanged();
};

Graphing.prototype.toggleSeriesVisibility = function(el){
    var id = parseInt(el[0].id, 10);
    var checked = el[0].checked;
    this.chart.setVisibility(id, checked);
    /* get all checked */
    if ($("#series_div").find(":checked").length == 1) {
        /* there is only a single selected series
         * we can activate splitting by server
         */
        $("#split_by_server").prop("disabled", false);
    } else {
        $("#split_by_server").prop("disabled", true).prop('checked', false);
    }
};

Graphing.prototype.selectOnlyThisSeries = function(el){
    /*
     * a label of one series was clicked - the user wants just this series
     * to be displayed. However, if it is already the only one displayed,
     * then we should select them all again.
     */
    var sel_id = parseInt(el[0].id, 10);
    var chart = this.chart;
    // what is the situation?
    var selected = [];
    var series_div = $("#series_div");
    series_div.find(":checkbox").each(function(){
        if ($(this).prop('checked')) {
            selected.push(this.id)
        }});
    if (selected.length == 1 && selected[0] == sel_id) {
        // only the clicked series is active - we should show all
        series_div.find(":checkbox").each(
            function(){
                $(this).prop('checked', true);
                chart.setVisibility(parseInt(this.id, 10), true);
                });
        /* get all checked */
        if (series_div.find(":checked").length == 1) {
            /* there is only a single selected series
             * we can activate splitting by server
             */
            $("#split_by_server").prop("disabled", false);
        } else {
            $("#split_by_server").prop("disabled", true).prop('checked', false);
        }
    } else {
        chart.setVisibility(sel_id, true);
        series_div.find(":checkbox").each(
            function(){
                if (this.id != sel_id) {
                    $(this).prop('checked', false);
                    chart.setVisibility(parseInt(this.id, 10), false);
                } else {
                    $(this).prop('checked', true);
                }
            });
        if ($("#split_by_server").prop("checked"))
            this.updateDetails();
        else
            $("#split_by_server").prop("disabled", false);
    }
};

Graphing.prototype.chartParamsToGetParams = function(range){
    if (this.isPageReady()) {
        /* data type and value subset */
        var data_type_id = document.getElementById("data_type_select").value;
        var url = "data_type=" + data_type_id;
        var data_type = this.dataTypes[data_type_id];
        if (data_type.dim_count == 2) {
            /* we need also the value of selected subset */
            var subset = document.getElementById("data_type_subset_select").value;
            url += "&subset=" + subset;
        }
        /* split by server */
        if ($("#split_by_server").prop('checked')) {
            var fix_value = $("#series_div").find(":checked").attr('id');
            url += "&fix_value=" + this.dataSeries[fix_value];
        }
        /* date range - either relative or absolute */
        if (this.chart != null) {
            if (range != null)
                url += '&rel='+range;
            else {
                var x1 = this.chart.xAxisRange()[0];
                var x2 = this.chart.xAxisRange()[1];
                var y1 = this.chart.yAxisRange()[0];
                var y2 = this.chart.yAxisRange()[1];
                url += '&x1=' + x1 + '&x2=' + x2 + '&y1=' + y1 + '&y2=' + y2;
            }
        }
        else {
            if (range != null)
                url += '&rel='+range;
            else
                url += '&rel=last_week';
        }
        /* active servers */
        var servers = this.getActiveServers();
        url += "&servers=" + servers;
        /* count scaling */
        var count_scale = document.getElementById("count_scale_select").value;
        url += "&count_scale=" + count_scale;
        /* it is all done */
        return url;
    } else
        return null;
};

Graphing.prototype.getActiveServers = function() {
    // returns comma-separated list of active servers
    var servers = "";
    $('#server_list').find(':input[id|="server"]:checked').each(function(){
        servers += this.id + ",";
    });

    return servers;
};

Graphing.prototype.handleQueryError = function(jqXHR, textStatus, errorThrown){
    this.deactivateBusyIndicator();
    if (this.chart != null) {
        this.chart.destroy();
        this.chart = null;
    }
    $('#series_div').empty();
    this.showErrorMessageInChart("Error occured when fetching data: "+
                                errorThrown);
};

Graphing.prototype.changeRange = function(event){
    this.resetHistory();
    $('#tracking_switch').show();
    this.updateDetailData(event.target.value);
};

Graphing.prototype.disableTracking = function () {
    $(":radio[name='range_radio']:checked").prop('checked', false).button('refresh');
    $('#tracking_switch').hide();
};

Graphing.prototype.stackDataChanged = function(event){
    if (this.chart != null) {
        var show = $("#stack_data").prop('checked');
        this.chart.updateOptions({stackedGraph: show,
                                  fillGraph: show,
                                  errorBars: false});
        if (!show)
            this.showErrorBandsToggled();
    }
};

Graphing.prototype.highlightSeriesChanged = function(event){
    if (this.chart != null) {
        var show = $("#highlight_series").prop('checked');
        if (show) {
            this.chart.updateOptions(
                    {highlightSeriesOpts: {
                        strokeWidth: 2,
                        strokeBorderWidth: 0,
                        highlightCircleSize: 5
                    }
              });
            $("#label_div").addClass("highlighting");
        } else {
            this.chart.updateOptions({highlightSeriesOpts: null});
            $("#label_div").removeClass("highlighting");
        }
    }
};

Graphing.prototype.previousZoomOffline = function(){
    if (this.dataHistory.length < 2)
        return;

    this.doingOfflineZoom = true;
    var record = this.dataHistory.pop();
    if (record != null) {
        record = this.dataHistory.pop();
        if (record != null) {
            this.dataHistory.push(record);
            this.updateData(record.data, {
                                dateWindow: [record.x1, record.x2],
                                valueRange: [record.y1, record.y2]
            });
            this._lastDataType = record.data_type;
            this._lastZoom = record.zoom;
            this.afterDisplayedDataChange();
        }
    }
    this.doingOfflineZoom = false;
};

Graphing.prototype.previousZoom = function(){
    this.previousZoomOffline();
};

Graphing.prototype.countScaleChanged = function(){
    this.resetHistory(false);
    this.updateDetails();
};

Graphing.prototype.changeDataType = function(){
    var data_type_id= document.getElementById("data_type_select").value;
    var data_type = this.dataTypes[data_type_id];
    $("#data_type_subset_select").remove();
    /* we are changing data types, we do not want to split by server anymore */
    $("#split_by_server").prop("checked", false);
    if (data_type.dim_count == 2) {
        /* we need to select a subset of the data first */
        var subset_select = '<select id="data_type_subset_select">';
        for (var i = 0; i<data_type.subsets.length; i++) {
            subset_select += '<option value="'+ data_type.subsets[i][0] +
            '">' + data_type.subsets[i][1] + '</option>';
            }
        subset_select += "</select>";
        $("#data_type_select_div").append(subset_select);
        $("#data_type_subset_select").change(this.changeSubset.bind(this));
    }
    this.resetHistory();
    this.clearDataArrays();
    DscUI.prototype.changeDataType.call(this);
};

Graphing.prototype.changeServers = function() {
    this.resetHistory();
    DscUI.prototype.changeServers.call(this);
};

Graphing.prototype.showTimeDetails = function(event, time, points){
    if (!this.outerLayout.state.east.isClosed) {
        /* the pane for this data is open - we show the data */
        jQuery.getJSON(this.getDataSourceUrl("time_details"),
                {time: time, zoom: this._lastZoom,
                    servers: this.getActiveServers()},
                this.timeDetailsLoadedCallback.bind(this));
    }
    if (!this.innerLayout.state.south.isClosed) {
        /* the pane for this data is open - we show the data */
        jQuery.getJSON(this.getDataSourceUrl("time_details_datatable"),
                {time: time, zoom: this._lastZoom,
                    servers: this.getActiveServers()},
                this.timeDetailsDataTableLoadedCallback.bind(this));
    }
};

Graphing.prototype.timeDetailsLoadedCallback = function(data){
    if (this.checkForRedirectRequest(data)) return;
    $("#time_details").empty().append("<table id='detail_table'></table>");
    var last_first_value = null;
    var color = -1;
    var detail_table = $("#detail_table");
    for (var i=0; i < data.length; i++) {
        if (typeof data[i][1] == 'string' && data[i][1].indexOf("Date(") != -1) {
            data[i][1] = new Date(parseInt(data[i][1].substr(5, data[i][1].length - 6)));
        }

        var row = "<tr>";
        for (var j=0; j < data[i].length; j++)
            row += "<td>" + data[i][j] + "</td>";
        row += "</tr>";
        if (last_first_value != data[i][0]) {
            last_first_value = data[i][0];
            color += 1;
            if (color >= this._colorPalette.length)
                color = 0;
        }
        row = $(row).css({'background-color': this._colorPalette[color]});
        detail_table.append(row);
    }
    detail_table.css({"border-collapse": 'collapse'});
};

Graphing.prototype.timeDetailsDataTableLoadedCallback = function(response){
    if (this.checkForRedirectRequest(response)) return;
    var data = null;
    eval('data ='+response.data);
    var datatable = new google.visualization.DataTable(data, 0.6);
    this.barChart.draw(datatable, {height: 400, title: null,
        isStacked: true, legend: 'none',
        chartArea: {left: '25%', top: 10},
        vAxis: {title: 'data type'},
        hAxis: {title: 'packets/s'}
    });
    var chartInfoDl = $("#bar_chart-info").empty().append("<dl></dl>");
    chartInfoDl.append("<dt>Time</dt>");
    chartInfoDl.append("<dd>" + new Date(parseInt(response.time.substr(5, response.time.length - 6))) + "</dd>");
    chartInfoDl.append("<dt>Servers</dt>");
    chartInfoDl.append("<dd>" + response.servers + "</dd>");
};

Graphing.prototype.changeSubset = function(){
    this.resetHistory();
    this.updateDetails();
};

Graphing.prototype.splitByServerToggled = function(){
    this.resetHistory();
    if ($("#split_by_server").prop('checked')) {
        $('#series_div').find('*[type="checkbox"]').prop('disabled', true);
    } else {
        $('#series_div').find('*[type="checkbox"]').prop('disabled', false);
    }
    this.updateDetails();
};

Graphing.prototype.showErrorBandsToggled = function(){
    if (this.chart != null) {
        var show = $("#show_error_bands").prop('checked');
        /* do not show error bands if we are in the stacked mode - it would not
           work */
        show = (show &&  !$("#stack_data").prop('checked'));
        this.chart.updateOptions({errorBars: show});
    }
};

Graphing.prototype.changeZoom = function(minDate, maxDate, yRange){
    /* check in which direction we are zooming */
    var hist = this.dataHistory[this.dataHistory.length-1];
    if (hist && hist.x1 == minDate && hist.x2 == maxDate) {
        /* this is Y zoom - we do not do anything */
            this.storeCurrentState(hist.data);
    } else {
        /* at first deselect the current zoom in the upper menu and hide tracking
         * switch */
        this.disableTracking();
        /* then check if it is needed and potentially update the details */
        /* we are at zoom 1 - we will not get more detailed data
         * from the server */
        this.needStoreState = true;
        //this.storeCurrentState(hist.data);
    }
    this.afterDisplayedDataChange();
};

Graphing.prototype.storeCurrentState = function(data){
    /* makes a dataHistory record that allows going back in displayed data
     * later on */
    var xrange = this.chart.xAxisRange();
    var x1 = xrange[0];
    var x2 = xrange[1];
    var yrange = this.chart.yAxisRange();
    var y1 = yrange[0];
    var y2 = yrange[1];
    this.dataHistory.push({'data': data, 'zoom': this._lastZoom,
        'data_type': this._lastDataType, x1: x1, x2: x2, y1: y1, y2: y2});
};

Graphing.prototype.reloadCurrentView = function(){
    /* refetch the data for current settings */
    /* if relative timespan is used, we want to keep it so that new data is
     * shown on reload of 'last_day' for example
     */
    var rel_range = null;
    $(":radio[name='range_radio']:checked").
                each(function() {rel_range = this.value});
    this.updateDetails(rel_range);
};

Graphing.prototype.periodicTasks = function(){
    /* this method is run periodically - all stuff that needs it resides here */
    var rel_range = null;
    $(":radio[name='range_radio']:checked").
                each(function() {rel_range = this.value});
    /* if the range is relative, we update the data periodically to track
     * data updates on the server
     */
    if (rel_range != null && $('#track_updates').prop('checked')
        && (this._lastDataChangeTime != null)) {
        var curr_time = Math.round(new Date().getTime()/1000.0);
        if (curr_time - this._lastDataChangeTime > this.trackingTimeout)
            this.reloadCurrentView();
    }
};

Graphing.prototype.afterDisplayedDataChange = function(){
    /* this method is run always after new data arrives to be displayed
     * and the view is updated */

    this.deactivateBusyIndicator();
    this._lastDataChangeTime = Math.round(new Date().getTime()/1000.0);
};

Graphing.prototype.checkForResponseError = function(response) {
    // returns true if something went wrong, false otherwise
    if (response.error != null) {
        if (this.checkForRedirectRequest(response))
            return true;
        /* an error occured at the server */
        if (this.chart != null) {
            this.chart.destroy();
            this.chart = null;
        }
        $('#series_div').empty();
        this.deactivateBusyIndicator();
        this.showErrorMessageInChart(response.error);
        return true;
    }

    /* check if the data type we received matches the currently chosen
     * otherwise we have probably received data that were requested
     * earlier and are no longer needed - we log it and leave
     */
    if (response.extra.data_type != $("#data_type_select").val()) {
        console.log("wrong data type in response - maybe stale request");
        return true;
    }

    return false;
};

Graphing.prototype.clearDataArrays = function() {
    // this function should be called every time the datatype or server changed
    this.coarseData = [];
    this.detailData = [];
    this.combinedData = [];
    this._isCoarseDataFetched = false;
    this._isDetailDataFetched = false;
};

Graphing.prototype.resetHistory = function (needStoreState) {
    this.dataHistory = [];
    if (needStoreState)
        this.needStoreState = true;
};

Graphing.prototype.createCombinedData = function() {
    // helper for finding the boundaries in the coarse data array
    var findBoundingIndexForDate = function(array, date, fromIndex) {
        fromIndex = fromIndex || 0;

        for (var i = fromIndex; i < array.length; i++) {
            if (array[i][0] > date)
                return i;
        }

        return i;
    };

    if (this.coarseData.length < 1 || this.detailData.length < 1) {
        // TODO: something went wront OR we don't have data for this time/zoom
        console.log("no coarse or detail data to combine");
        this.combinedData = this.coarseData;
        return;
    }

    var lowerVisibleDate = this.detailData[0][0];
    var upperVisibleDate = this.detailData[this.detailData.length - 1][0];

    var lowerBound = findBoundingIndexForDate(this.coarseData, lowerVisibleDate);
    var upperBound = findBoundingIndexForDate(this.coarseData, upperVisibleDate,
        lowerBound);

    // here we create the combined array itself
    this.combinedData = [];
    this.combinedData.push.apply(this.combinedData, this.coarseData.slice(0, lowerBound));
    // add detail data to boundaries of visible portion
    this.combinedData.push.apply(this.combinedData, this.detailData.slice(0));
    // fill the remaining part with coarse data
    this.combinedData.push.apply(this.combinedData, this.coarseData.slice(upperBound, this.coarseData.length));
};

Graphing.prototype.afterDataFetched = function(response) {
    if (!this._isCoarseDataFetched || !this._isDetailDataFetched) {
        // we do not have all the data ready, wait for the second part
        return;
    }

    var i = 0; // this is used later on in for cycles

    this.createCombinedData();

    var extra = response.extra;

    /* either create the graph with the data that arrived, or update it */
    if (this.chart == null) {
        this.createMinuteGraph(this.combinedData, extra.labels);
        this.needStoreState = true;
    }
    else {
        /* if we update the data, we should also do something with the series
         * visibility - when changing data type, all should be reselected,
         * otherwise, those series that were unselected should remain so
         */
        var visibility = [];
        if ($("#split_by_server").prop('checked')) {
            /* if split by server is activated, we are in a strange mode of
             * operation because the display of series labels below the chart
             * is not in sync with the series displayed.
             * Therefore we need to set visibility of all series to true,
             * to show all the servers
             */
            for (i = 1; i < extra.labels.length; i++) {
                visibility.push(true);
            }
        } else {
            /* here we deal with the situation when split_by_server is not
             * selected, so we must apply normal rules to what should be
             * preserved as visible and what not.
             */
            /* at first remember what is currently selected */
            var unselected = [];
            $('#series_div').find(':input').not(':checked').each(function(){
                var label = $(this).next("span.series").text();
                unselected.push(label);
            });
            for (i = 1; i < extra.labels.length; i++) {
                var visible = true; /* default is visible */
                if (extra.data_type == this._lastDataType) {
                    /* the data type did not change - unselect what was already
                     * unselected */
                    var label = extra.labels[i];
                    if ($.inArray(label, unselected) != -1)
                        visible = false; /* it was not selected before - unselect */
                }
                visibility.push(visible);
            }
        }
        /* options to use for update
         * update saved X axis range first to prevent draw callbackack
         * being executed */
        this.lastXAxisRange = this.getDetailDateWindow();
        var opts = {
                    dateWindow: this.lastXAxisRange,
                    valueRange: null,
                    visibility: visibility,
                    labels: extra.labels
        };
        /* should we adjust y zoom? Only if data type changed /
        if (extra.data_type != this._lastDataType)
            opts.valueRange = null; */
        /* perform the udpate */
        this.updateData(this.combinedData, opts);
    }
    /*
     * Here we put a list of series under the chart itself.
     * we only do so in case split_by_server is not active
     * otherwise the previous list of series stays and is just made inactive
     */
    if (!($("#split_by_server").prop('checked'))) {
        this.updateSeriesList(extra.labels);
    }
    this.adjustMinuteGraphSize();
    this._lastZoom = extra.zoom;
    this._lastDataType = extra.data_type;
    if (this.needStoreState) {
        this.storeCurrentState(this.combinedData);
        this.needStoreState = false;
    }
    /* run a function dedicated for after-data-loading stuff */
    this.afterDisplayedDataChange();
};

Graphing.prototype.displayWholeCoarseRange = function() {
    this.combinedData = this.coarseData;
    this.lastXAxisRange = [this.coarseData[0][0].getTime(), this.coarseData[this.coarseData.length - 1][0].getTime()];

    var opts = {
                dateWindow: this.lastXAxisRange
    };
    this.updateData(this.combinedData, opts);
    this.storeCurrentState(this.combinedData);

    this.adjustMinuteGraphSize();
    this.afterDisplayedDataChange();
};

Graphing.prototype.updateDetailData = function(range) {
    /* hard-code behaviour of range 'all' not to fetch data and use only coarse
     *  range instead */
    if (range == 'all') {
        this.displayWholeCoarseRange();
        return;
    }
    this.activateBusyIndicator();
    var url = this.chartParamsToGetParams(range);

    jQuery.ajax({
        url: this.getDataSourceUrl(this.visualizationDataUrl) + "?" + url,
        success: this.detailDataFetched.bind(this),
        error: this.handleQueryError.bind(this),
        dataType: "json"
    });
};

Graphing.prototype.detailDataFetched = function (response) {
    if (this.checkForResponseError(response))
        return;

    this.detailData = eval(response.data);

    if (this.detailData.length == 0) {
        console.log("no detail data for this view");
    }
    this._isDetailDataFetched = true;

    this.afterDataFetched(response);
};

Graphing.prototype.updateCoarseData = function() {
    jQuery.ajax({
        url: this.getDataSourceUrl(this.visualizationDataUrl)
            + "?" + this.chartParamsToGetParams('all'),
        success: this.coarseDataFetched.bind(this),
        dataType: "json"
    });
};

Graphing.prototype.coarseDataFetched = function(response) {
    if (this.checkForResponseError(response))
        return;

    this.coarseData = eval(response.data);

    if (this.coarseData.length == 0) {
        /* no data present */
        if (this.chart != null) {
            this.chart.destroy();
            this.chart = null;
        }
        $('#series_div').empty();
        this.showErrorMessageInChart(
                "No data present for this data type and time period.");
        return;
    }

    this._isCoarseDataFetched = true;

    this.afterDataFetched(response);
};

Graphing.prototype.fetchData = function (range) {
    this.activateBusyIndicator();

    if (this.coarseData.length < 1) {
        this.updateCoarseData();
    }
    // second part - update detail data
    this.updateDetailData(range);
};

Graphing.prototype.updateDetails = function(range) {
    if (this.isPageReady()) {
        this.clearDataArrays();
        this.fetchData(range);
        /* do the query */
    } else {
        if (this.serverListReady == null) {
            this.showErrorMessageInChart("No servers are available. You need to" +
                " load some data into the system first.");
        } else if (this.dataTypesReady == null) {
            this.showErrorMessageInChart("No data types defined. This is probably" +
                " an incomplete installation of DSCng.");
        }
    }
};

Graphing.prototype.drawCallbackUpdate = function(graph, initial) {
    var currentXAxisRange = graph.xAxisRange();
    if (initial || this.doingOfflineZoom) {
        // only save initial X axis range on first draw
        if (!this.doingOfflineZoom) {
            this.lastXAxisRange = currentXAxisRange;
        }
        return;
    }

    // did some of the graph boundaries changed?
    if (this.lastXAxisRange[0] == currentXAxisRange[0] &&
        this.lastXAxisRange[1] == currentXAxisRange[1])
        return;

    this.activateBusyIndicator();
    this.lastXAxisRange = currentXAxisRange;

    this.disableTracking();

    // refresh data every one second since last movement of x-axis
    window.clearTimeout(this.xAxisChangedTimeout);
    this.xAxisChangedTimeout = window.setTimeout(this.updateDetailData.bind(this), 1000);
};

Graphing.prototype.getDetailDateWindow = function () {
    if (this.detailData.length < 2)
        return this.lastXAxisRange;

    return [this.detailData[0][0].getTime(),
        this.detailData[this.detailData.length - 1][0].getTime()]
};