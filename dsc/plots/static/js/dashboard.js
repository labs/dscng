"use strict";
/*jsl:option explicit*/

function show_current_server_traffic(el){
    jQuery.ajax({
        dataType: "json",
        url: DSC_TOP_URL+"json/server_traffic/",
        success: function(response){
            var data = response.data;
            if (response.ok && data != null) {
                var datatable = new google.visualization.DataTable(data, 0.6);
                var barChart = new google.visualization.BarChart(el);
                barChart.draw(datatable, {
                    width: 400,
                    height: 40 + 15 * response.rows_cnt,
                    title: 'Server traffic: ' + response.time,
                    isStacked: true,
                    legend: 'none',
                    chartArea: {
                        top: 20,
                        left: 80
                    },
                    vAxis: {
                        title: 'server'
                    },
                    hAxis: {
                        title: 'packets/s'
                    }
                });
            } else {
                $(el).html("<b>Server traffic</b>: No data returned from server.")
            }
        },
        error:  function(jqXHR, textStatus, errorThrown) {
            $(el).html("<b>Server traffic</b>: Error occured when fetching data: "+
                    errorThrown);
        }});

}


function show_current_rcode_piechart(el){

    jQuery.ajax({
        dataType: "json",
        url: DSC_TOP_URL+"json/data_type_detail_dt/",
        success: function(response){
            var data = response.data;
            if (response.ok && data != null) {
                var datatable = new google.visualization.DataTable(data, 0.6);
                var barChart = new google.visualization.PieChart(el);
                barChart.draw(datatable, {
                    width: 400,
                    height: 180,
                    title: 'Rcode: ' + response.time,
                    isStacked: true,
                    chartArea: {
                        left: 20,
                        top: 50,
                        width: 380
                    },
                    vAxis: {
                        title: 'server'
                    },
                    hAxis: {
                        title: 'packets/s'
                    }
                });
                //$("#qtype_chart").style({position: "relative", "left": "-100"})
            } else {
                $(el).html("<b>Rcode chart</b>: No data returned from server.");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $(el).html("<b>Rcode chart</b>: Error occured when fetching data: "+
                    errorThrown);
        }});
}

function show_current_overall_traffic_gauge(el){
    jQuery.ajax({
        dataType: "json",
        url: DSC_TOP_URL+"json/overall_traffic/",
        success: function(response){
            if (response.ok) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Label');
                data.addColumn('number', 'Value');
                data.addRows(1);
                data.setValue(0, 0, 'Traffic');
                data.setValue(0, 1, Math.round(100 * response.current / response.mean) / 100);

                var chart = new google.visualization.Gauge(el);

                var options = {
                    width: 160,
                    height: 160,
                    redFrom: 2.5,
                    redTo: 3,
                    yellowFrom: 1.5,
                    yellowTo: 2.5,
                    minorTicks: 5,
                    max: 3,
                    majorTicks: ["0", "0.5", "1.0", "1.5", "2.0", "2.5", "3.0"],
                    greenFrom: 0.5,
                    greenTo: 1.5
                };
                chart.draw(data, options);
                $("#traffic_gauge_chart").append("<div style='text-align:center'>" +
                        "<b>Current</b>: " +
                        Math.round(response.current) +
                        " qps<br/><b>Typical</b>: " +
                        Math.round(response.mean) +
                        " qps</div>");
            } else {
                $(el).html("<b>Overall traffic</b>: No data returned from " +
                        "server.");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $(el).html("<b>Overall traffic</b>: Error occured when fetching " +
                    "data: "+ errorThrown);
        }});

}
/*function pieHover (event, pos, obj) {
    if (obj) {
        $("#text").html(obj.series.percent);
    }
}
*/

/*jQuery.getJSON("/dscng/json/data_type_detail/",
    function (response) {
        var data = response.data;
        $.plot($("#qtype_chart"), data, {
            series: {
                pie: {
                    show: true,
                    radius: 50,
                    label: {show: true, threshold: 0.01, radius: 80, background: {
                opacity: 0.5,
                color: '#000'
            }},

                }
            },
            legend: {
                show: true,
                hoverable: true
            },
            grid: {
                hoverable: true
            }
        });
        $("#qtype_chart").bind("plothover", pieHover);
    });*/