function DscUI() {
    this.TOP_URL = "/dscng/";
    this.topElement = null;
    this.visualizationDataUrl = null;
    this.visualizationReady = false;
    this.dataTypesReady = false;
    this.serverListReady = false;
    this.dataTypes = [];
    this.dataSeries = [];
    this._colorPalette = ['#9af8ff','#9affbf','#efff9a','#ffa69a','#ff9acd',
        '#d49aff','#9a9cff'];
    this.outerLayout = null;
    this.innerLayout = null;
}

DscUI.prototype.serversLoadCallback = function(result){
    var data = [];
    var make_tree = function(row) {
        var new_row = {'data': row.name,
            'attr': {id: row.type=="group" ?
                "grp-"+row.id : "srvr-"+row.id}};
        var children_rows = [];
        if (row.children != null) {
            for (var j = 0; j<row.children.length; j++) {
                children_rows.push(make_tree(row.children[j]));
            }
            new_row['children'] = children_rows;
        }
        return new_row;
    };
    for (var i = 0; i<result.data.length; i++) {
        data.push(make_tree(result.data[i]));
    }

    if (result.data.length == 0) {
        this.serverListReady = null;
    } else {
        /* we have some servers - we can show the tree */
        $("#server_list").jstree({
            "core": {"animation": 0},
            "json_data" : {"data" : data},
            "themes": {"theme": "default", "icons": false},
            "checkbox": {
                real_checkboxes: true,
                real_checkboxes_names: function (n) {
                    return [n[0].id.replace("srvr","server").replace("grp","group"),
                        1];
                }
            },
            "plugins": ["checkbox", "json_data", "ui", "themes"]
        }).
            bind("loaded.jstree",
                function () {
                    $("#server_list").jstree("check_all");
                    this.serverListReady = true;
                    this.outerLayout.sizePane('west',
                        $("#server_list").outerWidth(true)+36);
                    this.initializeGraph();
                }.bind(this)).
            bind('change_state.jstree',
                this.changeServers.bind(this)).
            bind('select_node.jstree',
                this.selectServerCallback.bind(this));
    }
};

DscUI.prototype.setTopUrl = function(url) {
    this.TOP_URL = url;
};

DscUI.prototype.getDataSourceUrl = function(name){
    return this.TOP_URL+"json/"+name+"/";
};

DscUI.prototype.visualizationReadyCallback = function(){
    this.visualizationReady = true;
    this.initializeGraph();
};

DscUI.prototype.dataTypesLoadCallback = function(result){
    if (result.data.length == 0) {
        /* no data types available */
        this.dataTypesReady = null;
    } else {
        var data_type_select = '<select id="data_type_select">';
        for (var i = 0; i<result.data.length; i++) {
            var selected = "";
            if (result.data[i].name == this.defaultDataType)
                selected = " selected='selected'";
            this.dataTypes[result.data[i].id] = result.data[i];
            data_type_select += '<option value="'+result.data[i].id + '"' +
                selected + '>' + result.data[i].local_name + '</option>';
        }
        data_type_select += "</select>";
        $("#data_type_select_div").append(data_type_select);
        $("#data_type_select").change(this.changeDataType.bind(this));
        this.dataTypesReady = true;
        this.initializeGraph();
        this.innerLayout.resizeAll(); /* adjust the size of this pane */
    }
};

DscUI.prototype.changeDataType = function(){
    this.updateDetails();
};

DscUI.prototype.changeServers = function(){
    /* see how many servers are active */
    var server_count = 0;
    var server_list = $('#server_list');
    server_list.find(':input[id|="server"]:checked').
        each(function(){ server_count += 1; });
    if (server_count == 0) {
        /* no servers are active - we activate all of them
         * we use a way that does not fire any callbacks, because if would
         * create 3 server requests instead of one
         */
        server_list.find(':input[id|="server"]').each(function() {
            $(this).attr('checked', true);
        });
        server_list.find('li').removeClass("jstree-unchecked").
            removeClass('jstree-undetermined').addClass("jstree-checked");
    }
    /* now we can move forward */
    this.updateDetails();
};

DscUI.prototype.downloadChartAsCSV = function(){
    /* download what is currently visible in the chart as CSV */
    var url = this.TOP_URL + this.csvDataUrl + "/?";
    url += this.chartParamsToGetParams(null);
    window.open(url, '_blank');
};

DscUI.prototype.selectServerCallback = function(e, data){
    /* when a name of a server is selected, not the checkbox;
     * switch only this node on and uncheck all others;
     * Uses a mixture of low-level aproach not triggering events
     * and high level code to trigger event at the end.
     */
    if (data && data.rslt && data.rslt.obj && data.rslt.obj[0]) {
        var server_list = $('#server_list');
        server_list.find('li').removeClass("jstree-checked").
            removeClass("jstree-undetermined").addClass("jstree-unchecked");
        server_list.find(':input[id|="server"]').each(function() {
            $(this).attr('checked', false);
        });
        server_list.jstree('check_node', data.rslt.obj[0]);
    }

};

DscUI.prototype.checkForRedirectRequest = function(response) {
    if (response.redirect != null) {
        window.location.replace(response.redirect);
        return true;
    }
    return false;
};

DscUI.prototype.isPageReady = function() {
    return this.visualizationReady && this.dataTypesReady &&
        this.serverListReady;
};

DscUI.prototype.updateDetails = function(range){
    if (this.isPageReady()) {
        var url = this.chartParamsToGetParams(range);
        this.activateBusyIndicator();
        /* do the query */
        jQuery.ajax({
            url: this.getDataSourceUrl(this.visualizationDataUrl) + "?" + url,
            success: this.handleQueryResponse.bind(this),
            error: this.handleQueryError.bind(this),
            dataType: "json"
        });
    } else {
        if (this.serverListReady == null) {
            this.showErrorMessageInChart("No servers are available. You need to" +
                " load some data into the system first.");
        } else if (this.dataTypesReady == null) {
            this.showErrorMessageInChart("No data types defined. This is probably" +
                " an incomplete installation of DSCng.");
        }
    }
};

DscUI.prototype.showErrorMessageInChart = function(text) {
    $("#chart_div").html("<h2>"+text+"</h2>");
};