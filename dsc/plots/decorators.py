# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from functools import wraps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseBadRequest
import json


def login_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME,
                   login_url=None):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary. Returns unaltered function if
    authentication is disabled in settings.
    """
    actual_decorator = user_passes_test(
        lambda u: not settings.USE_AUTHENTICATION or u.is_authenticated(),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def json_response(login_required=False):
    """
    Decorator for JSON views. Sends JSON response for views that return dict
    of JSON data or original HTTP response for views returning HttpResponse
    objects. Checks for user authentication if login_required=True.
    """
    def outer_wrap(function):
        def wrapped(request, *args, **kwargs):
            if (login_required and not request.user.is_authenticated() and
                    settings.USE_AUTHENTICATION):
                messages.add_message(request, messages.INFO, 'Your session has expired, please log in again.')
                response = {
                    "error": "User is not authenticated.",
                    "redirect": request.build_absolute_uri(reverse('home'))
                }
            elif not settings.DEBUG and not request.is_ajax():
                return HttpResponseBadRequest()
            else:
                response = function(request, *args, **kwargs)

            if isinstance(response, HttpResponse):
                return response
            elif not isinstance(response, (dict, list)):
                raise ValueError('json_response decorator expected a dict, list'
                                 ' or HttpReponse result.')

            return HttpResponse(json.dumps(response), "application/javascript")
        return wraps(function)(wrapped)
    return outer_wrap