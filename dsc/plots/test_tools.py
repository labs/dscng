# This software is covered by a BSD license:
#
# Copyright (c) 2011-2013, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from django.conf import settings
from django.db import ConnectionHandler
from django.db.backends.postgresql_psycopg2.base import DatabaseWrapper
from django.db.backends.postgresql_psycopg2.creation import DatabaseCreation
from django.test.simple import DjangoTestSuiteRunner, dependency_ordered
from dsc_storage.psql import db_management
from dsc_storage.psql.connection import \
    create_connection_manager_from_settings, get_connection_params_from_settings
from dsc_storage.psql.data_types import DataTypeManager
import os


BASE_PATH = os.path.abspath(os.path.dirname(__file__) + "/../../")


def create_dscng_tables(conn):
    BASE_SQL_FILE = BASE_PATH + "/data/db.sql"
    db_management.import_base_sql_file(conn, BASE_SQL_FILE,
                                       part=db_management.SQL_IMPORT_ALL)
    db_management.create_additional(conn)
    dtm = DataTypeManager(conn)
    dtm.load_data_type_defs_from_json(BASE_PATH + "/data/data_types.json",
                                      BASE_PATH + "/data/dimensions.json")


def create_dscng_test_data():
    # TODO: refactor dat-import module and remove this import hack
    os.sys.path.append(BASE_PATH)
    from import_dsc_dat_files import import_dirs

    conn_params = get_connection_params_from_settings(settings)
    import_dirs(conn_params, [BASE_PATH + "/data/test_input/"], quiet=True)


# following code is mostly taken from the Django source, there's probably
# no elegant way to provide custom SQL types without pre-syncdb signal,
# it could be ditched after patch from ticket #11398 is added to Django

class DscConnectionHandler(ConnectionHandler):
    def __getitem__(self, alias):
        if hasattr(self._connections, alias):
            return getattr(self._connections, alias)

        self.ensure_defaults(alias)
        db = self.databases[alias]
        conn = DscDatabaseWrapper(db, alias)
        setattr(self._connections, alias, conn)
        return conn


class DscDatabaseCreation(DatabaseCreation):
    def create_test_db(self, verbosity=1, autoclobber=False):
        """
        Creates a test database, prompting the user for confirmation if the
        database already exists. Returns the name of the test database created.
        """
        # Don't import django.core.management if it isn't needed.
        from django.core.management import call_command

        test_database_name = self._get_test_db_name()

        if verbosity >= 1:
            test_db_repr = ''
            if verbosity >= 2:
                test_db_repr = " ('%s')" % test_database_name
            print("Creating test database for alias '%s'%s..." % (
                self.connection.alias, test_db_repr))

        self._create_test_db(verbosity, autoclobber)

        self.connection.close()
        self.connection.settings_dict["NAME"] = test_database_name

        # DSCng custom initialization
        settings.DATABASE_NAME = test_database_name
        conn = create_connection_manager_from_settings(
            settings, ignore_version_error=True)
        print "Creating DSCng tables..."
        create_dscng_tables(conn)
        print "Importing DSCng test data..."
        create_dscng_test_data()
        conn.close()
        # end of DSCng custom init

        # Report syncdb messages at one level lower than that requested.
        # This ensures we don't get flooded with messages during testing
        # (unless you really ask to be flooded)
        call_command('syncdb',
            verbosity=max(verbosity - 1, 0),
            interactive=False,
            database=self.connection.alias,
            load_initial_data=False)

        # We need to then do a flush to ensure that any data installed by
        # custom SQL has been removed. The only test data should come from
        # test fixtures, or autogenerated from post_syncdb triggers.
        # This has the side effect of loading initial data (which was
        # intentionally skipped in the syncdb).
        call_command('flush',
            verbosity=max(verbosity - 1, 0),
            interactive=False,
            database=self.connection.alias)

        from django.core.cache import get_cache
        from django.core.cache.backends.db import BaseDatabaseCache
        for cache_alias in settings.CACHES:
            cache = get_cache(cache_alias)
            if isinstance(cache, BaseDatabaseCache):
                call_command('createcachetable', cache._table,
                             database=self.connection.alias)

        # Get a cursor (even though we don't need one yet). This has
        # the side effect of initializing the test database.
        self.connection.cursor()

        return test_database_name

    def destroy_test_db(self, old_database_name, verbosity=1):
        """
        Destroy a test database, prompting the user for confirmation if the
        database already exists.
        """
        # close storage's persistent connection before destroying DB
        import views
        views.storage.conn.close()
        super(DscDatabaseCreation, self).destroy_test_db(old_database_name,
                                                         verbosity)


class DscDatabaseWrapper(DatabaseWrapper):
    def __init__(self, *args, **kwargs):
        super(DscDatabaseWrapper, self).__init__(*args, **kwargs)
        self.creation = DscDatabaseCreation(self)


class DscTestRunner(DjangoTestSuiteRunner):
    def setup_databases(self, **kwargs):
        from django.db import DEFAULT_DB_ALIAS, connections
        connections = DscConnectionHandler(settings.DATABASES)

        # First pass -- work out which databases actually need to be created,
        # and which ones are test mirrors or duplicate entries in DATABASES
        mirrored_aliases = {}
        test_databases = {}
        dependencies = {}
        for alias in connections:
            connection = connections[alias]
            if connection.settings_dict['TEST_MIRROR']:
                # If the database is marked as a test mirror, save
                # the alias.
                mirrored_aliases[alias] = (
                    connection.settings_dict['TEST_MIRROR'])
            else:
                # Store a tuple with DB parameters that uniquely identify it.
                # If we have two aliases with the same values for that tuple,
                # we only need to create the test database once.
                item = test_databases.setdefault(
                    connection.creation.test_db_signature(),
                    (connection.settings_dict['NAME'], set())
                )
                item[1].add(alias)

                if 'TEST_DEPENDENCIES' in connection.settings_dict:
                    dependencies[alias] = (
                        connection.settings_dict['TEST_DEPENDENCIES'])
                else:
                    if alias != DEFAULT_DB_ALIAS:
                        dependencies[alias] = connection.settings_dict.get(
                            'TEST_DEPENDENCIES', [DEFAULT_DB_ALIAS])

        # Second pass -- actually create the databases.
        old_names = []
        mirrors = []

        for signature, (db_name, aliases) in dependency_ordered(
            test_databases.items(), dependencies):
            test_db_name = None
            # Actually create the database for the first connection

            for alias in aliases:
                connection = connections[alias]
                old_names.append((connection, db_name, True))
                if test_db_name is None:
                    test_db_name = connection.creation.create_test_db(
                            self.verbosity, autoclobber=not self.interactive)
                else:
                    connection.settings_dict['NAME'] = test_db_name

        for alias, mirror_alias in mirrored_aliases.items():
            mirrors.append((alias, connections[alias].settings_dict['NAME']))
            connections[alias].settings_dict['NAME'] = (
                connections[mirror_alias].settings_dict['NAME'])

        return old_names, mirrors

# end of Django testrunner hacks
