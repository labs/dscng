# This software is covered by a BSD license:
#
# Copyright (c) 2011-2013, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from datetime import datetime
from django.test import TestCase
from django.test.utils import override_settings
import json
import re
from models import User


@override_settings(DEBUG=False)
class ViewsTests(TestCase):
    VIEW_URLS = (
        "/",
        "/daily_data/", "/timegraph/",
        "/get_daily_data_csv/", "/get_minute_data_csv/?data_type=32"
    )

    def assert_response_ok(self, url):
        response = self.client.get(url)
        self.assertEqual(200, response.status_code,
                         "HTTP 200 expected in %s, got %s"
                         % (url, response.status_code))

    @override_settings(USE_AUTHENTICATION=False)
    def test_views_noauth(self):
        for url in self.VIEW_URLS:
            self.assert_response_ok(url)

    @override_settings(USE_AUTHENTICATION=True)
    def test_views_auth(self):
        User.objects.create_superuser(username="test", email="", password="pass")
        self.client.login(username="test", password="pass")
        for url in self.VIEW_URLS:
            self.assert_response_ok(url)

    @override_settings(USE_AUTHENTICATION=True)
    def test_views_auth_redirect(self):
        pass


@override_settings(DEBUG=False, USE_AUTHENTICATION=False)
class JsonTests(TestCase):
     # JSON views without required GET args
    JSON_URLS = (
        "/json/data_types/", "/json/servers/", "/json/available_date_range/",
        "/json/data_type_detail_dt/", "/json/server_traffic/",
        "/json/overall_traffic/",
    )

    @staticmethod
    def _get_leaves(tree):
        def dive(node, leaves):
            if node.get('children'):
                for child in node.get('children', []):
                    dive(child, leaves)
            else:
                leaves.append(node['id'])
        leaves = []
        for trunk in tree:
            dive(trunk, leaves)
        return leaves

    @classmethod
    @override_settings(USE_AUTHENTICATION=False)
    def setUpClass(cls):
        # minute data types
        client = cls.client_class()
        data = cls._get_json_data(client, "/json/data_types/?daily=false")
        cls.minute_data_types = data['data']
        # accumulated data types
        data = cls._get_json_data(client, "/json/data_types/?daily=true")
        cls.accum_data_types = data['data']
        # servers
        data = cls._get_json_data(client, "/json/servers/")
        cls.servers = data['data']
        cls.server_leaves = cls._get_leaves(cls.servers)
        # last date
        data = cls._get_json_data(client, "/json/available_date_range/")
        cls.max_date = data['max_date']
        cls.min_date = data['min_date']
        cls._initialized = True

    @staticmethod
    def _get_json_data(client, url):
        response = client.get(url, **{'HTTP_X_REQUESTED_WITH':
                                      'XMLHttpRequest'})
        return json.loads(response.content)

    def _validate_minute_json_data(self, data):
        if not "data" in data:
            self.fail("No data in reply")
        else:
            table_data_txt = data['data']
            if not type(table_data_txt) == unicode:
                self.fail("Raw table data is of wrong type {0}".\
                          format(type(table_data_txt)))
            # test date ordering
            last_date = None
            for date_str in re.findall("Date\((\d)\)", table_data_txt):
                date_val = datetime.fromtimestamp(int(date_str)/1000)
                if last_date and last_date > date_val:
                    self.fail("Dates not consecutive in response: %s > %s" % \
                              (last_date, date_val))
                last_date = date_val

    def _validate_daily_json_data(self, data):
        if 'extra' not in data:
            self.fail("No 'extra' metadata in the reply")
        extra = data['extra']
        expected_type = list if extra.get('chart_type') == 'xy' else unicode
        if not "table_data" in data:
            self.fail("No data in reply")
        else:
            table_data_content = data['table_data']
            if not type(table_data_content) == expected_type:
                self.fail("Raw table data is of wrong type {0}".\
                          format(type(table_data_content)))

    def test_json_ok(self):
        for url in self.JSON_URLS:
            response = self.client.get(url, **{'HTTP_X_REQUESTED_WITH':
                                               'XMLHttpRequest'})
            self.assertEqual(200, response.status_code)
            self.assertNotContains(response, "error")

    def test_minute_1d(self):
        for data_type in self.minute_data_types:
            if data_type['dim_count'] == 0:
                rel_url = "/json/minute_data/?data_type={0}".\
                                                format(data_type['id'])
                rel_url += "&rel=last_week"
                data = self._get_json_data(self.client, rel_url)
                self._validate_minute_json_data(data)
                # test server subset
                if self.server_leaves:
                        rel_url += "&servers=server-%d" % self.server_leaves[0]
                        data = self._get_json_data(self.client, rel_url)
                        self._validate_minute_json_data(data)

    def test_minute_2d(self):
        for data_type in self.minute_data_types:
            if data_type['dim_count'] == 1:
                rel_url = "/json/minute_data/?data_type={0}".\
                                                format(data_type['id'])
                rel_url += "&rel=last_week"
                data = self._get_json_data(self.client, rel_url)
                self._validate_minute_json_data(data)
                # test server subset
                if self.server_leaves:
                    rel_url += "&servers=server-%d" % self.server_leaves[0]
                    data = self._get_json_data(self.client, rel_url)
                    self._validate_minute_json_data(data)

    def test_minute_3d(self):
        for data_type in self.minute_data_types:
            if data_type['dim_count'] == 2:
                for i, _subset in enumerate(data_type['subsets'][0]):
                    rel_url = "/json/minute_data/?data_type={0}&subset={1}".\
                                                format(data_type['id'], i)
                    rel_url += "&rel=last_week"
                    data = self._get_json_data(self.client, rel_url)
                    self._validate_minute_json_data(data)
                    # test server subset
                    if self.server_leaves:
                        rel_url += "&servers=server-%d" % self.server_leaves[0]
                        data = self._get_json_data(self.client, rel_url)
                        self._validate_minute_json_data(data)

    def test_daily_2d(self):
        for data_type in self.accum_data_types:
            if True or data_type['dim_count'] == 1:
                rel_url = "/json/daily_data/?data_type={0}&start_date={1}".\
                                            format(data_type['id'], self.max_date)
                data = self._get_json_data(self.client, rel_url)
                self._validate_daily_json_data(data)