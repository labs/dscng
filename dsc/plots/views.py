# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from calendar import timegm
from django.conf import settings
from django.contrib import messages
from django.contrib import auth
import json
from decorators import login_required, json_response

from django.http import HttpResponse, HttpResponseServerError, \
    HttpResponseNotFound, Http404
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.template.loader import render_to_string
import csv
from cStringIO import StringIO
import logging
from dsc import dscng_version
from dsc_storage.psql.query_help import UTC
from models import DailyDataDisplayConfig

logging.basicConfig(level=logging.DEBUG if settings.DEBUG else logging.WARN)

import time
from datetime import timedelta, datetime
from dsc_storage.psql import query_help
import gviz_api

# some helper views
def page_not_found(req):
    return HttpResponseNotFound(render_to_string(
        "plots_base.html",
        {'body': "Page not found 404"},
        context_instance=RequestContext(req)))


def server_error(req):
    return HttpResponseServerError(render_to_string(
        "plots_base.html",
        {'body': "Server Error 500"},
        context_instance=RequestContext(req)))


def logout(request):
    auth.logout(request)
    messages.add_message(request, messages.INFO, 'You were logged out.')
    return redirect('home')


# HTML pages
@login_required
def timegraph(req):
    return render_to_response("graph_page.html",
                              {'active_page': "timegraph"},
                              context_instance=RequestContext(req))


def home(request):
    if not settings.USE_AUTHENTICATION or request.user.is_authenticated():
        return dashboard(request)

    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return redirect(request.POST.get('next', 'home'))
            else:
                messages.add_message(request, messages.ERROR,
                                     'Your user account is disabled.')
        else:
            messages.add_message(request, messages.ERROR,
                                 'Username or password you provided is incorrect.')

        # prevent re-submission
        return redirect('home')
    return render_to_response("login.html",
                              {'active_page': "home",
                               'next': request.GET.get('next'),
                               'USE_AUTHENTICATION': settings.USE_AUTHENTICATION},
                              context_instance=RequestContext(request))


@login_required
def dashboard(req):
    return render_to_response("dashboard_page.html",
                              {'active_page': "home",
                               'version': dscng_version.version},
                              context_instance=RequestContext(req))


@login_required
def daily_data(req):
    return render_to_response("daily_data_page.html",
                              {'active_page': "daily_data"},
                              context_instance=RequestContext(req))

# storage API is exposed through the storage object initialized below
from dsc_storage.psql.storage import StoragePSQL
from dsc_storage.psql.connection import create_connection_manager_from_settings
storage = StoragePSQL(create_connection_manager_from_settings(settings))


# JSON interface
@json_response(login_required=True)
def get_data_types_json(req):
    """returns a list of data types"""
    accum = req.GET.get("daily") == 'true'
    data_types = storage.dtm.get_data_types()
    result = []
    t = time.time()
    for data_type in data_types:
        if data_type.accumulated == accum:
            dim_count = len(storage.dtm.get_data_type_dimensions(data_type))
            info = {"name": data_type.name, "local_name": data_type.local_name,
                    "id": data_type.id, "dim_count": dim_count}
            if dim_count == 2:
                info["subsets"] = storage.get_distinct_values(data_type, [0])
            result.append(info)
    # sort it according to name
    to_sort = [(x['local_name'], x) for x in result]
    to_sort.sort()
    result = [x[1] for x in to_sort]
    logging.debug("Get data types: %s", time.time() - t)
    return {'data': result}


@json_response(login_required=True)
def get_servers_json(req):
    """returns a list servers"""
    def make_tree(group):
        rec = {"name": group.name, "id": group.id, "type": "group"}
        children = []
        for child in parent_to_children.get(group.id, []):
            children.append(make_tree(child))
        rec['children'] = group.children + children
        return rec
    servers = storage.get_servers()
    groups = storage.get_server_groups()
    #time.sleep(1)
    parent_to_children = {}
    for group in groups:
        if group.parent_id not in parent_to_children:
            parent_to_children[group.parent_id] = [group]
        else:
            parent_to_children[group.parent_id].append(group)
        group.children = []
        for server in servers:
            if server.group_id == group.id:
                group.children.append({"name": server.name, "id": server.id})
    #
    result = []
    for group in parent_to_children.get(None, []):
        result.append(make_tree(group))
    return {'data': result}


def _get_minute_data_result(req):
    """based on the request returns the data for a graph
    if an object defined by the StorageAPI
    """
    servers = _get_selected_servers_from_req(req)
    try:
        data_type_id = int(req.GET.get("data_type", 1))
    except ValueError:
        return None
    # subset of a 3D data
    subset = req.GET.get("subset")
    fixed_values = []
    if subset:
        try:
            fixed_values.append(int(subset))
        except ValueError:
            pass
    # fixed value for 2D data and 2D slices of 3D data
    fix_value = req.GET.get('fix_value')
    if fix_value:
        fixed_values.append(fix_value)
    # load the data type
    data_type = storage.dtm.get_data_type(data_type_id=data_type_id)
    params, tz_offset = _time_params_from_req(req)
    if not params or not data_type:
        return None
    # count scaling
    count_scale = req.GET.get("count_scale", "pps")
    if count_scale == "ppm":
        count_per_second = False
    else:
        count_per_second = True
    try:
        # get the minute data
        result_obj = storage.get_minute_data_points(
                                    params['from_time'],
                                    params['to_time'],
                                    data_type,
                                    servers,
                                    fixed_values = fixed_values,
                                    number_of_points=800,
                                    count_per_second=count_per_second,
                                    zeros_for_blanks=True)
    except (ValueError, NotImplementedError):
        return None
    # prepare the data for presentation
    if result_obj.data:
        result_obj.add_post_processing_step("add_zeros_for_empty_timepoints")
        result_obj.add_post_processing_step("merge_timezone",
                                            tz_offset=tz_offset)
        if count_scale == "rel_sum1":
            result_obj.add_post_processing_step("relative_scale_sum_is_1")
        elif count_scale == "rel_max1":
            result_obj.add_post_processing_step("relative_scale_max_is_1")
    return result_obj


class DygraphDataJsonEncoder(json.JSONEncoder):
    def _to_js_date(self, datetime_obj):
        return "new Date(%d)" % (timegm(datetime_obj.replace(
            tzinfo=UTC()).timetuple()) * 1000)

    def default(self, o):
        if isinstance(o, datetime):
            return self._to_js_date(o)
        else:
            return json.JSONEncoder.default(self, o)

    def encode(self, o):
        result = json.JSONEncoder.encode(self, o)
        # this method is not really nice and rigorous, however it's better
        # than monkey-patching of JSONEncoder's _iterencode in Python 2.7
        return result.replace("\"new Date(", "new Date(")\
            .replace("000)\"", "000)")


@json_response(login_required=True)
def get_minute_data_json(req):
    """based on the request returns the data for a graph
    as series of points
    """
    _t = time.time()
    result_obj = _get_minute_data_result(req)
    if result_obj is None:
        return {"error": "No data available"}
    logging.debug("Query end: %s", time.time() - _t)
    # prepare the data for presentation
    types = list(result_obj.types)
    types.sort()
    subrecords = ["__min", "", "__max"]
    graph_data = []
    for record in result_obj.get_time_series():
        timepoint = []
        for key in types:
            timepoint.append([record.get(key+add) for add in subrecords])
        graph_data.append([record['time']] + timepoint)

    json_data = json.dumps(graph_data, cls=DygraphDataJsonEncoder)

    extra = {"data_type": result_obj.data_type.id,
             "zoom": result_obj.timespan,
             "labels": ["Time"] + types}
    result = {"data": json_data, "extra": extra}
    logging.debug("Total time: %s", time.time() - _t)
    return result


@json_response(login_required=True)
def get_time_details_json(req):
    # soon to be deprecated...
    t = time.time()
    time_str = req.GET.get("time")
    timespan = req.GET.get("zoom", "1")
    servers = _get_selected_servers_from_req(req)
    if not timespan.isdigit():
        timespan = 1
    else:
        timespan = int(timespan)
    date = datetime.fromtimestamp(int(time_str)/1000, query_help.UTC())
    data = storage.get_details_for_time(date, timespan, dimensions=(1,2),
                                        servers=servers)
    data_types = dict([(dt.name, dt) for dt in storage.dtm.get_data_types()])
    for row in data:
        data_type = data_types.get(row[0])
        if data_type:
            row[0] = data_type.local_name
    logging.debug("Details time: %s", time.time() - t)
    data.insert(0, ["Time", "Date(%s)" % time_str])
    server_names = map(lambda x: x.name,
                       filter(lambda x: x.id in servers,
                              storage.get_servers()))
    data.insert(1, ["Servers", ', '.join(server_names)])
    return data


@json_response(login_required=True)
def get_time_details_datatable_json(req):
    t = time.time()
    time_str = req.GET.get("time", "current")
    servers = _get_selected_servers_from_req(req)
    if time_str == "current":
        _min_time, max_time = storage.get_available_timespan()
        date = max_time
        time_str = int(time.mktime(max_time.timetuple()) * 1000)
    else:
        try:
            date = datetime.fromtimestamp(int(time_str)/1000, query_help.UTC())
        except ValueError:
            raise Http404
    timespan = req.GET.get("zoom", "1")
    if not timespan.isdigit():
        timespan = 1
    else:
        timespan = int(timespan)

    try:
        data = storage.get_details_for_time(date, timespan, dimensions=(1,2),
                                            servers=servers)
    except ValueError:
        raise Http404
    data_types = dict([(dt.name, dt) for dt in storage.dtm.get_data_types()])
    table_data = {}
    series = {}
    for row in data:
        subtype = len(row)>2 and str(row[1]) or ""
        datatype_obj = data_types.get(str(row[0]))
        datatype = datatype_obj.name
        if not datatype in table_data:
            table_data[datatype] = {"datatype": datatype_obj.local_name}
        series[subtype] = ("number", subtype)
        table_data[datatype][subtype] = row[-1]
    column_order = ['datatype'] + series.keys()
    series['datatype'] = ("string", "Data Type")
    data_table = gviz_api.DataTable(series)
    data = []
    for _key, val in sorted(table_data.iteritems()):
        data.append(val)
    data_table.LoadData(data)
    # convert the data table into JSON for returning
    json_data = data_table.ToJSon(columns_order=column_order)
    server_names = map(lambda x: x.name,
                       filter(lambda x: x.id in servers,
                              storage.get_servers()))
    result = {"data": json_data, "ok": True, "time": "Date(%s)" % time_str,
              "servers": ', '.join(server_names)}
    logging.debug("Details time: %s", time.time() - t)
    return result


@json_response(login_required=True)
def get_server_traffic_json(req):
    """returns json data about traffic on each server"""
    _min_time, max_time = storage.get_available_timespan()
    if max_time:
        timepoint = max_time
        data = storage.get_server_traffic(timepoint)
        series = {"server": ("string", "Server"),
                  "count": ("number", "Traffic")}
        data_table = gviz_api.DataTable(series)
        data_dict = []
        remap = storage.get_server_remap()
        for row in data:
            data_dict.append({'count': row[1]/60,
                              'server': remap.get(row[0], row[0])})
        data_table.LoadData(data_dict)
        # convert the data table into JSON for returning
        json_data = data_table.ToJSon(columns_order=['server','count'])
        result = {"data": json_data, "ok": True, "time": str(timepoint),
                  "rows_cnt": len(data)}
    else:
        result = {"data": None, "ok": False, "time": None}
    return result


@json_response(login_required=True)
def get_overall_traffic_json(req):
    """returns json data about traffic on each server"""
    _min_time, max_time = storage.get_available_timespan()
    if max_time:
        timepoint = max_time
        traffic = storage.get_overall_traffic(timepoint)
        if traffic is None:
            qps = 0.0
        else:
            qps = traffic / 60.0
        older = []
        for i in range(1, 11):
            old_val = storage.get_overall_traffic(timepoint-timedelta(days=i))
            if old_val is not None:
                older.append(old_val)
        if older:
            mean = sum(older) / len(older) / 60.0
        else:
            mean = qps
        result = {"current": qps, "mean": mean, "ok": True,
                  "time": str(timepoint)}
    else:
        result = {"current": None, "mean": None, "ok": False,
                  "time": None}
    return result


@json_response(login_required=True)
def get_data_type_detail_json(req):
    """returns json data about traffic on a specific data_type
    non-datatable twin - soon to be deprecated"""
    _min_time, max_time = storage.get_available_timespan()
    if max_time:
        timepoint = max_time
        data_type = storage.dtm.get_data_type(name="rcode")
        result_obj = storage.get_minute_data_points(timepoint, timepoint,
                            data_type, servers=None, fixed_values=None,
                            split_by_server=False,
                            number_of_points=10, count_per_second=True,
                            zeros_for_blanks=True)
        data = list(result_obj.get_time_series())
        data_dict = []
        for type in sorted(result_obj.types):
            data_dict.append({"label": type,
                              "data": data[0].get(type, 0)})
        # convert the data table into JSON for returning
        result = {"data": data_dict, "ok": True, "time": str(timepoint)}
    else:
        result = {"data": None, "ok": False, "time": None}
    return result


@json_response(login_required=True)
def get_data_type_detail_dt_json(req):
    """returns json data about traffic on a specific data_type;
    dt = DataTable format"""
    _min_time, max_time = storage.get_available_timespan()
    if max_time:
        timepoint = max_time
        data_type = storage.dtm.get_data_type(name="rcode")
        result_obj = storage.get_minute_data_points(timepoint, timepoint,
                            data_type, servers=None, fixed_values=None,
                            split_by_server=False,
                            number_of_points=10, count_per_second=True,
                            zeros_for_blanks=True)
        data = list(result_obj.get_time_series())
        if data:
            series = {"record_type": ("string", "Record type"),
                      "count": ("number", "Traffic")}
            data_table = gviz_api.DataTable(series)
            data_dict = []
            for subtype in sorted(result_obj.types):
                data_dict.append({'count': data[0].get(subtype, 0),
                                  'record_type': subtype})
            data_table.LoadData(data_dict)
            # convert the data table into JSON for returning
            json_data = data_table.ToJSon(columns_order=['record_type','count'])
            result = {"data": json_data, "ok": True, "time": str(timepoint)}
        else:
            result = {"data": None, "ok": False, "time": None}
    else:
        result = {"data": None, "ok": False, "time": None}
    return result


def _get_daily_data_result(req):
    """Helper function for functions retrieving data from DB based on a
    request"""
    servers = _get_selected_servers_from_req(req)
    try:
        data_type_id = int(req.GET.get("data_type", 1))
    except ValueError:
        return None
    # load the data type
    data_type = storage.dtm.get_data_type(data_type_id=data_type_id)
    if not data_type:
        return None
    start_date = req.GET.get("start_date", datetime.now())
    end_date = req.GET.get("end_date") or start_date
    try:
        if type(start_date) in (str, unicode):
            start_date = datetime.strptime(start_date, "%Y-%m-%d")
        if type(end_date) in (str, unicode):
            end_date = datetime.strptime(end_date, "%Y-%m-%d")
    except ValueError:
        return None
    # get the minute data
    result_obj = storage.get_daily_data(start_date, end_date, data_type,
                                        servers)
    return result_obj


def _postprocess_daily_data_result(result_obj):
    """prepare the data for output based on config in
    DAILY_DATA_DISPLAY_CONFIG"""
    # process it to the proper output format
    config = DailyDataDisplayConfig.objects.get_for_data_type(result_obj.data_type.id)

    if config.group_by:
        for dim in result_obj.dimensions:
            if dim.name == config.group_by.name:
                grouping_dimension = dim
                break
        else:
            raise ValueError("Wrong group_by dimension in config")
    else:
        grouping_dimension = result_obj.dimensions[0]
    key_count = (0 if config.chart_type == DailyDataDisplayConfig.CHART_TYPE_XY
                 else config.key_count if config.key_count else 25)
    if config.postprocess == DailyDataDisplayConfig.POSTPROCESS_GEOIP:
        result_obj.postprocess_expand_ip_with_geoip()
    types, data = result_obj.get_data(grouping_dimension, key_count=key_count,
                                      order_by=config.order_by,
                                      max_key_value=config.max_key_value,
                                      count_per_second=config.per_second,
                                      subkey_count=config.subkey_count,
                                      key_rel_threshold=config.key_rel_threshold)
    return types, data, config


@json_response(login_required=True)
def get_daily_data_json(req):
    """based on the request returns the data for a graph
    as series of points
    """
    _t = time.time()
    result_obj = _get_daily_data_result(req)
    if not result_obj:
        raise Http404
    types, data, config = _postprocess_daily_data_result(result_obj)
    if config.chart_type == DailyDataDisplayConfig.CHART_TYPE_XY:
        # this is flot type data serialization
        result = dict([(_type, []) for _type in types])
        for row in data:
            x = row['point_name']
            for _type in types:
                if _type in row:
                    result[_type].append((x, row.get(_type, 0)))
        json_data = [{'label': _type, "data": result[_type]} for _type in types]
    else:
        # this is Google visualization _type serialization
        description = {'point_name': ('string', 'Point')}
        for _type in types:
            description[_type] = ('number', _type or result_obj.data_type.name)
        data_table = gviz_api.DataTable(description)
        data_table.LoadData(data)
        # convert the data table into JSON for returning
        json_data = data_table.ToJSon(['point_name']+list(types))
    extra = {"data_type": result_obj.data_type.id,
             "chart_type": config.chart_type,
             "per_second": config.per_second}
    result = {"table_data": json_data, "extra": extra}
    logging.debug("Total time: %s", time.time() - _t)
    return result


@json_response(login_required=True)
def get_available_date_range_json(req):
    min_time, max_time = storage.get_available_timespan()
    utc = query_help.UTC()
    if min_time:
        min_time = str(min_time.astimezone(utc).date())
    if max_time:
        max_time = str(max_time.astimezone(utc).date())
    result = {'min_date': min_time,
              'max_date': max_time}
    return result


# -------------------- CSV STUFF -------------------
@login_required
def get_minute_data_csv(req):
    """based on the request returns the data for a graph
    as series of points
    """
    result_obj = _get_minute_data_result(req)
    if not result_obj:
        raise Http404

    csv_data_obj = StringIO()
    csv_writer = csv.writer(csv_data_obj)

    #print time_series
    description = {}
    types = list(result_obj.types)
    types.sort()
    full_types = []
    if result_obj.timespan > 1:
        subrecords = ["__min","","__max"]
    else:
        subrecords = [""]
    for type_str in types:
        for subrecord in subrecords:
            full_types.append(type_str+subrecord)
    description['time'] = ("datetime", "Time")
    #print time_series
    csv_writer.writerow(["Time"]+full_types)
    for record in result_obj.get_time_series():
        lrecord = [str(record['time'].replace(tzinfo=None)).replace(" ","T")]
        for key in types:
            lrecord += [record.get(key+add) for add in subrecords]
        csv_writer.writerow(lrecord)
    res = HttpResponse(csv_data_obj.getvalue(),
                       mimetype="text/csv")
    res["Content-Disposition"] = "attachment; filename=data.csv"
    return res


@login_required
def get_daily_data_csv(req):
    """based on the request returns the data in CSV format
    """
    result_obj = _get_daily_data_result(req)
    if not result_obj:
        raise Http404

    types, data, config = _postprocess_daily_data_result(result_obj)

    csv_data_obj = StringIO()
    csv_writer = csv.writer(csv_data_obj)
    #print time_series
    if config.group_by:
        dim_name = config.group_by
    else:
        dim_name = result_obj.dimensions[0].name
    csv_writer.writerow([dim_name]+types)
    types.insert(0, 'point_name')
    for record in data:
        csv_writer.writerow([record.get(subkey, 0) for subkey in types])
    res = HttpResponse(csv_data_obj.getvalue(),
                       mimetype="text/csv")
    res["Content-Disposition"] = "attachment; filename=%s.csv" % dim_name
    return res


# helpers
def _time_params_from_req(req):
    min_time, max_time = storage.get_available_timespan()
    try:
        tz_offset = -int(req.GET.get("tz_offset", 0))
        if tz_offset < -1439 or tz_offset > 1439:
            # tzinfo.utcoffset() must be in -1439 .. 1439; ignore invalid values
            tz_offset = 0
    except ValueError:
        tz_offset = 0

    if "x1" in req.GET and "x2" in req.GET:
        try:
            start = round(float(req.GET['x1']) / 1000)
            end = round(float(req.GET['x2']) / 1000)
        except ValueError:
            return None
        params = query_help.compute_time_query_params(start, end,
                                                      limit_start=min_time,
                                                      limit_end=max_time,
                                                      tz_offset=tz_offset)
    elif "rel" in req.GET:
        if min_time is None or max_time is None:
            return None
        rel = req.GET['rel']
        end_time = max_time
        if rel == 'last_month':
            start_time = end_time-timedelta(days=31)
        elif rel == 'last_week':
            start_time = end_time-timedelta(days=7)
        elif rel == 'last_day':
            start_time = end_time-timedelta(days=1)
        elif rel == 'all':
            start_time = min_time
        else:
            start_time = max(end_time-timedelta(days=7), min_time)
        if start_time == end_time:
            # there is only one time point - add a minute on both sides
            start_time -= timedelta(minutes=1)
            end_time += timedelta(minutes=1)
        params = query_help.compute_time_query_params(start_time, end_time,
                                                      tz_offset=tz_offset)
    else:
        params = query_help.compute_time_query_params(min_time, max_time,
                                                      tz_offset=tz_offset)
    return params, tz_offset


def _get_selected_servers_from_req(req):
    data = req.GET.get('servers')
    if not data:
        return ""
    ids = [part[7:] for part in data.split(",") if part.startswith("server-")]
    ids = [int(id) for id in ids if id.isdigit()]
    return tuple(ids)