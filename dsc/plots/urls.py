# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    # HTML stuff:
    url(r'^$', views.home, name="home"),
    url(r'^timegraph/$', views.timegraph, name="timegraph"),
    url(r'^daily_data/$', views.daily_data, name="daily_data"),

    # Auth
    url(r'^logout/$', views.logout, name='logout'),
    

    # JSON stuff
    (r'^json/available_date_range/$', views.get_available_date_range_json),
    (r'^json/minute_data/$', views.get_minute_data_json),
    (r'^json/data_types/$', views.get_data_types_json),
    (r'^json/servers/$', views.get_servers_json),
    (r'^json/time_details/$', views.get_time_details_json),
    (r'^json/time_details_datatable/$', views.get_time_details_datatable_json),
    (r'^json/server_traffic/$', views.get_server_traffic_json),
    (r'^json/data_type_detail/$', views.get_data_type_detail_json),
    (r'^json/data_type_detail_dt/$', views.get_data_type_detail_dt_json),
    (r'^json/overall_traffic/$', views.get_overall_traffic_json),
    (r'^json/daily_data/$', views.get_daily_data_json),
                       
    # CSV stuff
    (r'^get_minute_data_csv/$', views.get_minute_data_csv),
    (r'^get_daily_data_csv/$', views.get_daily_data_csv),

)