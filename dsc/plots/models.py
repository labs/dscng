# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from django.contrib.auth.models import AbstractUser

from django.db import models


# Unmanaged models


class ServerGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    parent = models.ForeignKey('self')

    class Meta:
        managed = False
        db_table = 'dscng_server_group'

    def __str__(self):
        return self.name


class Server(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    group = models.ForeignKey(ServerGroup)

    class Meta:
        managed = False
        db_table = 'dscng_server'

    def __str__(self):
        return self.name


class TimeRecord(models.Model):
    id = models.AutoField(primary_key=True)
    server_id = models.ForeignKey(Server, db_column="server_id")
    time = models.DateTimeField(db_index=True)
    timespan = models.PositiveIntegerField()

    class Meta:
        managed = False
        unique_together = (("server_id", "time", "timespan"),)
        db_table = 'dscng_timerecord'


class DataType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    dimensions = models.ManyToManyField("Dimension",
                                        through="DimensionToDataType")
    accumulated = models.BooleanField()
    parent = models.ForeignKey('self', db_column='parent')

    class Meta:
        managed = False
        db_table = 'dscng_datatype'

    def __str__(self):
        return self.name


class Dimension(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    type = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'dscng_dimension'

    def __str__(self):
        return self.name


class DimensionToDataType(models.Model):
    # TODO: is it possible to use composite PK in admin?
    id = models.AutoField(primary_key=True)
    data_type = models.ForeignKey(DataType)
    dimension = models.ForeignKey(Dimension)
    position = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        unique_together = (("data_type", "dimension", "position"),)
        db_table = 'dscng_dimensiontodatatype'


class DataRecord(models.Model):
    id = models.AutoField(primary_key=True)
    time_record = models.ForeignKey(TimeRecord)
    data_type = models.ForeignKey(DataType)

    class Meta:
        managed = False
        unique_together = (("time_record", "data_type"),)
        db_table = 'dscng_datarecord'


class DataPoint1D(models.Model):
    data_record = models.ForeignKey(DataRecord)
    count = models.PositiveIntegerField()
    min = models.PositiveIntegerField(null=True)
    max = models.PositiveIntegerField(null=True)

    class Meta:
        managed = False
        db_table = 'dscng_datapoint1d'


class DataPoint2D(models.Model):
    data_record = models.ForeignKey(DataRecord)
    count = models.PositiveIntegerField()
    min = models.PositiveIntegerField(null=True)
    max = models.PositiveIntegerField(null=True)
    value1 = models.PositiveIntegerField()

    class Meta:
        managed = False
        unique_together = (("data_record", "value1"),)
        db_table = 'dscng_datapoint2d'


class DataPoint3D(models.Model):
    data_record = models.ForeignKey(DataRecord)
    count = models.PositiveIntegerField()
    min = models.PositiveIntegerField(null=True)
    max = models.PositiveIntegerField(null=True)
    value1 = models.PositiveIntegerField()
    value2 = models.PositiveIntegerField()

    class Meta:
        managed = False
        unique_together = (("data_record", "value1", "value2"),)
        db_table = 'dscng_datapoint3d'


# Managed models
class User(AbstractUser):
    # Currently just same behavior as of Django's default User.
    pass


class DailyDataDisplayConfigManager(models.Manager):
    def get_for_data_type(self, data_type_id):
        try:
            return self.get(pk=data_type_id)
        except self.model.DoesNotExist:
            # return dummy config object with default values
            return DailyDataDisplayConfig(data_type=DataType(data_type_id))


class DailyDataDisplayConfig(models.Model):
    CHART_TYPE_BAR = "bar"
    CHART_TYPE_XY = "xy"
    CHART_TYPE_CHOICES = (
        (CHART_TYPE_BAR, "Bar graph"),
        (CHART_TYPE_XY, "XY plot")
    )

    POSTPROCESS_NONE = "none"
    POSTPROCESS_GEOIP = "geoip"
    POSTPROCESS_CHOICES = (
        (POSTPROCESS_NONE, "None"),
        (POSTPROCESS_GEOIP, "GeoIP"),
    )

    ORDER_BY_COUNT = "count"
    ORDER_BY_GROUP = "group"
    ORDER_BY_CHOICES = (
        (ORDER_BY_COUNT, "Count"),
        (ORDER_BY_GROUP, "Group"),
    )

    data_type = models.ForeignKey(DataType, primary_key=True)
    chart_type = models.CharField(max_length=10, choices=CHART_TYPE_CHOICES,
                                  default=CHART_TYPE_BAR)
    key_count = models.IntegerField(null=True, blank=True)
    max_key_value = models.IntegerField(blank=True, null=True)
    order_by = models.CharField(max_length=10, choices=ORDER_BY_CHOICES,
                                default=ORDER_BY_COUNT)
    group_by = models.ForeignKey(Dimension, related_name='group_by_dimension',
                                 blank=True, null=True)
    per_second = models.BooleanField(default=True)
    subkey_count = models.IntegerField(default=20)
    postprocess = models.CharField(max_length=10, choices=POSTPROCESS_CHOICES,
                                   default=POSTPROCESS_NONE)
    key_rel_threshold = models.FloatField(null=True, blank=True)

    objects = DailyDataDisplayConfigManager()

    def __str__(self):
        return self.data_type.name