#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This file is the home of a RPC server which is used to process requests for
updates of the DSCng database.
"""

# built-in modules
import logging
from optparse import OptionParser, Values
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import errno
import pwd

# local imports
from dsc_storage.update.daemon import Updater
from dsc_storage.update.config import process_options, Defaults, get_log_level
from dsc_storage.update.security import \
    AuthenticationFileError, pyro_set_daemon_authentication_from_file  

# some module level shared object
logging.basicConfig()
logger = logging.getLogger("dscng.updater")

from dsc_storage.contrib.daemon import Daemon
import Pyro.core
import signal

class PyroDaemon(Daemon):
    
    def __init__(self, pyro_daemon, db_params, pidfile, journal_dir, uid=None,
                 stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        Daemon.__init__(self, pidfile, uid=uid, logger=logger,
                        stdin=stdin, stdout=stdout, stderr=stderr)
        self.db_params = db_params
        self.pyro_daemon = pyro_daemon
        self.journal_dir = journal_dir
        signal.signal(signal.SIGTERM, self._sigterm)
        signal.signal(signal.SIGINT, self._sigint)
        self.updater = None
        
    def run(self):
        pyro_obj = Pyro.core.ObjBase()
        self.updater = Updater(self.db_params, journal_dir=self.journal_dir)
        pyro_obj.delegateTo(self.updater)
        _uri = self.pyro_daemon.connect(pyro_obj, "dsc_updater")
        Pyro.core.initServer()
        # the main loop of the daemon, anything after this call will be called 
        # after the daemon exits
        try:
            self.pyro_daemon.requestLoop()
        finally:
            self.pyro_daemon.shutdown()
            if self.updater and self.updater.active:
                # the updater is not yet in the stage of shutting down
                self.updater.shutdown(graceful=False)
            
    def _sigterm(self, signum, frame):
        logger.info("Sigterm was received - shutting down")
        if self.updater:
            logger.info("Calling immediate shutdown on updater")
            self.updater.shutdown(graceful=False)
        self.pyro_daemon.shutdown()
        
    def _sigint(self, signum, frame):
        logger.info("Sigint was received - shutting down")
        if self.updater:
            logger.info("Calling graceful shutdown on updater")
            self.updater.shutdown(graceful=True)
        self.pyro_daemon.shutdown()


class ParameterError(Exception):
    pass

class OperationError(Exception):
    pass


def run_pyro(command, db_params, options):
    """
    Starts a Pyro server exposing the Updater class
    """
    pyro_daemon = Pyro.core.Daemon(host=options.hostname, port=options.port)
    # check the desired uid
    if options.user:
        try:
            user_info = pwd.getpwnam(options.user)
        except KeyError:
            raise ParameterError("No user by name '%s' exists, please check "
                                 "your configuration." % options.user)
        uid = user_info.pw_uid
    else:
        uid = None
    # create the daemon instance
    daemon = PyroDaemon(pyro_daemon, db_params, options.pidfile,
                        options.journal_dir,
                        uid=uid,
                        stdout=options.log_file,
                        stderr=options.log_file)
    # check the command
    if command == 'start':
        # check for the authentication settings
        if options.client_authentication:
            if not options.auth_file:
                raise ParameterError(
                            "Client authentication was enabled, but no "
                            "authentication file was specified. Please use the "
                            "config file or the -f argument to specify it.")
            try:
                pyro_set_daemon_authentication_from_file(pyro_daemon,
                                                         options.auth_file)
            except (IOError, AuthenticationFileError) as exc:
                raise ParameterError("Error reading authentication file. "
                                     "Details: %s" % exc)
        # check if the pidfile exists
        if os.path.exists(options.pidfile):
            # try to read it
            try:
                pid = file(options.pidfile, 'r').read()
            except Exception:
                raise OperationError("Pid file '%s' already exists, the "
                    "daemon is probably already running. Please stop it "
                    "before attempting to start the daemon." % options.pidfile)
            else:
                try:
                    os.kill(int(pid), 0)
                except OSError as exc:
                    if exc.errno == errno.EPERM:
                        # not permitted
                        raise OperationError("Pid file '%s' already exists "
                            "and the process seems to be running. This means "
                            "that the update daemon is probably already "
                            "running. However, you do not have rights to "
                            "stop it as it was probably started by another "
                            "user. You have to find a way stop it before you "
                            "can start your own update daemon."\
                            % options.pidfile)
                    if exc.errno == errno.ESRCH:
                        # no such process
                        raise OperationError("Pid file '%s' already exists "
                            "but the corresponding process does not seem to "
                            "be running. This means that the update daemon "
                            "probably terminated in an unusual way and left "
                            "the file behind. If it does not indeed run, "
                            "you can remove the pid file or use the 'stop' "
                            "command to make the daemon remove it for you." \
                            % options.pidfile)
                    else:
                        raise OperationError("Pid file '%s' already exists, "
                            "the daemon is probably already running. Please "
                            "stop it before attempting to start the daemon."\
                            % options.pidfile)
                else:
                    raise OperationError("Pid file '%s' already exists and the "
                        "process seems to be running. This means that the "
                        "update daemon is probably already running. "
                        "Please stop it before attempting to start the daemon."\
                        % options.pidfile)
        # check if the pidfile is writable
        else:
            try:
                fobj = file(options.pidfile, "w")
            except Exception as exc:
                raise ParameterError("Pid file '%s' is not writable, please "
                                     "check the permissions.\nDetails: %s" % \
                                     (options.pidfile, exc))
            else:
                fobj.close()
                os.remove(options.pidfile)
        # check the journal dir
        if not os.path.isdir(options.journal_dir):
            raise OperationError("Journal directory '%s' does not exist, "
                                 "please create it." % options.journal_dir)
        logger.info("Starting Pyro server on %s:%d", options.hostname,
                    options.port)
        if options.foreground:
            logger.info("Daemon is staying in foreground.")
            daemon.run()
        else:
            logger.info("Daemon is going into background.")
            daemon.start()
    elif command == 'stop':
        if not os.path.exists(options.pidfile):
            raise OperationError("Pid file '%s' does not exist, the daemon "
                                 "is probably not running" % options.pidfile)
        logger.info("Doing graceful shutdown. It can take some time "
                    "before queued data are processed. Please be patient.")
        try:
            daemon.stop()
        except OSError as exc:
            if exc.errno == errno.EPERM:
                raise OperationError("Could not stop the daemon. It was "
                    "probably started by another user and you do not have "
                    "rights to stop it.\nDetails: %s" % exc)
            else:
                raise OperationError("Could not stop daemon.\nDetails: %s"\
                                     % exc)
        except Exception as exc:
            raise OperationError("Could not stop daemon.\nDetails: %s" % exc)
        else:
            logger.info("Daemon was stopped.")
    elif command == 'status':
        if not os.path.exists(options.pidfile):
            logger.info("Pid file '%s' does not exist, the daemon is not "
                         "running", options.pidfile)
        else:
            try:
                pid = file(options.pidfile, 'r').read().strip()
            except Exception:
                raise OperationError("Pid file '%s' cannot be read, cannot "
                                     "check status.", options.pidfile)
            else:
                try:
                    os.kill(int(pid), 0)
                except OSError as exc:
                    if exc.errno == errno.ESRCH:
                        # no such process
                        logger.info("Process %s is not alive, the daemon is "
                                     "not running. However, a pidfile '%s' was "
                                     "left behind, which means the daemon did "
                                     "not die a peaceful death.",
                                     pid, options.pidfile)
                    else:
                        raise OperationError("Cannot get information about "
                                             "process %s, cannot check its "
                                             "status. Details: %s" %\
                                             (pid, exc))
                else:
                    logger.info("Daemon is running under pid %s.", pid)
    else:
        raise ParameterError("Unsupported command '%s'. Command must be one "
                             "of 'start', 'stop', 'status'." % command)


def main():
    """
    This is the main code that is run when update_daemon is run
    from command line
    """
    import sys

    # optional process title modification
    try:
        from setproctitle import setproctitle
    except ImportError:
        setproctitle = lambda title: None

    opsr = OptionParser(usage="python %prog [options] command")
    opsr.add_option("-d", "--debug", action="store_true",
                    dest="debug", default=Defaults.debug,
                    help="Log debug info about what the daemon is doing")
    opsr.add_option("-p", "--port", action="store", type="int",
                    dest="port", default=Defaults.port,
                    help="Port the daemon should listen on")
    opsr.add_option("-n", "--hostname", action="store", type="string",
                    dest="hostname", default=Defaults.hostname,
                    help="Hostname (address) to listen on")
    opsr.add_option("-c", "--config", action="store", type="string",
                    dest="config_file", default="",
                    help="Config file to use (command line options override " 
                         "equivalent settings from the config file).")
    opsr.add_option("-a", "--client-auth", action="store_true",
                    dest="client_authentication",
                    default=Defaults.client_authentication,
                    help="Use client authentication (-f should be supplied as "
                         "well)")
    opsr.add_option("-f", "--auth-file", action="store", type="string",
                    dest="auth_file", default=Defaults.auth_file,
                    help="Authentication file - where authetication data "
                         "should be read from when client authentication "
                         "is enable")
    opsr.add_option("--pidfile", action="store", type="string",
                    dest="pidfile", default=Defaults.pidfile,
                    help="Pid file - where PID of running daemon should be "
                         "written.")
    opsr.add_option("-j", "--journal-dir", action="store", type="string",
                    dest="journal_dir", default=Defaults.journal_dir,
                    help="Journal dir - a directory where the update daemon "
                         "stores incomming data until it processes it.")
    opsr.add_option("-u", "--user", action="store", type="string",
                    dest="user", default=Defaults.user,
                    help="User under which the daemon should run (by using "
                         "setuid after it performed startup).")
    opsr.add_option("-l", "--log-file", action="store", type="string",
                    dest="log_file", default=Defaults.log_file,
                    help="File into which the daemon will log its activity")
    opsr.add_option("-F", "--foreground", action="store_true",
                    dest="foreground", default=Defaults.foreground,
                    help="Stay in foreground - do not 'daemonize'.")    
    default = Values()
    (options, args) = opsr.parse_args(values=default)
    if len(args) != 1:
        opsr.print_help()
        print >> sys.stderr, "\nYou must supply a command. Command is one of "\
                             "'start', 'stop', 'status'."
        return
    else:
        command = args[0]
    # check the command
    logger.setLevel(get_log_level(options))
    process_options(options, Defaults)
    # reset logging level - it might have been changed from a config file
    logger.setLevel(get_log_level(options))
    # normalization of paths
    options.journal_dir = os.path.abspath(options.journal_dir)
    options.pidfile = os.path.abspath(options.pidfile)
    # the exposed updater instance
    # at first some setup of database connection
    os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
    from dsc.dsc import settings
    from dsc_storage.psql.connection import get_connection_params_from_settings
    db_params = get_connection_params_from_settings(settings)
    run = run_pyro # use pyro server
    try:
        setproctitle(os.path.basename(sys.argv[0]))
    except Exception:
        logger.exception("Error while setting process title.")
    try:
        run(command, db_params, options)
    except KeyboardInterrupt:
        logger.info("Caught keyboard interrupt - exiting")
    except ParameterError as exc:
        opsr.print_help()
        print >> sys.stderr, "\nERROR:\n", exc
        return
    except OperationError as exc:
        print >> sys.stderr, "ERROR:\n", exc
        return


if __name__ == "__main__":
    main()