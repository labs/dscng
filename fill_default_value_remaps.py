#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import logging
from dsc.dsc import settings
from dsc_storage.psql.connection import create_connection_manager_from_settings

connection = create_connection_manager_from_settings(settings)


def read_cvs_file(filename):
    remap = {-1: "Other"}
    with file(filename, "r") as f:
        for line in f:
            if line.strip() and not line.lstrip().startswith("#"):
                data = line.strip().split("\t")
                if len(data) != 2:
                    logging.error("Wrong number of columns: %s", line.strip())
                    continue
                try:
                    orig_int = int(data[0])
                except:
                    logging.error("Wrong format of integer: %s", data[0])
                    continue
                remap[orig_int] = data[1]
    return remap

def process_cvs_file(filename, dimension_id):
    remap = read_cvs_file(filename)
    cursor = connection.cursor()
    cursor.execute("""SELECT original, remap
        FROM dscng_int_to_str_remap
        WHERE dimension_id = %s""", (dimension_id,))
    for orig, replace in cursor.fetchall():
        if orig in remap and remap[orig] == replace:
            del remap[orig]
    to_insert = [(dimension_id,k,v) for k,v in remap.iteritems()]
    logging.debug("Updating - values %s", remap)
    cursor.executemany("INSERT INTO dscng_int_to_str_remap VALUES (%s,%s,%s)",
                       to_insert)
    connection.commit()
    
    
def process_dir(dirname):
    cursor = connection.cursor()
    cursor.execute("SELECT id, name FROM dscng_dimension")
    for dim_id, name in cursor.fetchall():
        cvs_fname = os.path.join(dirname, name + ".remap.csv")
        if os.path.exists(cvs_fname):
            logging.info("Processing %s", cvs_fname)
            process_cvs_file(cvs_fname, dim_id)
            
if __name__ == "__main__":
    import sys
    logging.basicConfig(level=logging.DEBUG)
    if len(sys.argv) <= 1:
        print >> sys.stderr, "Give me a name of directory containing the remaps"
        print >> sys.stderr, "Normally it would be the 'data/' directory in \
DSCng distribution."
        sys.exit(100)
    process_dir(sys.argv[1])
