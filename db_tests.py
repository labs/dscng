#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains unittests for the DSCng storage subsystem.
It can be run from the command line like this
 > python db_tests.py
 
However, some parts assume that you have at least two test databases set up
which might not necessarily be the case. 
"""

# built-in imports
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import unittest
import logging
logging.basicConfig(level=logging.WARNING)
from datetime import timedelta

# local imports
from dsc_storage.psql.storage import StoragePSQL
from dsc_storage.psql.connection import create_connection_manager_from_settings
from dsc_storage.psql.query_help import round_time_to_timespan
from tests_common import compare_lists

logging.getLogger('dsc_storage').setLevel(logging.ERROR)


class TestDBConsistency(unittest.TestCase):
    
    """
    Tests whether caching data for minute data are consistent.
    """
    
    def setUp(self):    
        """
        set up the test - make a database connection
        """
        from dsc.dsc import settings
        self.conn = create_connection_manager_from_settings(settings)
        
    def tearDown(self):
        """
        Close database connection
        """
        self.conn.close()
        
    def test_caching_data(self):
        """
        Fetch both minute and cached data and compare it
        """
        cursor = self.conn.cursor()
        # imported data
        storage = StoragePSQL(self.conn)
        data_types = storage.dtm.get_data_types()
        to_sort = [(dt.name, dt) for dt in data_types if not dt.accumulated]
        to_sort.sort()
        for _dt_name, data_type in to_sort:
            # fetch minute data
            date_to_data = {}
            dims = storage.dtm.get_data_type_dimensions(data_type)
            dim_count = len(dims)
            val_cols = ",".join(["value%d" % (i+1) for i in range(dim_count)])
            if val_cols:
                val_cols = ","+val_cols
            select = """SELECT server_id,time,minute,count%s
                FROM dscng_data%dd 
                WHERE data_type_id = %%s AND count != -1""" % (val_cols,
                                                               dim_count+1)
            cursor.execute(select, (data_type.id,))
            for row in cursor:
                server_id, time, minute, count = row[:4]
                key = tuple([row[4+i] for i in range(dim_count)])
                full_time = time + timedelta(minutes=minute)
                if full_time not in date_to_data:
                    date_to_data[full_time] = []
                date_to_data[full_time].append(((server_id,) + tuple(key), 
                                                count))
            # fetch and check caching data
            for cpnt in StoragePSQL.caching_points:
                real_cdata = [] # fetched from db
                # get cached data from db
                select = """SELECT server_id,time,minute,avg_count,
                    min_count,max_count%s
                    FROM dscng_data%dd_c%d
                    WHERE data_type_id = %%s AND avg_count != -1""" % \
                    (val_cols, dim_count+1, cpnt)
                cursor.execute(select, (data_type.id,))
                for row in cursor:
                    server_id, time, minute = row[:3]
                    counts = tuple(row[3:6])
                    key = tuple([row[6+i] for i in range(dim_count)])
                    full_time = time + timedelta(minutes=minute*cpnt)
                    real_cdata.append((full_time, (server_id,) +
                                       tuple(key), counts))
                # compute cached data from input
                comp_cdata = self._create_caching_data(date_to_data, cpnt)
                # check the data
                comp_cdata.sort()
                real_cdata.sort()
                diff = set(comp_cdata)^set(real_cdata)
                if diff:
                    logging.warning("Mismatch in data: %s, %d vs %d (%d diff)",
                                    _dt_name, len(comp_cdata), len(real_cdata),
                                    len(diff))
                    for side, row in compare_lists(comp_cdata, real_cdata):
                        print side, row
                self.assertEqual(real_cdata, comp_cdata)
        cursor.close()

    def test_server_timespans(self):
        """
        Check that timespan data exist for all servers and that they contain
        the right values
        """
        cursor = self.conn.cursor()
        # imported data
        storage = StoragePSQL(self.conn)
        servers = storage.get_servers()
        for server in servers:
            cursor.execute("""SELECT start_time, end_time
                FROM dscng_servertimespan WHERE server_id = %s""",
                (server.id,))
            rec = cursor.fetchone()
            if rec is None:
                # there is no record - the only valid way for this to happen
                # is if there is also no data for this server (it is new or
                # it is a parent of some group)
                cursor.execute(
                    "SELECT COUNT(*) FROM dscng_timerecord WHERE server_id=%s",
                    (server.id,))
                if cursor.fetchone()[0] != 0:
                    self.fail(
                        "No timespan record but data present for server %s" %\
                        server.name)
                continue
            # this is where we process servers with actual data
            start_time_db, end_time_db = rec
            # here we compute the data from the db
            # end time
            cursor.execute("""SELECT time, max(minute) 
                FROM dscng_data1d 
                WHERE time=(SELECT MAX(time) FROM dscng_data1d 
                        WHERE time >= (SELECT MAX(time)
                           FROM dscng_timerecord_c WHERE server_id = %s) 
                           AND server_id = %s)
                    AND server_id = %s
                    AND count > -1
                GROUP BY time""", (server.id, server.id, server.id))
            tm_base, minute = cursor.fetchone()
            end_time_comp = tm_base + timedelta(minutes=minute)
            self.assertEqual(end_time_comp, end_time_db,
                             "End times stored in db and computed differ")
            # start time
            cursor.execute("""SELECT time, MIN(minute) 
                FROM dscng_data1d 
                WHERE time=(SELECT MIN(time) FROM dscng_data1d 
                        WHERE time >= (SELECT MIN(time)
                           FROM dscng_timerecord_c WHERE server_id = %s) 
                           AND server_id = %s)
                    AND server_id = %s
                    AND count > -1
                GROUP BY time""", (server.id, server.id, server.id))
            tm_base, minute = cursor.fetchone()
            start_time_comp = tm_base + timedelta(minutes=minute)
            self.assertEqual(start_time_comp, start_time_db,
                             "Start times stored in db and computed differ")

        cursor.close()
        
        
        
    @classmethod
    def _create_caching_data(cls, date_to_data, caching_point):
        """
        computes aggregated caching data based on raw minute input data.
        """
        if not date_to_data:
            return []
        start_time = round_time_to_timespan(min(date_to_data.keys()), 1440,
                                            upper=False)
        end_time = round_time_to_timespan(max(date_to_data.keys()), 1440,
                                          upper=True)
        result = []
        while start_time < end_time:
            key_to_counts = {}
            for mnt in xrange(1440):
                cur_time = start_time + timedelta(minutes=mnt)
                data_set = date_to_data.get(cur_time)
                if data_set:
                    for (key, count) in data_set:
                        if key not in key_to_counts:
                            key_to_counts[key] = []
                        key_to_counts[key].append(count)
                if (mnt+1) % caching_point == 0:
                    st_tm = cur_time - timedelta(minutes=caching_point-1)
                    for key, counts in key_to_counts.iteritems():
                        maxc = max(counts)
                        minc = min(counts)
                        avgc = sum(counts) // len(counts)
                        result.append((st_tm, key, (avgc, minc, maxc))) 
                    key_to_counts = {}
            start_time += timedelta(days=1)
        return result
    
    
    
if __name__ == "__main__":
    unittest.main()
    #suite = unittest.TestLoader().loadTestsFromTestCase(
    #                                            TestDBConsistency)
    #unittest.TextTestRunner(verbosity=2).run(suite)
    
    