#
# This code was obtained on 2012-07-23 from
# http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/
# where it was explicitly placed into public domain by its author
# Sander Marechal.
# All credits belong to the above mentioned author
#
# Adapted for DSCng by Beda Kosata, CZ.NIC Labs
#

"""
Implementation of basis of daemon processes in UNIX
"""

import sys, os, time, atexit
from signal import SIGINT
import errno
import logging
import stat


class Daemon:
    """
    A generic daemon class.

    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null',
                 stderr='/dev/null', uid=None, logger=None):
        self.logger = logger or logging.getLogger("daemon")
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.uid = uid # daemon will change its uid to this using os.setuid

    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                os._exit(0)
        except OSError as exc:
            self.logger.critical("fork #1 failed: %d (%s)", 
                                 exc.errno, exc.strerror)
            os._exit(1)

        # decouple from parent environment
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                os._exit(0)
        except OSError as exc:
            self.logger.critical("fork #2 failed: %d (%s)", exc.errno, 
                                 exc.strerror)
            os._exit(1)

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        sti = file(self.stdin, 'r')
        sto = file(self.stdout, 'a')
        os.fchmod(sto.fileno(), stat.S_IRUSR|stat.S_IWUSR|stat.S_IRGRP)
        if self.stderr != self.stdout:
            ste = file(self.stderr, 'a', 0)
            os.fchmod(ste.fileno(), stat.S_IRUSR|stat.S_IWUSR|stat.S_IRGRP)
        else:
            ste = sto
        os.dup2(sti.fileno(), sys.stdin.fileno())
        os.dup2(sto.fileno(), sys.stdout.fileno())
        os.dup2(ste.fileno(), sys.stderr.fileno())

        # write pidfile
        atexit.register(self.save_delpid)
        pid = str(os.getpid())
        file(self.pidfile,'w').write("%s\n" % pid)

    def delpid(self):
        """
        Remove the pidfile of this daemon process
        """
        os.remove(self.pidfile)

    def save_delpid(self):
        """
        Remove the pidfile, quietly ignore any exceptions
        """
        try:
            self.delpid()
        except:
            pass

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        try:
            with file(self.pidfile,'r') as pidf:
                pid = int(pidf.read().strip())
        except IOError:
            pid = None

        if pid:
            self.logger.error("pidfile %s already exist. Daemon already "
                              "running?",  self.pidfile)
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        if self.uid:
            try:
                os.setuid(self.uid)
            except Exception as exc:
                self.logger.error("Could not set uid to %d (error: %s)",
                                  self.uid, exc)
                sys.exit(1)
            else:
                self.logger.debug("Switched to uid %d", self.uid)
        self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        try:
            with file(self.pidfile,'r') as pidf:
                pid = int(pidf.read().strip())
        except IOError:
            pid = None

        if not pid:
            self.logger.error("pidfile %s does not exist. Daemon not running?",
                              self.pidfile)
            return # not an error in a restart

        # Try killing the daemon process
        try:
            os.kill(pid, SIGINT)
            while 1:
                os.kill(pid, 0)
                time.sleep(0.5)
        except OSError as exc:
            if exc.errno == errno.ESRCH:
                # no such process
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                raise

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be
        called after the process has been daemonized by start() or restart().
        """
        pass
