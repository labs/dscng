# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This file contains classes that are relevant to the update daemon
implementation, namely the interface for incoming raw data.

RawBundleConverter takes care of converting incoming RawUpdateBundles
for UpdateBundles that are suitable for direct processing by Storage.
"""

# built-in modules
import logging

# local imports
from dsc_storage.psql.storage import DataRecord, UpdateBundle

# some module level shared object
logger = logging.getLogger("dscng.updater")

class RawBundleConverter(object):
    """
    RawBundleConverter takes care of converting incoming RawUpdateBundles
    for UpdateBundles that are suitable for direct processing by Storage.
    """
    
    def __init__(self, storage, update_callback):
        self.storage = storage
        self.dtm = self.storage.dtm
        self.update_callback = update_callback
    
    @classmethod
    def _decode_int_value(cls, value):
        if value.isdigit():
            return int(value)
        return value

    def process_raw_update_bundle(self, raw_bundle):
        """
        Processes one RawUpdateBundle instance and calls update_callback
        on the resulting UpdateBundle.
        As a side effect of this call, server instance might be created
        """
        # server_id
        parent_name, server_name = raw_bundle.server
        server_id = self.storage.get_or_create_server(server_name, parent_name)
        final_bundle = UpdateBundle(server_id=server_id,
                                    timepoint=raw_bundle.timepoint)
        # source id and data type processing
        source_id = self.dtm.get_data_source_id(raw_bundle.source_type)
        in_data_types = self.dtm.get_incoming_data_types(source_id)
        in_dimensions = self.dtm.get_incoming_dimensions(source_id)
        dims = self.dtm.get_dimensions()
        dim_id_to_dim = dict([(dim.id, dim) for dim in dims])
        for record in raw_bundle.records:
            for final_record in self._process_raw_record(record, in_data_types,
                                                        in_dimensions,
                                                        dim_id_to_dim):
                final_bundle.records.append(final_record)
        self.update_callback(final_bundle)
    
    def _process_raw_record(self, raw_record, in_data_types, in_dimensions,
                           dim_id_to_dim):
        """
        This method is called for individual raw records in a bundle.
        It is a generator yielding processed bundles (there might be more
        than one record created from one raw record)
        """
        db_dt_defs = in_data_types.get(raw_record.data_type_name)
        if db_dt_defs is None:
            logging.warn("No processing defined for incoming data type %s" %\
                         raw_record.data_type_name)
            return
        # create a DataRecord based on what we have already
        record = DataRecord(data_type_name=raw_record.data_type_name,
                            start_time=raw_record.start_time)
        record.data = raw_record.data
        # clean up the dimensions - "All" is a dummy dimension
        record.dimensions = [dim for dim in raw_record.dimensions
                             if dim != "All"]
        # there may be multiple processings defined for one incoming
        # data type
        for rec_idx, (dt_id, processing) in enumerate(db_dt_defs):
            if rec_idx == len(db_dt_defs)-1:
                # this is the last processing of this record - use it as is
                cur_rec = record
            else:
                cur_rec = record.clone()
            # get the data type and database dimensions
            cur_rec.data_type = self.dtm.get_data_type(data_type_id=dt_id)
            # mapping of dimensions from incoming to db
            dim_remap = in_dimensions.get(dt_id, [])
            # add processing into the record
            cur_rec.processing = processing
            # fix the dimension count
            # - if processing is reduction and mapped data type has one more
            #   dim less then we need to remove the last dimension
            db_dims = self.dtm.get_data_type_dimensions(cur_rec.data_type)
            if processing in ("reduce-trace", "reduce-count") and \
                len(db_dims) == (len(cur_rec.dimensions)-1):
                cur_rec.dimensions = cur_rec.dimensions[:-1]   
            remap_dims = []
            for dim in cur_rec.dimensions:
                if dim not in dim_remap:
                    raise ValueError("Unknown dimension '%s' for data type"
                                     " '%s'" % (dim, cur_rec.data_type.name))
                else:
                    remap_dim = dim_remap[dim]
                    remap_dims.append(remap_dim)
            # do some checks on arriving and database dimensions
            db_dim_ids = [db_dim.id for db_dim in db_dims]
            if set(db_dim_ids) != set(remap_dims):
                logging.critical("%s vs. %s", db_dim_ids, remap_dims)
                raise ValueError("The set of dimension for recorded and "\
                                 "incoming data type differs!")
            # check the order of dimensions - in some cases it is not the same
            # as in the text files produced by DSC
            if len(remap_dims) == 2:
                if db_dim_ids != remap_dims and db_dim_ids[1] == remap_dims[0] \
                    and db_dim_ids[0] == remap_dims[1]:
                    # we have to reverse the order
                    remap_dims.reverse()
                    for row in cur_rec.data:
                        row[0], row[1] = row[1], row[0]
            # process values according to dimension type
            for row in cur_rec.data:
                for i, dim_id in enumerate(remap_dims):
                    dim = dim_id_to_dim[dim_id]
                    if dim.type == "int":
                        row[i] = self._decode_int_value(row[i])
            # finalize the record
            cur_rec.dimensions = db_dims
            self._current_record_time = cur_rec.start_time
            # yield the current record and continue processing
            yield cur_rec
            

class RawUpdateBundle(object):
    """
    Object describing one piece of data. It contains data for one server
    at one time. 
    It is very similar to from dsc_storage.psql.storage.UpdateBundle, but does
    not know anything that has to be dug out of the database.
    It is the role of an importer to convert RecordBundles into UpdateBundles
    """
    
    def __init__(self, source_type, server=None, timepoint=None):
        self.source_type = source_type
        self.server = server
        self.timepoint = timepoint
        self.records = []
        
    def __unicode__(self):
        return "RecordBundle: server: %s, time: %s, %d records" % \
            (self.server, self.timepoint, len(self.records))
        
    def __str__(self):
        return unicode(self).encode('utf-8')
    

class RawDataRecord(object):
    """
    Intermediate object for storage of data of one data type. It is closely
    related to dsc_storage.psql.storage.DataRecord, but does not have any
    information that must be obtained from the database.
    It is used by the RecordBundle to store data for individual data types.
    """
    
    def __init__(self, data_type_name=None, start_time=None):
        self.data_type_name = data_type_name
        self.start_time = start_time
        self.dimensions = []
        self.data = []
        
    def __unicode__(self):
        return "RawDataRecord: %s (%s)" % (self.data_type_name,
                                           self.start_time)
        
    def __str__(self):
        return unicode(self).encode('utf-8')
