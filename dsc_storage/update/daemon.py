# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This file contains classes that are relevant to the update daemon implementation
"""

# built-in modules
import logging
from multiprocessing import Queue, Process, TimeoutError
from Queue import Empty
import time
import os
from tempfile import NamedTemporaryFile
import cPickle

# local imports
from dsc_storage.psql.storage import StoragePSQL
from converter import RawBundleConverter

# some module level shared object
logger = logging.getLogger("dscng.updater")


class BundlePusher(Process): 
    
    QUEUE_CAPACITY = 100
    
    class RunningStats(object):
        def __init__(self):
            self.start_time = time.time()
            self.bundle_count = 0
            self.insert_time = 0
        def bundle_inserted(self, timing):
            self.bundle_count += 1
            self.insert_time += timing
        def __str__(self):
            if self.insert_time:
                bps = self.bundle_count / self.insert_time
            else:
                bps = float("NaN")
            wall_time = time.time() - self.start_time
            return ("DB pusher stats: {0} bundles in {1:.1f} s; {2:.2f} bps; "
                    "wall clock time {3:.1f} s; utilization {4:.1f}%"
                    .format(self.bundle_count, self.insert_time, bps, wall_time,
                            100*self.insert_time/wall_time))
    
    def __init__(self, storage, *args, **kwargs):
        Process.__init__(self, *args, **kwargs)
        self.error = False
        self.storage = storage
        self.bundle_queue = Queue(self.QUEUE_CAPACITY)
        self.stats = self.RunningStats()
        self.converter = RawBundleConverter(self.storage,
                                            self.insert_update_bundle)
        self.parent_pid = None

    def run(self):
        """
        The is the main loop waiting for data from the data_queue
        """
        self.parent_pid = os.getppid()
        logger.debug("Starting BundlePusher")
        end = False
        try:
            while not ((end and self.bundle_queue.empty()) or self.error):
                try:
                    data = self.bundle_queue.get(timeout=1)
                except (TimeoutError, Empty):
                    self.storage.conn.close()
                    # check if the parent is alive
                    if os.getppid() != self.parent_pid:
                        # the parent has changed - the originally is supposedly
                        # dead
                        logger.info("Parent process seems to be dead, exiting.")
                        break
                    else:
                        continue
                if data:
                    self.storage.conn.open()
                    journal_name, bundle = data
                    logger.debug("Pusher - got %s", bundle)
                    start_tm = time.time()
                    self.converter.process_raw_update_bundle(bundle)
                    self.stats.bundle_inserted(time.time()-start_tm)
                    logger.debug(self.get_stats_str())
                    try:
                        os.remove(journal_name)
                    except Exception as exc:
                        logger.error("Could not remove journal file '%s' - %s", 
                                     journal_name, exc)
                    else:
                        logger.debug("Removed journal file '%s'", journal_name) 
                else:
                    end = True
        except KeyboardInterrupt:
            pass  # just normally close after keyboard interrupt
        except Exception:
            logger.exception("BundlePusher unexpectedly crashed.")
        logger.debug("END OF PUSHER THREAD")
        logger.debug(self.get_stats_str())
        
    def get_stats_str(self):
        return "{0}; Queue fill {1:d}/{2:d}".format(self.stats,
                                                    self.bundle_queue.qsize(),
                                                    self.QUEUE_CAPACITY)

    def insert_update_bundle(self, bundle):
        self.storage.insert_update_bundle(bundle)


class Updater(object):
    """
    This is the class that takes care of the update tasks.
    It is meant to be exposed to the outside using some RPC mechanism
    """
    
    def __init__(self, db_params, journal_dir=".dscng_journal"):
        self.db_params = db_params
        self.subprocesses = []
        self.bundle_queues = []
        self._start_subprocesses()
        self.active = True
        self.journal_dir = journal_dir
        self._read_journal()
    
    def _start_subprocesses(self):
        for i in range(1):
            logger.debug("Starting storage pushing process #%d", i)
            storage = StoragePSQL(self.db_params)
            sub = BundlePusher(storage)
            self.subprocesses.append(sub)
            self.bundle_queues.append(sub.bundle_queue)
            sub.daemon = False
            sub.start()
            
    def process_update_bundle(self, bundle):
        logger.debug("Received: %s", bundle)
        if not self.active:
            raise Exception("Daemon is not accepting data - it is shutting "
                            "down.")
        # save data into the journal
        filename = self._journal_bundle(bundle)
        self._put_bundle_to_queue(filename, bundle)
                
    def _put_bundle_to_queue(self, journal_name, bundle):
        emptiest = None
        emptiest_size = None
        for queue in self.bundle_queues:
            if queue.empty():
                logger.debug("Found empty queue")
                queue.put((journal_name, bundle))
                break
            else:
                if not emptiest_size or emptiest_size > queue.qsize():
                    emptiest_size = queue.qsize()
                    emptiest = queue
        else:
            if emptiest:
                logger.debug("Bundles in emptiest queue: %d", emptiest_size)
                emptiest.put((journal_name, bundle))
                
    def _journal_bundle(self, bundle):
        """
        Puts serialized bundle into the journal and returns filename of the
        corresponding file.
        """
        with NamedTemporaryFile(dir=self.journal_dir, mode="w",
                                suffix=".bundle", prefix="dsc",
                                delete=False) as temp:
            cPickle.dump(bundle, temp)
            return temp.name
        
    def _read_journal(self):
        logger.debug("Reading journal")
        for fname in os.listdir(self.journal_dir):
            if fname.endswith(".bundle"):
                fullname = os.path.join(self.journal_dir, fname)
                try:
                    with file(fullname, "r") as fobj:
                        bundle = cPickle.load(fobj)
                except Exception as exc:
                    logger.error("Could not read journal bundle '%s' - %s",
                                 fullname, exc)
                else:
                    logger.debug("Read journal bundle '%s'", fullname)
                    self._put_bundle_to_queue(fullname, bundle)
    
    def shutdown(self, graceful=True):
        """
        Shut down the daemon. If graceful is True, than it waits until all data
        from queues are processes, otherwise it removes it at first and end
        immediately.
        """
        self.active = False
        try:
            for sub in self.subprocesses:
                queue = sub.bundle_queue
                if not graceful or not sub.is_alive():
                    while not queue.empty():
                        try:
                            logger.debug("Removing '%s' from queue '%s'",
                                         queue.get_nowait(), queue)
                        except Empty:
                            break
                queue.put(None)
                while not queue.empty() and sub.is_alive():
                    pass
        except KeyboardInterrupt:
            pass

    def test(self):
        return True