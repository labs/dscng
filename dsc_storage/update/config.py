# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This file is the home of a RPC server which is used to process requests for
updates of the DSCng database.
"""

# built-in modules
import logging
import ConfigParser

# some module level shared object
logger = logging.getLogger("dscng.updater")

TYPE_TO_METH = {int: "getint",
                bool: "getboolean",
                str: "get"}

DAEMON_CONFIG_STRUCT = (
    ("daemon", (("port", int),
                ("hostname", str),
                ("debug", bool),
                ("foreground", bool),
                ("pidfile", str),
                ("journal_dir", str),
                ("user", str),
                )),
    ("authentication", (("auth_file", str),
                        ("client_authentication", bool),
                        )),
    ("logging", (("log_file", str),
                 )),
    )

class Defaults:
    """
    Default values used for daemon configuration
    """
    hostname = "localhost"
    port = 7777
    debug = False
    client_authentication = False
    auth_file = ""
    foreground = False
    pidfile = "/var/run/dscng-update-daemon.pid"
    journal_dir = "/var/cache/dscng-update-journal/"
    user = "root"
    log_file = "/var/log/dscng-update-daemon.log"
    

def read_config_file(filename, struct):
    config = ConfigParser.RawConfigParser()
    try:
        config.read(filename)
    except IOError as exc:
        logger.critical("Cannot load config file '%s'. Error %s",
                        filename, exc)
        raise
    # process the config file and replace non-default values from it
    config_vals = {}
    for subsec, items in struct:
        for opt, typ in items:
            meth_name = TYPE_TO_METH.get(typ, str)
            meth = getattr(config, meth_name)
            if config.has_option(subsec, opt):
                try:
                    value = meth(subsec, opt)
                except:
                    logger.warn("Cannot read config file value '%s' in section "
                                "'%s'",
                                opt, subsec)
                else:
                    config_vals[opt] = value
                    logger.debug("Reading value from config file '%s/%s': '%s'",
                                 subsec, opt, value)
    return config_vals


def process_options(options, defaults):
    """
    Takes parsed command line options and set of default values and 
    creates a dictionary describing all the options.
    It takes into account data in a potential config file.
    """
    # check the config file
    if hasattr(options, "config_file") and options.config_file:
        logger.debug("Reading config file: '%s'", options.config_file)
        config_vals = read_config_file(options.config_file,
                                       DAEMON_CONFIG_STRUCT)
    else:
        config_vals = {}
    # put it all together
    keys = [key for key in defaults.__dict__ if not key.startswith("_")]
    for key in keys:
        if hasattr(options, key):
            # command line options have highest priority
            logger.debug("Using command line value for '%s'='%s'",
                         key, getattr(options, key))
            pass
        elif key in config_vals:
            setattr(options, key, config_vals[key])
            logger.debug("Using config file value for '%s'='%s'",
                         key, config_vals[key])
        else:
            setattr(options, key, getattr(defaults, key))
            logger.debug("Using default value for '%s'='%s'",
                         key, getattr(defaults, key))
            
def get_log_level(options):
    """
    Helper function to determine log level based on the options settings.
    """
    if hasattr(options, "debug") and options.debug:
        return logging.DEBUG
    return logging.INFO
