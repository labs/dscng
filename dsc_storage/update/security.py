# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains security related code that is used to validate RPC
connections to the update daemon, etc.
"""

class SecurityError(Exception):
    pass

class AuthenticationFileError(SecurityError):
    pass


def read_authentication_file(filename):
    """
    A very simple function that reads passwords stored on individual lines
    of a text file.
    """
    passwords = []
    with file(filename, "r") as pass_file:
        for line in pass_file:
            data = line.strip()
            if data:
                passwords.append(data)
    if not passwords:
        raise AuthenticationFileError("No authentication data found in file.")
    return passwords

def pyro_set_daemon_authentication_from_file(daemon, filename):
    passwords = read_authentication_file(filename)
    daemon.setAllowedIdentifications(passwords)

    
def pyro_set_client_authentication_from_file(client, filename):
    passwords = read_authentication_file(filename)
    if passwords:
        client._setIdentification(passwords[0])
    else:
        raise AuthenticationFileError("No passwords found in file.")
