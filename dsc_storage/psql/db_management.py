# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import logging
import re

# local imports
from storage import StoragePSQL
from version import DB_SCHEMA_VERSION


COPY_TABLE_SQL = """CREATE TABLE %s
 (LIKE %s INCLUDING INDEXES INCLUDING CONSTRAINTS);"""
 
FK_CONSTRAINT_SQLS = [
    ("time_record_id",
     """ALTER TABLE %s ADD CONSTRAINT 
        %s 
        foreign key (time_record_id)
        references dscng_timerecord_c (id) on delete cascade;"""),
    ("data_type_id",
     """ALTER TABLE %s ADD CONSTRAINT
        %s
        foreign key (data_type_id)
        references dscng_datatype (id) on delete cascade;
    """)]

CREATE_VIEW_SQLS = {1: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_c.server_id,dscng_timerecord_c.time,
  %(table_name)s.data_type_id,
  unnest(%(table_name)s.avg_counts) AS avg_count,
  unnest(%(table_name)s.min_counts) AS min_count,
  unnest(%(table_name)s.max_counts) AS max_count,
  generate_subscripts(%(table_name)s.avg_counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord_c 
    ON %(table_name)s.time_record_id = dscng_timerecord_c.id;
""",
2: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_c.server_id,dscng_timerecord_c.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,
  unnest(%(table_name)s.avg_counts) AS avg_count,
  unnest(%(table_name)s.min_counts) AS min_count,
  unnest(%(table_name)s.max_counts) AS max_count,
  generate_subscripts(%(table_name)s.avg_counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord_c 
    ON %(table_name)s.time_record_id = dscng_timerecord_c.id;
""",
3: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_c.server_id,dscng_timerecord_c.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,%(table_name)s.value2,
  unnest(%(table_name)s.avg_counts) AS avg_count,
  unnest(%(table_name)s.min_counts) AS min_count,
  unnest(%(table_name)s.max_counts) AS max_count,
  generate_subscripts(%(table_name)s.avg_counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord_c 
    ON %(table_name)s.time_record_id = dscng_timerecord_c.id;
"""
}

CREATE_UNCACHED_VIEW_SQLS = {1: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord.server_id,dscng_timerecord.time,
  %(table_name)s.data_type_id,
  unnest(%(table_name)s.counts) AS count,
  generate_subscripts(%(table_name)s.counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord 
    ON %(table_name)s.time_record_id = dscng_timerecord.id;
""",
2: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord.server_id,dscng_timerecord.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,
  unnest(%(table_name)s.counts) AS count,
  generate_subscripts(%(table_name)s.counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord 
    ON %(table_name)s.time_record_id = dscng_timerecord.id;
""",
3: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord.server_id,dscng_timerecord.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,%(table_name)s.value2,
  unnest(%(table_name)s.counts) AS count,
  generate_subscripts(%(table_name)s.counts, 1)-1 as minute
FROM %(table_name)s 
  JOIN dscng_timerecord 
    ON %(table_name)s.time_record_id = dscng_timerecord.id;
"""
}

CREATE_ACCUM_VIEW_SQLS = {
2: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,
  %(table_name)s.count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
""",
3: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,%(table_name)s.value2,
  %(table_name)s.count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum 
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
"""
   }

CREATE_ACCUM_IP_VIEW_SQLS = {
2: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,
  %(table_name)s.count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
""",
3: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,
  %(table_name)s.value2,%(table_name)s.count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum 
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
"""
   }

CREATE_ACCUMSKIPPED_VIEW_SQLS = {
2: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.skipped,
  %(table_name)s.skipped_count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
""",
3: """
CREATE VIEW %(view_name)s AS 
SELECT
  dscng_timerecord_accum.server_id,dscng_timerecord_accum.time,
  %(table_name)s.data_type_id,%(table_name)s.value1,%(table_name)s.skipped,
  %(table_name)s.skipped_count
FROM %(table_name)s 
  JOIN dscng_timerecord_accum
    ON %(table_name)s.time_record_id = dscng_timerecord_accum.id;
"""
   }

STORED_PROCEDURES = """
CREATE OR REPLACE FUNCTION 
    update_accum_data_3d(data dscng_datapoint3d_accum[]) RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_datapoint3d_accum SET count=count+(data[i].count) 
            WHERE time_record_id=data[i].time_record_id 
                AND data_type_id=data[i].data_type_id 
                AND value1=data[i].value1 
                AND value2=data[i].value2;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
            INSERT INTO dscng_datapoint3d_accum 
                (time_record_id,data_type_id,value1,value2,count) 
                VALUES
                (data[i].time_record_id,data[i].data_type_id,data[i].value1,
                data[i].value2,data[i].count);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION 
    update_accum_data_2d(data dscng_datapoint2d_accum[]) RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_datapoint2d_accum SET count=count+data[i].count 
            WHERE time_record_id=data[i].time_record_id
                AND data_type_id=data[i].data_type_id
                AND value1=data[i].value1;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
            INSERT INTO dscng_datapoint2d_accum
                (time_record_id,data_type_id,value1,count) 
                VALUES
                (data[i].time_record_id,data[i].data_type_id,data[i].value1,
                data[i].count);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION
    update_accum_data_2d_ip(data dscng_datapoint2d_accum_ip[])
    RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_datapoint2d_accum_ip SET count=count+data[i].count 
            WHERE time_record_id=data[i].time_record_id
                AND data_type_id=data[i].data_type_id
                AND value1=data[i].value1;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
              INSERT INTO dscng_datapoint2d_accum_ip
                  (time_record_id,data_type_id,value1,count) 
                  VALUES
                  (data[i].time_record_id,data[i].data_type_id,data[i].value1,
                  data[i].count);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION
    update_accum_data_3d_ip(data dscng_datapoint3d_accum_ip[])
    RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_datapoint3d_accum_ip SET count=count+(data[i].count) 
            WHERE time_record_id=data[i].time_record_id
                AND data_type_id=data[i].data_type_id
                AND value1=data[i].value1
                AND value2=data[i].value2;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN                                                                     
            INSERT INTO dscng_datapoint3d_accum_ip
                (time_record_id,data_type_id,value1,value2,count) 
                VALUES
                (data[i].time_record_id,data[i].data_type_id,data[i].value1,
                data[i].value2,data[i].count);
        END IF;
    END LOOP;
END;                
$$ LANGUAGE plpgsql;

"""

CACHE_UPDATE_STORED_PROCEDURES = """
CREATE OR REPLACE FUNCTION 
    update_cache_data_3d_c%(cp)d(tr_id int, dt_id int, index int,
    data cache_record_3d[]) RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_dataarray3d_c%(cp)d 
           SET avg_counts[index] = data[i].avg, min_counts[index]=data[i].min,
               max_counts[index]=data[i].max
           WHERE time_record_id=tr_id 
                AND data_type_id=dt_id 
                AND value1=data[i].value1 
                AND value2=data[i].value2;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
            INSERT INTO dscng_dataarray3d_c%(cp)d
                (time_record_id,data_type_id,value1,value2,avg_counts,
                min_counts,max_counts) 
                VALUES
                (tr_id,dt_id,data[i].value1,
                data[i].value2,data[i].avg_empty_arr,data[i].min_empty_arr,
                data[i].max_empty_arr);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
    update_cache_data_2d_c%(cp)d(tr_id int, dt_id int, index int,
    data cache_record_2d[]) RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_dataarray2d_c%(cp)d 
           SET avg_counts[index] = data[i].avg, min_counts[index]=data[i].min,
               max_counts[index]=data[i].max
           WHERE time_record_id=tr_id 
                AND data_type_id=dt_id 
                AND value1=data[i].value1;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
            INSERT INTO dscng_dataarray2d_c%(cp)d
                (time_record_id,data_type_id,value1,avg_counts,
                min_counts,max_counts) 
                VALUES
                (tr_id,dt_id,data[i].value1,
                data[i].avg_empty_arr,data[i].min_empty_arr,
                data[i].max_empty_arr);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
    update_cache_data_1d_c%(cp)d(tr_id int, dt_id int, index int,
    data cache_record_1d[]) RETURNS VOID AS $$
DECLARE
    row_num INTEGER;
BEGIN
    FOR i IN COALESCE(array_lower(data,1),0) .. COALESCE(array_upper(data,1),-1)
    LOOP
        UPDATE dscng_dataarray1d_c%(cp)d 
           SET avg_counts[index] = data[i].avg, min_counts[index]=data[i].min,
               max_counts[index]=data[i].max
           WHERE time_record_id=tr_id AND data_type_id=dt_id;
        GET DIAGNOSTICS row_num = ROW_COUNT;
        IF row_num = 0 THEN
            INSERT INTO dscng_dataarray1d_c%(cp)d
                (time_record_id,data_type_id,avg_counts,
                min_counts,max_counts) 
                VALUES
                (tr_id,dt_id,data[i].avg_empty_arr,data[i].min_empty_arr,
                data[i].max_empty_arr);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;
"""

CACHING_POINTS = StoragePSQL.caching_points
    
# constants used in import_base_sql_file
SQL_IMPORT_NOTHING = 0
SQL_IMPORT_STRUCTURE = 1
SQL_IMPORT_CONSTRAINTS = 2
SQL_IMPORT_ALL = 3
    

def run_sql(conn, cursor, command, ignore_error=False):
    try:
        cursor.execute(command)
    except Exception as e:
        if not ignore_error:
            logging.error("Error occured when running\n%s\n%s", command, e)
    conn.commit()

def print_sql(command, **kw):
    print command


def check_other_connections(conn):
    """
    Returns a list of connections that are currently in transaction or otherwise
    occupied
    """
    my_pid = conn.get_backend_pid()
    cursor = conn.cursor()
    cursor.execute("SHOW server_version_num")
    version = int(cursor.fetchone()[0])
    if version < 90200:
        cursor.execute("""SELECT procpid
            FROM pg_stat_activity
            WHERE datname = current_database() AND query != '<IDLE>'
                  AND procpid != %s""", (my_pid,))
    else:
        cursor.execute("""SELECT pid
            FROM pg_stat_activity
            WHERE datname = current_database() AND state != 'idle'
                  AND pid != %s""", (my_pid,))
    pids = list(cursor)
    cursor.close()
    conn.commit()
    return pids
    

def drop_additional(conn, test_only=False):
    """
    Drop custom views and caching tables
    """
    cursor = conn.cursor()
    if test_only:
        sql_command = print_sql
    else:
        sql_command = lambda command, **kw: run_sql(conn, cursor, command, **kw)
    # possibly drop old things
    for caching_point in CACHING_POINTS:
        for dim in (1, 2, 3):
            view_name = "dscng_data%dd_c%d" % (dim, caching_point)
            logging.debug("Dropping view %s", view_name)
            sql_command("DROP VIEW IF EXISTS %s;" % view_name)
            table_name = "dscng_dataarray%dd_c%d" % (dim, caching_point)
            logging.debug("Dropping caching table %s", table_name)
            sql_command("DROP TABLE IF EXISTS %s CASCADE;" % table_name)
    for dim in (1, 2, 3):
        view_name = "dscng_data%dd" % dim
        logging.debug("Dropping view %s", view_name)
        sql_command("DROP VIEW IF EXISTS %s;" % view_name)
    
    # accum data views
    for suffix in "", "_ip":
        for dim in (2, 3):
            view_name = "dscng_accumdata%dd%s" % (dim, suffix)
            logging.debug("Dropping view %s", view_name)
            sql_command("DROP VIEW IF EXISTS %s;" % view_name)

    # daily stats view
    for dim in (2, 3):
        view_name = "dscng_accumskipped%dd" % dim
        logging.debug("Dropping view %s", view_name)
        sql_command("DROP VIEW IF EXISTS %s;" % view_name)
    
    conn.commit()
    # drop some more
    sql_command("""ALTER TABLE dscng_str_to_int_remap 
                DROP constraint dscng_str_to_int_remap_uniq_consts;""",
                ignore_error=True)
    sql_command("""ALTER TABLE dscng_dimension 
                DROP constraint dscng_dimension_name_uniq_consts;""",
                ignore_error=True)
    # drop custom functions
    for suffix in "", "_ip":
        for dim in (2,3):
            func = "update_accum_data_%dd%s(dscng_datapoint%dd_accum%s[])" %\
                    (dim, suffix, dim, suffix)
            sql_command("DROP FUNCTION IF EXISTS %s" % func, ignore_error=True)
    # droping type cache_record_3d with cascade
    # drops the functions that use it as well
    for dim in (1,2,3):
        sql_command("DROP TYPE cache_record_%dd CASCADE;" % dim,
                    ignore_error=True)
    conn.commit()
    cursor.close()


def setup_db(conn, base_sql_file, test_only=False):
    """
    conn is an established connection into a database;
    if test_only is True, nothing will happen, only the commands will be printed
    out for debugging purposes
    """
    other = check_other_connections(conn)
    if other:
        logging.warn("Other connections to the same database exist - "
                     "the process might get blocked.") 
    drop_additional(conn, test_only=test_only)
    # import the basic schema contained in db.sql
    try:
        import_base_sql_file(conn, base_sql_file, test_only=test_only)
    except IOError as e:
        logging.error("Could not open db definition file %s", base_sql_file)
        sys.exit()
    create_additional(conn, test_only=test_only)


def import_base_sql_file(conn, base_sql_file, part=SQL_IMPORT_ALL,
                         test_only=False):
    """
    conn is an established connection into a database;
    if test_only is True, nothing will happen, only the commands will be printed
        out for debugging purposes;
    part describes what should be imported - it is described using SQL_IMPORT_
        constants defined above;
    """
    cursor = conn.cursor()
    if test_only:
        sql_command = print_sql
    else:
        sql_command = lambda command, **kw: run_sql(conn, cursor, command, **kw)

    # import the basic schema contained in db.sql
    logging.debug("Importing basic DB schema from %s", base_sql_file)
    constraint_matcher = re.compile(".*alter table \S+ add constraint.*",
                                    re.IGNORECASE|re.DOTALL)
    with file(base_sql_file, "r") as base_sql_f:
        command_text = base_sql_f.read()
        commands = command_text.split(";")
        for command in commands:
            if command.strip():
                if constraint_matcher.match(command):
                    # this is constraint cration command
                    if part & SQL_IMPORT_CONSTRAINTS:
                        sql_command(command, ignore_error=True)
                else:
                    # this is structure defining command
                    if part & SQL_IMPORT_STRUCTURE:
                        sql_command(command, ignore_error=True)
    conn.commit()
    # insert db version number into metadata
    # we do it here to ensure this data is always available in a database
    update_db_schema_version_record(conn, DB_SCHEMA_VERSION)
    cursor.close()

def update_db_schema_version_record(conn, version):
    cursor = conn.cursor()
    # first test update
    cursor.execute("""UPDATE dscng_metadata 
                      SET value=%s WHERE section=%s AND name=%s""",
                      (version, "db_static", "schema_version"))
    if cursor.rowcount == 0:
        # we need to insert
        cursor.execute("""INSERT INTO dscng_metadata (section,name,value)
                          VALUES (%s,%s,%s)""",
                          ("db_static", "schema_version", version))
    logging.debug("DB schema version set to %d", version) 
    cursor.close()
    conn.commit()

def create_additional(conn, test_only=False):
    """
    Creates caching tables, custom views and other stuff
    """
    cursor = conn.cursor()
    if test_only:
        sql_command = print_sql
    else:
        sql_command = lambda command, **kw: run_sql(conn, cursor, command, **kw)
    # special things
    sql_command("""ALTER TABLE dscng_str_to_int_remap 
                   ADD constraint dscng_str_to_int_remap_uniq_consts
                   UNIQUE(dimension_id, remap);""")
    sql_command("""ALTER TABLE dscng_dimension 
                   ADD constraint dscng_dimension_name_uniq_consts
                   UNIQUE(name);""")
    sql_command("""CREATE TYPE cache_record_1d AS 
                   (avg int, min int, max int,
                   avg_empty_arr int[], min_empty_arr int[],
                   max_empty_arr int[]);""")
    sql_command("""CREATE TYPE cache_record_2d AS 
                   (value1 int, avg int, min int, max int,
                   avg_empty_arr int[], min_empty_arr int[],
                   max_empty_arr int[]);""")
    sql_command("""CREATE TYPE cache_record_3d AS 
                   (value1 int, value2 int, avg int, min int, max int,
                   avg_empty_arr int[], min_empty_arr int[],
                   max_empty_arr int[]);""")
    # stored procedures
    logging.debug("Creating stored procedures")
    cursor.execute("SELECT * FROM pg_language WHERE lanname='plpgsql';")
    if cursor.rowcount == 0:
        sql_command("CREATE LANGUAGE plpgsql;")
    sql_command(STORED_PROCEDURES)
    conn.commit()
    
    # create individual caching tables
    for caching_point in CACHING_POINTS:
        for dim in (1,2,3):
            table_name = "dscng_dataarray%dd_c%d" % (dim, caching_point)
            copy_from = "dscng_dataarray%dd_c" % dim
            logging.debug("Creating caching table %s", table_name)
            # create table
            sql_command(COPY_TABLE_SQL % (table_name, copy_from))
            # create foreign key constraints
            for name_base, fk_command in FK_CONSTRAINT_SQLS:
                fk_name = "%s_fk_%s" % (table_name, name_base)
                sql_command(fk_command % (table_name, fk_name))
            # create view
            view_name = "dscng_data%dd_c%d" % (dim, caching_point)
            logging.debug("Creating view %s", view_name)
            sql_command(CREATE_VIEW_SQLS[dim] % {'table_name': table_name,
                                                 'view_name': view_name})
        # update procedure
        for dim in (3,):
            sql_command(CACHE_UPDATE_STORED_PROCEDURES % {"cp": caching_point})
    conn.commit()
    
    # create views for uncached data
    for dim in (1,2,3):
        table_name = "dscng_dataarray%dd" % dim
        view_name = "dscng_data%dd" % dim
        logging.debug("Creating view %s", view_name)
        sql_command(CREATE_UNCACHED_VIEW_SQLS[dim] % {'table_name': table_name,
                                                      'view_name': view_name})
    # accum data views
    for suffix in ("", "_ip"):
        if suffix == "_ip":
            command_source = CREATE_ACCUM_IP_VIEW_SQLS
        else:
            command_source = CREATE_ACCUM_VIEW_SQLS
        for dim in (2,3):
            table_name = "dscng_datapoint%dd_accum%s" % (dim, suffix)
            view_name = "dscng_accumdata%dd%s" % (dim, suffix)
            logging.debug("Creating view %s", view_name)
            sql_command(command_source[dim] % {'table_name': table_name,
                                               'view_name': view_name})
    # daily skipped data
    for dim in (2,3):
        table_name = "dscng_dailystats%dd" % dim
        view_name = "dscng_accumskipped%dd" % dim
        logging.debug("Creating view %s", view_name)
        sql_command(CREATE_ACCUMSKIPPED_VIEW_SQLS[dim] % \
                    {'table_name': table_name,
                     'view_name': view_name})
    conn.commit()
    cursor.close()

        
