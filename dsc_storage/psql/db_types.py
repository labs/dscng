# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Here reside data types that are used in different places in dsc_storage
and they do not fit in one of the modules.
"""

class DBObject(object):
    """Parent of all objects retrieved from a database"""
    pass


class Server(DBObject):
    """Represents a server"""
    def __init__(self, db_id, group_id=None, name=None):
        super(Server, self).__init__()
        self.id = db_id
        self.group_id = group_id
        self.name = name


class ServerGroup(DBObject):
    """Represents a server"""
    def __init__(self, db_id, parent_id=None, name=None):
        super(ServerGroup, self).__init__()
        self.id = db_id
        self.parent_id = parent_id
        self.name = name


class DataType(DBObject):
    """Represents a data type"""
    def __init__(self, db_id, name=None, accumulated=None, parent_id=None,
                 local_name=None):
        super(DataType, self).__init__()
        self.id = db_id
        self.name = name
        self.accumulated = accumulated
        self.parent_id = parent_id
        self.local_name = local_name or name


class Dimension(DBObject):
    """Represents a dimension"""
    def __init__(self, db_id, name=None, dim_type=None, meta=None):
        super(Dimension, self).__init__()
        self.id = db_id
        self.name = name
        self.type = dim_type 
        self.meta = meta
