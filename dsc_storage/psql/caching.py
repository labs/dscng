# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains code related to caching of data obtained from the database
"""

import memcache


class Cache(object):
    """represents a key-value based cache backed by memcached"""

    # default timeout in seconds
    default_timeout = 3600
    # memcached servers - default installation is at localhost:11211
    servers = ['127.0.0.1:11211']
    
    def __init__(self):
        self._cache = memcache.Client(Cache.servers)

    def set(self, key, value, timeout=None):
        """put a value into the cache based on key"""
        timeout = timeout or Cache.default_timeout
        self._cache.set(self._make_key(key), value, timeout)

    def get(self, key):
        """get value from cache"""
        return self._cache.get(self._make_key(key))

    def _make_key(self, key):
        """
        Function for converting iterable-type keys to string that are accepted
        as memcache's keys.
        """
        if isinstance(key, str):
            return key
        if isinstance(key, (list, tuple)):
            return ':'.join(map(str, key)).strip().replace(" ", "_")

        raise ValueError("Cache key must be str, list or tuple.")
