# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module implements stuff necessary to provide testing:

creation of test databases, their destroying and other test stuff
"""

# third party module for PostgreSQL communication
try:
    import psycopg2
except ImportError:
    try:
        # try psycopg2ct (c-types version of psycopg)
        import psycopg2ct as psycopg2
    except ImportError:
        raise ImportError("No psycopg2 implementation found")

from dsc_storage.psql.connection import ConnectionManager


class TestDBManager(object):
    
    ERROR_CODE_DB_EXISTS = "42P04"
    
    def __init__(self, username, password, host=None, port=None):
        """
        username and password should be of a user entitled to create new
        PostgreSQL databases.
        """
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.conn = None

    
    def get_connection_params(self, **kw):
        params = dict(user=self.username, password=self.password)
        if self.host:
            params['host'] = self.host
        if self.port:
            params['port'] = self.port
        params.update(kw)
        return params
        
    
    def _create_connection(self):
        params = self.get_connection_params()
        self.conn = ConnectionManager(ignore_version_error=True, **params)
        # without the following commands like create database cannot be run
        self.conn.set_isolation_level(0)
        
    
    def create_test_db(self, dbname=None):
        """
        dbname is optional and could be used to create a database with a
        specific name.
        """
        if not self.conn:
            self._create_connection()
        dbname = dbname or 'test_dsc'
        suffix = ""
        suffix_int = 0
        done = False
        cursor = self.conn.cursor()
        while not done:
            full_dbname = dbname + suffix
            try:
                cursor.execute("CREATE DATABASE %s" % full_dbname)
            except psycopg2.ProgrammingError as e:
                if e.pgcode == self.ERROR_CODE_DB_EXISTS:
                    suffix_int += 1
                    suffix = str(suffix_int)
                else:
                    raise e
            else:
                done = True
        cursor.close()
        # we got here - everything went well
        # we can now connect to the database itself
        params = self.get_connection_params(database=full_dbname)
        conn = ConnectionManager(ignore_version_error=True, **params)
        tdb = TestDB(name=full_dbname, conn=conn)
        return tdb
    
    
    def connect_to_test_db(self, dbname):
        """
        This is used when there is a pool of test databases created ahead of
        time.
        """
        params = self.get_connection_params(database=dbname)
        conn = ConnectionManager(**params)
        tdb = TestDB(name=dbname, conn=conn)
        return tdb


    def destroy_test_db(self, tdb):
        if not tdb.name:
            raise ValueError(
                "There is no database associated with this TestDB instance")
        tdb.close()
        cursor = self.conn.cursor()
        cursor.execute("DROP DATABASE %s" % tdb.name)
        cursor.close()

   
    def close(self):
        """
        Closes the connection to the database.
        """
        if self.conn:
            self.conn.close()
            self.conn = None
           
   

class TestDB(object):
    
    def __init__(self, name, conn):
        self.name = name
        self.conn = conn

    def set_up_tables(self):
        pass
        
    def close(self):
        if self.conn:
            self.conn.close()
        self.conn = None
             
    
