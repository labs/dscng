# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains functions related to keeping a database healthy
"""

import logging
from datetime import timedelta
from storage import StoragePSQL

def sync_sequences(conn):
    """
    Take a look at the sequence counters and corresponding values and update
    the sequences when necessary.
    """
    cursor = conn.cursor()
    # time record id sequences
    for suffix in ("","_c","_accum"):
        table = "dscng_timerecord{0}".format(suffix)
        sequence = "dscng_timerecord{0}_id_seq".format(suffix)
        cursor.execute("SELECT nextval ('{0}');".format(sequence))
        seq_value = cursor.fetchone()[0]
        cursor.execute("SELECT MAX(id) FROM {0};".format(table))
        id_value = cursor.fetchone()[0]
        if id_value >= seq_value:
            logging.debug("Sequence %s is out of sync: stored %d, found %d. "
                          "Fixing", sequence, seq_value, id_value)
            cursor.execute("SELECT setval('{0}', %s);".format(sequence),
                           (id_value,))
    conn.commit()
    cursor.close()
    
def sync_timespans(conn):
    """
    Checks if server timespans are in sync and updates them if necessary.
    """
    storage = StoragePSQL(conn)
    cursor = conn.cursor()
    for server in storage.get_servers():
        logging.debug("Syncing timestamp for server: %s", server.name)
        # get the stored timespans
        cursor.execute("""SELECT start_time,end_time FROM dscng_servertimespan
                          WHERE server_id=%s""", (server.id,))
        rec = cursor.fetchone()
        if rec is not None:
            db_min_t, db_max_t = rec
        else:
            db_min_t, db_max_t = None, None
        # fetch computed values from dscng_timerecord
        cursor.execute("""SELECT MIN(time),MAX(time) FROM dscng_timerecord
                          WHERE server_id=%s""", (server.id,))
        comp_min_t, comp_max_t = cursor.fetchone()
        # deal with the minimum value
        if comp_min_t is not None and comp_min_t != db_min_t:
            # the minimal value differs - we need to obtain more precise value
            # and then make and update
            logging.debug("Min time differs in screening: %s vs %s",
                          db_min_t, comp_min_t)
            min_minute = 60
            for dim in (1,2,3):
                # get minimum minute from tables with increasing complexity
                cursor.execute("""SELECT MIN(minute) FROM dscng_data{0}d 
                                   WHERE server_id=%s AND time=%s 
                                       AND count != -1;""".format(dim),
                                (server.id, comp_min_t))
                cur_minute = cursor.fetchone()[0]
                if cur_minute is not None and cur_minute < min_minute:
                    min_minute = cur_minute
                if min_minute == 0:
                    # we cannot get lower
                    break
            # update comp_min_t to exact value
            comp_min_t += timedelta(minutes=min_minute)
        # now deal with the maximum value
        if comp_max_t is not None and comp_max_t != db_max_t:
            # the maximum value differs - we need to obtain more precise value
            # and then make and update
            logging.debug("Max time differs in screening: %s vs %s",
                          db_max_t, comp_max_t)
            max_minute = 0
            for dim in (1,2,3):
                # get minimum minute from tables with increasing complexity
                cursor.execute("""SELECT MAX(minute) FROM dscng_data{0}d 
                                   WHERE server_id=%s AND time=%s 
                                       AND count != -1;""".format(dim),
                                (server.id, comp_min_t))
                cur_minute = cursor.fetchone()[0]
                if cur_minute is not None and cur_minute > max_minute:
                    max_minute = cur_minute
                if max_minute == 59:
                    # we cannot get higher
                    break
            # update comp_min_t to exact value
            comp_max_t += timedelta(minutes=max_minute)
        # now have look at the exact values and make an update if needed
        if comp_min_t != db_min_t or comp_max_t != db_max_t:
            logging.debug("Syncing timespan because of a found difference.")
            storage.update_server_timespan(server.id, comp_min_t, comp_max_t)
                    
        
def fix_db_health(conn):
    """
    This function runs all the maintenance functions in this module on the 
    connection at hand
    """  
    sync_sequences(conn)
    sync_timespans(conn)