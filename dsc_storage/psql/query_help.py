# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import datetime
import math
import time


# The following implementation of FixedOffset and UTC is taken directly
# from the Python documentation for the datetime module, just with slight
# modifications

ZERO = datetime.timedelta(0)
HOUR = datetime.timedelta(hours=1)

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO

class FixedOffset(datetime.tzinfo):
    """Fixed offset in minutes east from UTC."""

    def __init__(self, offset):
        self.__offset = datetime.timedelta(minutes=offset)
        self.__name = "%d:%02d" % divmod(offset, 60)

    def utcoffset(self, dt):
        return self.__offset

    def tzname(self, dt):
        return self.__name

    def dst(self, date_time):
        if self._isdst(date_time):
            return HOUR
        else:
            return ZERO

    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day,
              dt.hour, dt.minute, dt.second,
              dt.weekday(), 0, -1)
        stamp = time.mktime(tt)
        tt = time.localtime(stamp)
        return tt.tm_isdst > 0

# // END of code from Python documentation

def construct_time_where_clause(from_time=None, to_time=None):
    parts = []
    if from_time:
        parts.append("time AT TIME ZONE 'UTC' >= '%s'" %\
                     from_time)
    if to_time:
        parts.append("time AT TIME ZONE 'UTC' <= '%s'" %\
                     to_time)
    return parts


def compute_time_query_params(start, end, include_sampling=True,
                              limit_start=None, limit_end=None,
                              tz_offset=0):
    """limit_start and limit_end must be passed as datetime type (result of
     StoragePSQL.get_available_timespan())
    """
    if type(start) in (float, int):
        start = datetime.datetime.fromtimestamp(start, FixedOffset(tz_offset))
    if type(end) in (float, int):
        end = datetime.datetime.fromtimestamp(end, FixedOffset(tz_offset))
    if limit_start and limit_start > start:
        start = limit_start
    if limit_end and limit_end < end:
        end = limit_end
    return dict(from_time=start, to_time=end)


def construct_query(base, conditions):
    if conditions:
        where_clause = " WHERE " + " AND ".join(conditions)
    else:
        where_clause = ""
    return base % locals()


def get_best_timespan(span, caching_points):
    if span <= 0:
        return 1
    min_idx = None
    best_cp = None
    for cp in (1,) + caching_points:
        idx = abs(math.log(1.0*span/cp))
        if min_idx is None or idx < min_idx:
            min_idx = idx
            best_cp = cp
    return best_cp

def join_and_prepend(joiner, values):
    res = ""
    if values:
        res += joiner
    res += joiner.join(values)
    return res

def round_time_to_timespan(timepoint, timespan, upper=True):
    """
    Takes time and returns the nearest time that is rounded to timespan.
    If upper is True, than it returns the lowest larger value, otherwise
    the largest lower value.
    """
    # some timezone related stuff
    orig_tzinfo = timepoint.tzinfo
    utc = UTC()
    start = timepoint.replace(second=0, microsecond=0)
    if orig_tzinfo:
        start = start.astimezone(utc)
    # decide where to start from while searching for the right value
    if timespan > 1:
        start = start.replace(minute=0)
    if timespan > 60:
        start = start.replace(hour=0)
    # add delta to cur_time until we reach the right point
    delta = datetime.timedelta(minutes=timespan)
    cur_time = start
    while cur_time < timepoint:
        cur_time += delta
    # for perfect matches, the upper attr has no influence
    if cur_time == timepoint:
        result = cur_time
    # depending on the upper setting, return this or previous point
    elif upper:
        result = cur_time
    else:
        result = cur_time - delta
    if orig_tzinfo:
        result = result.astimezone(orig_tzinfo)
    return result

    
def split_time_for_query(timepoint, timespan):
    """
    >>> d1 = datetime.datetime(2011,10,20,0,0)
    >>> split_time_for_query(d1, 1)
    (datetime.datetime(2011, 10, 20, 0, 0), 0)
    >>> split_time_for_query(d1, 360)
    (datetime.datetime(2011, 10, 20, 0, 0), 0)
    >>> split_time_for_query(d1, 1440)
    (datetime.datetime(2011, 10, 20, 0, 0), 0)
    """
    if timespan == 1:
        start_time = round_time_to_timespan(timepoint, 60, upper=False)
    else:
        start_time = round_time_to_timespan(timepoint, 360, upper=False)
    delta = timepoint - start_time
    minute = delta.seconds // 60 // timespan
    return start_time, minute
