# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module implements the storage API for PostgreSQL database
"""

# std lib imports
from datetime import timedelta
from django.conf import settings
from time import mktime, time
import logging
logger = logging.getLogger("dsc_storage")
logger.setLevel(level=logging.DEBUG if settings.DEBUG else logging.WARN)
import copy

# 3rd party imports
# third party module for PostgreSQL communication
try:
    import psycopg2
except ImportError:
    try:
        # try psycopg2ct (c-types version of psycopg)
        import psycopg2ct as psycopg2
    except ImportError:
        raise ImportError("No psycopg2 implementation found")

# local imports
import query_help
from connection import ConnectionManager
from data_types import DataTypeManager
from db_types import Server, Dimension, ServerGroup
from caching import Cache



class StoragePSQL(object):
    """
    Represents an interface to the PostgreSQL based storage
    """
    
    # how many minutes are aggregated in cache tables
    caching_points = (5, 20, 90, 360, 1440)


    def __init__(self, connection_attrs):
        """
        Initializes the storage using connection_attrs to create
        connection to a database
        """
        if isinstance(connection_attrs, ConnectionManager):
            self.conn = connection_attrs
        else:
            self.conn = ConnectionManager(**connection_attrs)
        self._remaps = {}
        self.servers = {}
        self.dtm = DataTypeManager(self.conn)
        self._cache = Cache()

    def get_servers(self):
        """
        returns Server instances for all known servers
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id,name,group_id FROM dscng_server;")
        result = []
        for server_id, name, group_id in cursor:
            result.append(Server(server_id, group_id=group_id, name=name))
        cursor.close()
        self.conn.commit()
        return result
    
    def get_server_groups(self):
        """
        returns ServerGroup instances for all known servers
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id,name,parent_id FROM dscng_server_group;")
        result = []
        for group_id, name, parent_id in cursor:
            result.append(ServerGroup(group_id, parent_id=parent_id, name=name))
        cursor.close()
        self.conn.commit()
        return result
    
    def get_server_by(self, server_id=None, name=None):
        """
        return a Server instance either by its id or its name
        """
        cursor = self.conn.cursor()
        template = "SELECT id,group_id,name FROM dscng_server WHERE %s=%%s"
        if server_id is not None:
            select = template % "id"
            value = server_id
        else:
            select = template % "name"
            value = name
        cursor.execute(select, (value,))
        data = cursor.fetchone()
        cursor.close()
        self.conn.commit()
        if data is None:
            return None
        server_id, group_id, name = data 
        return Server(server_id, group_id=group_id, name=name)
    
    def get_available_timespan(self, server_id=None, exact_max=True):
        """
        Returns minimal and maximal value for time that is available
        in data array tables;
        When server_id is given, the timespan is returned only for the given
        server_id.
        """
        _t = time()
        cursor = self.conn.cursor()
        if server_id:
            cursor.execute("""SELECT start_time AT TIME ZONE 'UTC',
                                     end_time AT TIME ZONE 'UTC' 
                              FROM dscng_servertimespan
                              WHERE server_id = %s""", (server_id,))
        else:
            cursor.execute("""SELECT MIN(start_time AT TIME ZONE 'UTC'),
                                     MAX(end_time AT TIME ZONE 'UTC')
                              FROM dscng_servertimespan""")
        min_time, max_time = cursor.fetchone()
        cursor.close()
        self.conn.commit()
        logger.debug("Getting available timespan: %f", time()-_t)
        return min_time, max_time
    

    def get_db_to_user_value_remap(self, dimension_id):
        """
        returns a mapping between database stored integer value
        and a string suitable for user display
        """
        # see if we have it in cache
        if dimension_id in self._remaps:
            return self._remaps[dimension_id]
        # we don't - fetch it
        cursor = self.conn.cursor()
        remap = {}
        cursor.execute("""SELECT original,remap 
            FROM dscng_int_to_str_remap
            WHERE dimension_id = %s""", (dimension_id,))
        for orig, remapped in cursor:
            remap[orig] = remapped
        if not remap:
            # remap from original data
            cursor.execute("""SELECT original,remap 
                FROM dscng_str_to_int_remap
                WHERE dimension_id = %s""", (dimension_id,))
            for orig, remapped in cursor:
                remap[remapped] = orig
        if not remap:
            remap = {-1: "Other", -2: "None"} # default remap
        self._remaps[dimension_id] = remap
        cursor.close()
        self.conn.commit()
        return remap
        
    
    def get_server_remap(self):
        """
        returns a mapping between server id and server name
        """ 
        cursor = self.conn.cursor()
        remap = {}
        cursor.execute("SELECT id,name FROM dscng_server")
        for server_id, name in cursor:
            remap[server_id] = name
        cursor.close()
        self.conn.commit()
        return remap
    
    def get_server_group_remap(self):
        """
        returns a mapping between server_group id and server_group name
        """ 
        cursor = self.conn.cursor()
        remap = {}
        cursor.execute("SELECT id,name FROM dscng_group_server")
        for server_id, name in cursor:
            remap[server_id] = name
        cursor.close()
        self.conn.commit()
        return remap

        
    def get_distinct_values(self, data_type, dim_indexes=None, remap=True):
        """
        returns a list of distinct values for given data_type
        and dimensions specified by its indexes;
        indexes is a list with one or two numbers for 3D data.
        >>> x.get_distinct_values(dt, dim_indexes=[0])
        returns distinct values for value1
        >>> x.get_distinct_values(dt, dim_indexes=[1])
        returns distinct values for value2
        >>> x.get_distinct_values(dt, dim_indexes=[0,1])
        returns distinct values for (value1,value2) that is all combinations
        of distinct values that occur
        """
        if dim_indexes is None:
            dim_indexes = []
        cache_key = ("distinct_values", data_type.id,
                     "_".join(map(str, dim_indexes)))
        result = self._cache.get(cache_key)
        if result:
            return result
        # no cache hit
        dims = self.dtm.get_data_type_dimensions(data_type)
        dim_count = len(dims)
        for dim_index in dim_indexes:
            if dim_count <= dim_index:
                raise ValueError(
                        "There are %d dimensions, no dimension #%d" % \
                        dim_count, dim_index)
        # decide on remap
        if remap and len(dim_indexes) == 1:
            value_remap = self.get_db_to_user_value_remap(dims[dim_indexes[0]].id)
        else:
            value_remap = {}
        cursor = self.conn.cursor()
        result = []
        table_name = "dscng_dataarray%dd_c1440" % (dim_count+1)
        col_names = ["value%d" % (dim_index+1) for dim_index in dim_indexes]
        select = "SELECT DISTINCT(%s) FROM %s WHERE data_type_id = %%s" % \
                (",".join(col_names), table_name)
        cursor.execute(select, (data_type.id,))
        for value in cursor:
            result.append((value[0], value_remap.get(value[0], str(value[0]))))
        cursor.close()
        self.conn.commit()
        self._cache.set(cache_key, result)
        return result

    
    def get_minute_data_points(self, start_time, end_time, data_type,
                        servers=None, fixed_values=None, split_by_server=True,
                        number_of_points=500, count_per_second=True,
                        zeros_for_blanks=True, ignored_dimension=None,
                        value_subsets=None):
        """
        return data points defined by:
        
        - start_time .. start_time time
        - end_time .. end_time time
        - data_type .. type of data to return
        - servers .. optional - all servers will be used if not given
        - fixed_values .. a list specifying values that should be fixed for 
                          multidimensional data, thus defining a subset
        - split_by_server .. if the arguments specify 1D data (either 1D
                             data_type or 1D subset of multiD), split the
                             data to server series or aggregate it
        - number_of_points .. optional argument determining aggregation of data
                              if None is given, unaggregated data will be used
        - count_per_second .. should the count be divided by 60 to give data
                              per second
        - zeros_for_blanks .. insert zero values for series that are missing
                              in some time intervals
        - ignored_dimension .. the result will have 1 dimension less and all
                              values with different ignored_dimension will be
                              joined
        - value_subsets .. a list of values for each dimension. If values for a
                            dim are not None, then the values will be filtered
                            according to the list
        
        return value is an instance of MinuteDataQueryResults
        """
        # if we detect a virtual data type, we adjust the parameters and call
        # ourselves again
        if data_type.parent_id is not None:
            # we have a virtual data type
            dims = self.dtm.get_data_type_dimensions(data_type)
            dim_names = [dim.name for dim in dims]
            if "_server_name" in dim_names:
                split_by_server = True
                if fixed_values: 
                    # we cannot use fixed values on server_name because
                    # it does not make sense
                    idx = dim_names.index("_server_name")
                    fixed_server = self.get_server_by(name=fixed_values[idx])
                    if not fixed_server:
                        raise ValueError("Can't find server %s" % fixed_values[idx])
                    servers = (fixed_server.id,)
                    fixed_values[idx] = None
            parent_dt = self.dtm.get_data_type(data_type_id=data_type.parent_id)
            parent_dims = self.dtm.get_data_type_dimensions(parent_dt)
            ignored_dimension = None
            dim_ids = [dim.id for dim in dims]
            value_subsets = []
            for dim in parent_dims:
                if dim.id not in dim_ids:
                    ignored_dimension = dim
                    value_subsets.append(None)
                else:
                    values = self.dtm.get_valid_dimension_values(data_type, dim)
                    value_subsets.append(values)
            result = self.get_minute_data_points(start_time, end_time,
                                    parent_dt,
                                    servers=servers, fixed_values=fixed_values,
                                    split_by_server=split_by_server,
                                    number_of_points=number_of_points,
                                    count_per_second=count_per_second,
                                    zeros_for_blanks=zeros_for_blanks,
                                    ignored_dimension=ignored_dimension,
                                    value_subsets=value_subsets)
            # make necessary changes to the underlying data
            result.data_type = data_type
            return result
        # this is for normal data types
        _tm = time()
        cursor = self.conn.cursor()
        if servers:
            server_conds = [("server_id IN %s" % (servers,)).replace(",)",")")]
        else:
            server_conds = []
        # find dimension types
        dims = self.dtm.get_data_type_dimensions(data_type)
        dim_count = len(dims)
        # prepare remaps for dimensions
        dim_remaps = []
        ignored_dimension_idx = None
        for i, dim in enumerate(dims):
            if ignored_dimension and dim.id == ignored_dimension.id:
                ignored_dimension_idx = i
            else:
                dim_remaps.append(self.get_db_to_user_value_remap(dim.id))
        # check for fixed values
        effective_dim_count = dim_count
        fixed_value_conds = []
        if fixed_values:
            for i, value in enumerate(fixed_values):
                if value is not None:
                    # we use reverse remap if the fixed value is not integer
                    if type(value) != int:
                        for k, v in dim_remaps[i].iteritems():
                            if v == value:
                                value = k
                                break
                    fixed_value_conds.append("value%d = %d" % (i+1, value))
                    effective_dim_count -= 1
        # value_subsets
        if value_subsets:
            for i, subset in enumerate(value_subsets):
                if subset is not None:
                    adapt = lambda x: str(self.dtm.remap_value_to_int(x, None))
                    sub_str = ",".join([adapt(sub_v) for sub_v in subset
                                        if sub_v is not None])
                    if None not in subset:
                        # this is normal subset - inclusive
                        fixed_value_conds.append(
                                        "value%d IN (%s)" % (i+1, sub_str))
                    else:
                        # this is exclusive subset 
                        # - what is listed, we do not want
                        fixed_value_conds.append(
                                        "value%d NOT IN (%s)" % (i+1, sub_str))
        # ignored_dimension
        if ignored_dimension:
            effective_dim_count -= 1
        # should we split series by server?
        series_by_server = (effective_dim_count == 0) and split_by_server
        # look for data remaps from database integers to strings
        server_remap = {}
        if series_by_server:
            server_remap = self.get_server_remap()
        
        time_delta = mktime(end_time.timetuple()) - \
                     mktime(start_time.timetuple())
        if number_of_points:
            timespan = time_delta / number_of_points / 60
        else:
            timespan = 1
        timespan = query_help.get_best_timespan(timespan, self.caching_points)
    
        q_start_time = start_time
        if timespan == 1:
            q_start_time -= timedelta(hours=1)
        else:
            q_start_time -= timedelta(hours=6)
        conds = query_help.construct_time_where_clause(from_time=q_start_time,
                                                       to_time=end_time)
        conds += server_conds
        conds += ["data_type_id = %s" % data_type.id]
        conds += fixed_value_conds
        # if we want to fill in zeroes for blanks, we need to have a list
        # of all possible series, we accomplish this by pre-filling types
        # with data taken from distinct values of this data type and free
        # dimension (without a fixed value)
        if not zeros_for_blanks:
            types = set()
        else:
            if series_by_server:
                # when splitting by server, we want just active servers to
                # define series
                if servers:
                    types = set([val for (key,val) in server_remap.items()
                                 if key in servers])
                else:
                    types = set(server_remap.values())
            elif dim_count == 1:
                if value_subsets and value_subsets[0]:
                    value_remap = self.get_db_to_user_value_remap(dims[0].id)
                    types = set([value_remap.get(typ, str(typ)) 
                                 for typ in value_subsets[0]])
                else:
                    distinct = self.get_distinct_values(data_type, [0],
                                                        remap=True)
                    types = set([tup[1] for tup in distinct])
            elif dim_count == 2 and fixed_values:
                if len(fixed_values) == 2 and fixed_values[0] is not None:
                    # fixed dimension must be the second one
                    free_dim = 0
                else:
                    free_dim = 1
                distinct = self.get_distinct_values(data_type, [free_dim],
                                                    remap=True)
                types = set([tup[1] for tup in distinct])
            elif dim_count == 2 and ignored_dimension:
                free_dim = 0 if ignored_dimension_idx == 1 else 1
                distinct = self.get_distinct_values(data_type, [free_dim],
                                                    remap=True)
                types = set([tup[1] for tup in distinct])
            else:
                raise NotImplementedError()
        # divider chooses between packets/s or packets/min values
        divider = 1
        if count_per_second:
            divider = 60.0
        # Because the structure of the returned tuples is always the same,
        # we can save some processing time by pre-meditating which part of the
        # result is what.
        # Here we do so:
        name_cols = []  # always a tuple of index and remap
        value_cols = []  # just indexes
        value_start_idx = 2
        if series_by_server:
            name_cols.append((value_start_idx, server_remap))
            value_start_idx += 1
        for i in range(0, dim_count):
            # do not include fixed values
            if fixed_values and i < len(fixed_values) and \
                fixed_values[i] is not None:
                value_start_idx += 1
                continue 
            # ignored dimension is similar to fixed value - we skip it
            if ignored_dimension_idx == i:
                #value_start_idx += 1
                continue
            name_cols.append((value_start_idx, dim_remaps[i]))
            value_start_idx += 1
        if timespan == 1:
            value_cols.append(value_start_idx)
        else:
            # there are three columns with values
            value_cols += range(value_start_idx, value_start_idx+3)
        # construct and run the query
        select = self._create_minute_select(dim_count, timespan,
                                    series_by_server,
                                    ignored_dimension_idx=ignored_dimension_idx)
        query = query_help.construct_query(select, conds)
        logger.debug("Query preparation: %s", time() - _tm)
        _tm = time() 
        logger.debug(query)
        cursor.execute(query)
        logger.debug("Query execution: %s", time() - _tm)
        _tm = time()
        # here we start to process the data rows
        raw_data = {}
        for row in cursor:
            # the time - compute and check it
            stm = row[0]
            mnt = row[1]
            rec_tm = stm + timedelta(minutes=mnt*timespan)
            # because in the database minute data is stored separately
            # we need to check if the time is really inside bounds
            if rec_tm < start_time or rec_tm > end_time:
                continue
            if rec_tm not in raw_data:
                if zeros_for_blanks:
                    raw_data[rec_tm] = dict.fromkeys(types, 0)
                else:
                    raw_data[rec_tm] = {}
            # get the series name
            series_name = "" 
            for idx, remap in name_cols:
                series_name += remap.get(row[idx], str(row[idx]))
            # get the value (we round it because the display cannot deal with
            # floats with more than one decimal point - it gets stuck eating
            # huge amounts of memory - don't ask me why, I have no idea
            if len(value_cols) == 1:
                avgx = round(row[value_cols[0]]/divider, 1)
                raw_data[rec_tm][series_name] = avgx
            else:
                avgx = round(row[value_cols[0]]/divider, 1)
                maxx = round(row[value_cols[1]]/divider, 1)
                minx = round(row[value_cols[2]]/divider, 1)
                raw_data[rec_tm][series_name] = avgx
                raw_data[rec_tm][series_name+"__min"] = minx
                raw_data[rec_tm][series_name+"__max"] = maxx
            types.add(series_name)
        result = MinuteDataQueryResults(data_type, start_time, end_time,
                                        timespan, raw_data, types)
        logger.debug("Result processing: %s", time() - _tm)
        cursor.close()
        self.conn.commit()
        return result


    def get_details_for_time(self, timepoint, timespan, dimensions=(1,2,3),
                             count_per_second=True, servers=None):
        """
        Returns values for all data types with dimension count included in
        the dimensions argument.
        """
        if timespan != 1 and timespan not in self.caching_points:
            raise ValueError("%s is not an allowed timespan value." % timespan)
        result = []
        divider = 1
        if count_per_second:
            divider = 60.0
        cursor = self.conn.cursor()
        # find the time for which to search - split it to base and additional
        # parts to reflect the organization of the database
        start_time, minute = query_help.split_time_for_query(timepoint,
                                                             timespan)
        # find the data for all selected dimensions
        for dim_count in dimensions:
            table_name = "dscng_data%dd" % dim_count
            if timespan != 1:
                table_name += "_c%d" % timespan
            columns = "data_type_id"
            count_col = "count"
            if timespan > 1:
                count_col = "avg_count"
            for i in range(1, dim_count):
                columns += ",value%d" % i
            where_conds = []
            if servers:
                where_conds += [("server_id IN %s" % (servers,)).replace(",)",")")]
            where_conds += ["time = %s AT TIME ZONE 'UTC'"]
            where_conds += ["minute = %s"]
            where_conds += ["%s != -1" % count_col]

            select = """SELECT %(columns)s, SUM(%(count_col)s) FROM %(table_name)s
            %%(where_clause)s
            GROUP BY %(columns)s ORDER BY %(columns)s;""" % dict(
                count_col=count_col, table_name=table_name, columns=columns)
            query = query_help.construct_query(select, where_conds)
            cursor.execute(query, (start_time, minute))
            for row in cursor:
                data_type_id = row[0]
                data_type = self.dtm.get_data_type(data_type_id=data_type_id)
                new_row = [data_type.name]
                if dim_count > 1:
                    dims = self.dtm.get_data_type_dimensions(data_type)
                    for i, dim in enumerate(dims):
                        remap = self.get_db_to_user_value_remap(dim.id)
                        new_row.append(remap.get(row[1+i], str(row[1+i])))
                new_row.append(round(row[-1]/divider, 1))
                if new_row[-1] != 0:
                    result.append(new_row)
        cursor.close()
        self.conn.commit()
        return result
    
    def get_server_traffic(self, timepoint):
        """rep_time is time at which to report"""
        start_time, minute = query_help.split_time_for_query(timepoint, 1)
        data_type = self.dtm.get_data_type(name="idn_qname")
        select = """SELECT server_id,SUM(count)
            FROM dscng_data2d 
            WHERE time = %s AT TIME ZONE 'UTC' AND minute = %s AND count != -1
            AND data_type_id = %s
            GROUP BY server_id;"""
        cursor = self.conn.cursor()
        logger.debug("SELECT: %s", cursor.mogrify(select, 
                                        (start_time, minute, data_type.id)))
        cursor.execute(select, (start_time, minute, data_type.id))
        data = list(cursor)
        cursor.close()
        self.conn.commit()
        return data
        
    def get_overall_traffic(self, timepoint):
        """rep_time is time at which to report"""
        start_time, minute = query_help.split_time_for_query(timepoint, 1)
        data_type = self.dtm.get_data_type(name="idn_qname")
        select = """SELECT SUM(count)
            FROM dscng_data2d 
            WHERE time = %s AT TIME ZONE 'UTC' AND minute = %s AND count != -1
            AND data_type_id = %s;"""
        cursor = self.conn.cursor()
        cursor.execute(select, (start_time, minute, data_type.id))
        data = cursor.fetchone()[0]
        cursor.close()
        self.conn.commit()
        return data
        
        
    def get_daily_data(self, start_time, end_time, data_type, servers=None,
                       value_subsets=None):
        """
        return data points defined by:
        
        - start_time .. start_time time
        - end_time .. end_time time
        - data_type .. type of data to return
        - servers .. optional - all servers will be used if not given        
        """
        # if we detect a virtual data type, we adjust the parameters and call
        # ourselves again
        if data_type.parent_id is not None:
            # we have a virtual data type
            dims = self.dtm.get_data_type_dimensions(data_type)
            parent_dt = self.dtm.get_data_type(data_type_id=data_type.parent_id)
            parent_dims = self.dtm.get_data_type_dimensions(parent_dt)
            dim_ids = [dim.id for dim in dims]
            value_subsets = []
            for dim in parent_dims:
                if dim.id not in dim_ids:
                    value_subsets.append(None)
                else:
                    values = self.dtm.get_valid_dimension_values(data_type, dim)
                    value_subsets.append(values)
            result = self.get_daily_data(start_time, end_time,
                                         parent_dt,
                                         #ignored_dimension=ignored_dimension,
                                         value_subsets=value_subsets)
            result.data_type = data_type
            return result
        # this is normal course of action when the data type is non-virtual
        _tm = time()
        cursor = self.conn.cursor()
        if servers:
            server_conds = [("server_id IN %s" % (servers,)).replace(",)",")")]
        else:
            server_conds = []
        # find dimension types
        dims = self.dtm.get_data_type_dimensions(data_type)
        q_start_time = start_time.replace(hour=0, minute=0, second=0,
                                          microsecond=0)
        q_end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0)
        conds = []
        conds += server_conds
        conds += ["data_type_id = %s" % data_type.id]
        conds += ["time >= '%s'" % q_start_time]
        conds += ["time <= '%s'" % q_end_time]
        if value_subsets:
            for i, subset in enumerate(value_subsets):
                if subset is not None:
                    adapt = lambda x: str(self.dtm.remap_value_to_int(x, None))
                    sub_str = ",".join([adapt(sub_v) for sub_v in subset
                                        if sub_v is not None])
                    if None not in subset:
                        # this is normal subset - inclusive
                        conds.append("value%d IN (%s)" % (i+1, sub_str))
                    else:
                        # this is exclusive subset 
                        # - what is listed, we do not want
                        conds.append("value%d NOT IN (%s)" % (i+1, sub_str))
        # construct and run the query
        select = self._create_daily_select(dims)
        logger.debug("SELECT: %s", select)
        query = query_help.construct_query(select, conds)
        logger.debug("QUERY: %s", query)
        logger.debug("Query preparation: %s", time() - _tm)
        _tm = time() 
        cursor.execute(query)
        logger.debug("Query execution: %s", time() - _tm)
        _tm = time()
        raw_data = []
        # here we start to process the data rows
        for row in cursor:
            raw_data.append(row)
        result = DailyDataQueryResults(data_type, start_time, end_time,
                                       raw_data, dims, self)
        logger.debug("Result processing: %s", time() - _tm)
        cursor.close()
        self.conn.commit()
        return result

        

    @classmethod
    def _create_minute_select(cls, dim_count, timespan, split_by_server,
                              ignored_dimension_idx=None):
        """
        creates appropriate select template according to supplied data,
        the definition of arguments can be found in get_minute_data_points
        """
        parts = ["SELECT time AT TIME ZONE 'UTC', minute"]
        table_name = "dscng_data%dd" % (dim_count+1)
        group_by = ""
        if split_by_server:
            parts.append(",server_id")
        # multiD data values
        for i in range(1, dim_count+1):
            if i-1 != ignored_dimension_idx:
                parts.append(",value%d" % i)
                group_by += ",value%d" % i
        # timespan dependent stuff
        if timespan == 1:
            # not accumulated data
            parts.append(",SUM(count)")
            extra = " AND count != -1 "
        else:
            # accumulated data
            parts.append(",SUM(avg_count),SUM(max_count),SUM(min_count)")
            table_name += "_c%d" % timespan
            extra = " AND avg_count != -1 "
        parts.append(" FROM ")
        parts.append(table_name)
        parts.append("%(where_clause)s")
        parts.append(extra)
        parts.append(" GROUP BY time,minute"+group_by)
        if split_by_server:
            parts.append(",server_id")
        parts.append(" ORDER BY time,minute;")
        result = "".join(parts)
        return result


    @classmethod
    def _create_daily_select(cls, dimensions):
        """
        creates appropriate select template according to supplied data,
        the definition of arguments can be found in get_minute_data_points
        """
        dim_count = len(dimensions)
        ip_table = bool([d for d in dimensions if d.type == "ip_addr"])
        if ip_table:
            # this is a specialized case to treat ip based data.
            # it gets around the problem of there being tens of thousands
            # of records while only a few of them get actually displayed
            if dim_count == 2:
                return """SELECT value1,value2,SUM(count)
                FROM dscng_accumdata3d_ip %(where_clause)s
                AND count != -1 AND 
                value2 IN (SELECT DISTINCT(value2) FROM 
                           (SELECT value2,count FROM dscng_accumdata3d_ip 
                            %(where_clause)s AND count != -1
                            ORDER BY count DESC LIMIT 1000)
                           AS X)
                GROUP BY value1,value2;"""
            elif dim_count == 1:
                return """SELECT value1,SUM(count)
                FROM dscng_accumdata2d_ip %(where_clause)s
                AND count != -1 AND 
                value1 IN (SELECT DISTINCT(value1) FROM 
                           (SELECT value1,count FROM dscng_accumdata2d_ip 
                            %(where_clause)s AND count != -1
                            ORDER BY count DESC LIMIT 1000)
                           AS X)
                GROUP BY value1;"""
             
        parts = ["SELECT "]
        table_name = "dscng_accumdata%dd" % (dim_count+1)
        if ip_table:
            table_name += "_ip"
        group_by = []
        select = []
        # multiD data values
        for i in range(1, dim_count+1):
            select.append("value%d" % i)
            group_by.append("value%d" % i)
        select.append("SUM(count)")
        parts.append(",".join(select))
        extra = " AND count != -1 "
        parts.append(" FROM ")
        parts.append(table_name)
        parts.append(" %(where_clause)s")
        parts.append(extra)
        if group_by:
            parts.append(" GROUP BY "+",".join(group_by))
        #parts.append(" ORDER BY time,minute;")
        result = "".join(parts)
        return result
        

    def minute_update_present(self, server_id, data_type_id, minute_time):
        """
        Checks if we already have record of this minute being inserted for this
        server_id and data type
        """
        cursor = self.conn.cursor()
        day_start = minute_time.replace(second=0, minute=0, hour=0)
        minute_idx = int((minute_time - day_start).seconds // 60)
        cursor.execute("""SELECT (minutes<<%s & B'1'::bit(1440))::bit(1) 
            FROM dscng_serverupdates 
            WHERE server_id=%s AND data_type_id=%s AND day=%s;""",
            (minute_idx, server_id, data_type_id, day_start))
        if cursor.rowcount == 0 or cursor.fetchone()[0] == '0':
            return False
        cursor.close()
        self.conn.commit()
        return True
    
    def record_minute_update(self, server_id, data_type_id, minute_time,
                             commit=True):
        """
        Records the presence of data for the server_id, data type and minute at
        hand in order to prevent future updates using the same data - it would
        screw the accum data values.
        If commit is False, we do not commit the changes to the database at
        the end of the call - this can be used to optimize speed for many updates
        """
        cursor = self.conn.cursor()
        if type(server_id) == tuple:
            # we have a parent-server name tuple
            server_id = self.get_or_create_server(*server_id)
        day_start = minute_time.replace(second=0, minute=0, hour=0)
        minute_idx = int((minute_time - day_start).seconds // 60)
        cursor.execute("""UPDATE dscng_serverupdates SET
            minutes = (minutes | ((B'1'::bit(1440))>>%s))
            WHERE server_id=%s AND data_type_id=%s AND day=%s;""",
            (minute_idx, server_id, data_type_id, day_start))
        if cursor.rowcount == 0:
            # we need to insert - there is nothing to update
            cursor.execute("""INSERT INTO dscng_serverupdates
            (server_id, data_type_id, day, minutes)
            VALUES (%s,%s,%s,(B'1'::bit(1440))>>%s);""",
            (server_id, data_type_id, day_start, minute_idx))
        cursor.close()
        if commit:
            self.conn.commit()
        
 
    def get_or_create_server(self, server_name, group_name):
        """
        If server is known, return its id, otherwise insert it into the
        database and return the new id.
        """
        # check if we have this combination of server and parent
        if (server_name, group_name) in self.servers:
            return self.servers[(server_name, group_name)]
        # check if we at least have parent
        group_id = self.servers.get((group_name, None))
        cursor = self.conn.cursor()
        if not group_id:
            # we don't have it in cache - get it or create it
            cursor.execute("""SELECT id FROM dscng_server_group WHERE name=%s
                              AND parent_id IS NULL""",
                            (group_name,))
            parent = cursor.fetchone()
            if not parent:
                cursor.execute(
                    "INSERT INTO dscng_server_group (name) VALUES (%s) "
                    "RETURNING id", (group_name,))
                self.conn.commit()
                parent = cursor.fetchone()
            group_id = parent[0]
        # create the server
        cursor.execute("""SELECT id FROM dscng_server WHERE name=%s
                             AND group_id=%s""",
                            (server_name, group_id))
        server = cursor.fetchone()
        if not server:
            cursor.execute(
                """INSERT INTO dscng_server (name,group_id) 
                 VALUES (%s,%s) RETURNING id""",
                (server_name, group_id))
            self.conn.commit()
            server = cursor.fetchone()
        server_id = server[0]
        self.servers[(server_name, group_name)] = server_id
        cursor.close()
        self.conn.commit()
        return server_id
    
    def update_server_timespan(self, server_id, min_time, max_time):
        """
        Synchronizes the timespan defined by min_time, max_time with the db.
        """
        cursor = self.conn.cursor()
        if min_time is None and max_time is None:
            # delete the record because there is no data
            cursor.execute("""DELETE FROM dscng_servertimespan
                WHERE server_id = %s""", (server_id,))
        else:
            cursor.execute("""SELECT start_time, end_time 
                FROM dscng_servertimespan
                WHERE server_id = %s""", (server_id,))
            rec = cursor.fetchone()
            if rec:
                # a record is there - see if it needs updating
                update = False
                if min_time < rec[0]:
                    update = True
                else:
                    min_time = rec[0]
                if max_time > rec[1]:
                    update = True
                else:
                    max_time = rec[1]
                if update:
                    cursor.execute("""UPDATE dscng_servertimespan 
                    SET start_time=%s, end_time=%s WHERE server_id=%s""",
                    (min_time, max_time, server_id))
            else:
                # no such record yet - insert it
                cursor.execute("""INSERT INTO dscng_servertimespan 
                    (server_id, start_time, end_time) VALUES (%s, %s, %s);""",
                    (server_id, min_time, max_time))
        cursor.close()
        self.conn.commit()
            

    def insert_update_bundle(self, bundle):
        change = False
        for record in bundle.records:
            update = self.insert_data_record(bundle, record)
            change = change or update
        # sync dynamic remaps
        self.dtm.write_dynamic_remaps_to_db()
        # update server timespans
        self.update_server_timespan(bundle.server_id, bundle.timepoint,
                                    bundle.timepoint)
        # process cache update requests
        if change:
            self.register_cache_update_request(bundle.server_id, 
                                               bundle.timepoint)
        self.process_due_cache_update_requests(bundle.server_id, 
                                               bundle.timepoint)
        # do database commit to be sure everything is written out
        self.conn.commit()

    def insert_data_record(self, bundle, record):
        """
        Takes a DataRecord instance and syncs it with the database;
        The bundle variable is an UpdateBundle instance providing additional
        context for the DataRecord 
        Return value says if the data was actually inserted (it is False
        in case of update with already present data) 
        """
        def _update_db(server_id, record):
            if record.data_type.accumulated:
                self._update_accum_data(server_id, record)
            else:
                # the result would be None in case no update was done
                # this is here to detect import of already present data
                result = self._update_minute_data(server_id, record)
                return result
      
        # get matching server
        self._elsify_unwanted_keys(record)
        change = False
        # lets see what the processing method is
        if record.processing in ("as-is", "accum"):
            # we just store it as it is
            updated = _update_db(bundle.server_id, record)
        elif record.processing in ("reduce-count", "reduce-trace"):
            # this data should be reduced
            self._reduce_data(record)
            updated = _update_db(bundle.server_id, record)
        else:
            updated = None
        if updated is not None:
            change = True
        return change


    def _elsify_unwanted_keys(self, record):
        """
        data in record might be for keys that are not used in display and
        storage. Such values are all merged into 'else' records.
        """
        # get valid values for this data type dimensions
        valid_keys_list = []
        for dim in record.dimensions:
            values = self.dtm.get_valid_dimension_values(record.data_type, dim)
            valid_keys_list.append(values)

        new_data = []
        elses = {}
        # we process the data and add unchanged values to new_data
        # in the meantime accumulating 'else' values
        for row in record.data:
            cur_key = []
            for i, keys in enumerate(valid_keys_list):
                if keys is not None and row[i] not in keys:
                    cur_key.append('else')
                else:
                    cur_key.append(row[i])
            if 'else' in cur_key:
                elses[tuple(cur_key)] = elses.get(tuple(cur_key), 0) + row[-1]
            else:
                new_data.append(row)
        for key, count in elses.iteritems():
            new_data.append(key + (count,))
        record.data = new_data
        

    def _reduce_data(self, record):
        """
        Reduction records have all values from the lowest dimensions reduced
        into one value, thus reducing the number of dimensions by 1.
        (trace = sum of all values in the lowest dimension
         count = number of distinct values in the lowest dimension)
        """
        counts = {}
        traces = {}
        # data gathering is very similar for both types of reductions
        # therefore we do it in one go for both
        for row in record.data:
            val2, count = row[-2:]
            if len(record.dimensions) == 0:
                val1 = None
            else:
                val1 = row[0]
            if val2 == "-:SKIPPED:-":
                counts[val1] = counts.get(val1, 0) + count
            elif val2 == "-:SKIPPED_SUM:-":
                traces[val1] = traces.get(val1, 0) + count
            else:
                counts[val1] = counts.get(val1, 0) + 1
                traces[val1] = traces.get(val1, 0) + count
        if record.processing == "reduce-count":
            #record.dimensions=record.dimensions[:-1]
            if record.dimensions:
                record.data = counts.items()
            else:
                record.data = [(counts.get(None, 0),)]
        elif record.processing == "reduce-trace":
            #record.dimensions=record.dimensions[:-1]
            record.data = traces.items()
        else:
            raise ValueError("Unknown reduction method %s" % record.processing)


    def _update_minute_data(self, server_id, record):
        """
        updates minute data using already pre-fetched ids of time_record
        and data_type;
        Returns None if no data was inserted, data_type_id otherwise
        """
        data_type_id = record.data_type.id
        # check if we already have this data
        if self.minute_update_present(server_id, data_type_id,
                                      record.start_time):
            return None
        time_record_id = self._get_or_create_timerecord(server_id,
                                                        record.start_time)
        cursor = self.conn.cursor()
        # construct template for update
        table_name = "dscng_dataarray%dd" % (len(record.dimensions)+1)
        conds = query_help.join_and_prepend(" AND ", 
                                ["value%d=%%s"%(i+1)
                                 for i,_x in enumerate(record.dimensions)])
        update = """UPDATE %s SET counts[%%d]=%%%%s 
                    WHERE time_record_id=%d AND data_type_id=%d %s;""" % \
                    (table_name, time_record_id, data_type_id, conds)
        # construct a template for insert
        cols = ['time_record_id', 'data_type_id']
        cols += ["value%d" % (i+1)
                 for i,_x in enumerate(record.dimensions)]
        cols.append("counts")
        insert = "INSERT INTO %s (%s) VALUES (%s)" % \
                 (table_name, ",".join(cols),
                  ",".join(len(cols)*["%s"]))
        for row in record.data:
            values = []
            for i, orig in enumerate(row[:-1]):
                values.append(self.dtm.remap_user_value_to_db(orig,
                                                        record.dimensions[i]))
            idx = record.start_time.minute+1
            update2 = update % tuple([idx] + values)
            try:
                cursor.execute(update2, (row[-1],))
            except Exception as exc:
                logger.critical("Exception %s", exc)
                self.conn.rollback()
                raise exc
            if cursor.rowcount == 0:
                # the row is not there - we need to insert it
                to_insert = [time_record_id, data_type_id]+values
                array = 60*["-1"]
                array[idx-1] = str(row[-1])
                to_insert.append("{%s}" % ",".join(array))
                try:
                    cursor.execute(insert, to_insert)
                except Exception as exc:
                    logger.critical("Exception %s", exc)
                    self.conn.rollback()
                    raise exc
        self.conn.commit()
        cursor.close()
        # record the fact that we have seen this data
        self.record_minute_update(server_id, data_type_id, record.start_time,
                                  commit=False)
        return data_type_id
        
        
    def _update_cache_data(self, server_id, start_time, data_type_id):
        """
        Updates cache tables for minute data for a specified server, data_type
        and time;
        This method is not used in current code - instead delayed updates
        are realized through combination of register_cache_update_request and
        process_due_cache_update_requests
        """
        data_type = self.dtm.get_data_type(data_type_id=data_type_id)
        dimensions = self.dtm.get_data_type_dimensions(data_type)
        # select all day data
        cursor = self.conn.cursor()
        day_start = start_time.replace(hour=0, minute=0, second=0,
                                       microsecond=0)
        day_end = day_start + timedelta(days=1)
        value_cols = ["value%d" % (i+1)
                       for i,_x in enumerate(dimensions)]
        select = """SELECT time,minute,count%s FROM dscng_data%dd 
                    WHERE server_id=%%s AND data_type_id=%%s AND time>=%%s AND
                    TIME < %%s AND COUNT != -1 ORDER BY time,minute""" % \
                    (query_help.join_and_prepend(",", value_cols),
                     len(dimensions)+1)
        cursor.execute(select, (server_id, data_type_id, day_start, day_end))
        data = []
        for row in cursor:
            data.append((row[0] + timedelta(minutes=row[1]),) + row[2:])
        dim_count = len(dimensions) + 1
        for cpnt in self.caching_points:
            cp_start_time = query_help.round_time_to_timespan(start_time, cpnt,
                                                              upper=False)
            cp_end_time = cp_start_time + timedelta(minutes=cpnt)
            self._update_cache_internal(cursor, server_id, data_type_id,
                                        cp_start_time, cp_end_time, cpnt,
                                        dim_count, data)
        self.conn.commit()
        cursor.close()

    def _update_cache_internal(self, cursor, server_id, data_type_id, 
                               cp_start_time, cp_end_time, cpnt, dim_count,
                               data):
        _qtime, qindex = query_help.split_time_for_query(cp_start_time, cpnt)
        values = {}
        for row in data:
            if row[0] < cp_start_time:
                continue
            elif cp_start_time <= row[0] < cp_end_time:
                count = row[1]
                key = row[2:]
                if key not in values:
                    values[key] = [count]
                else:
                    values[key].append(count)
            else: # act_time >= cp_end_time:
                break
        # get timerecord (or create it)
        time_record_id = self._get_or_create_timerecord_c(server_id,
                                                          cp_start_time,
                                                          timespan=cpnt)
        # insert data using a stored procedure
        to_insert = []
        for key, vals in values.iteritems():
            if not vals:
                min_c, max_c, avg_c = -1, -1, -1
            else:
                min_c = min(vals)
                max_c = max(vals)
                avg_c = sum(vals) // len(vals)
            # try to update, if it does not work - insert it instead
            # check if the record is there and we just need to update
            point_num = 360 // cpnt
            if point_num == 0:
                point_num = 1
            def_avg = point_num*['-1']
            def_min = point_num*['-1']
            def_max = point_num*['-1']
            def_avg[qindex] = str(avg_c)
            def_min[qindex] = str(min_c)
            def_max[qindex] = str(max_c)
            default_avg = "ARRAY[%s]" % (",".join(def_avg))
            default_min = "ARRAY[%s]" % (",".join(def_min))
            default_max = "ARRAY[%s]" % (",".join(def_max))
            to_insert.append(key+(avg_c, min_c, max_c, default_avg,
                                  default_min, default_max))
        sql_data = ",".join(["(%s)::cache_record_%dd" % \
                             (",".join(map(str, row)), dim_count)
                     for row in to_insert])
        cursor.execute("""SELECT update_cache_data_%dd_c%d(
                        %s,%s,%s,ARRAY[%s]::cache_record_%dd[]);""" % \
                       (dim_count, cpnt, time_record_id, data_type_id, 
                        qindex+1, sql_data, dim_count))


    def register_cache_update_request(self, server_id, start_time):
        """
        Register server, time and data_type for cache update
        """
        cursor = self.conn.cursor()
        for cpnt in self.caching_points:
            # check if a pending request is already there
            cursor.execute("""SELECT 1 FROM dscng_cache_request
                              WHERE server_id=%s
                              AND timespan=%s AND start_time<=%s
                              AND due_time>=%s""",
                              (server_id, cpnt, start_time, start_time))
            res = cursor.fetchone()
            if res is None:
                # the record was not there - lets create it
                cp_start_time = query_help.round_time_to_timespan(start_time,
                                                                  cpnt,
                                                                  upper=False)
                cp_end_time = cp_start_time + timedelta(minutes=cpnt)
                cursor.execute("""INSERT INTO dscng_cache_request
                    (server_id, timespan, start_time, due_time)
                    VALUES (%s,%s,%s,%s)""",
                    (server_id, cpnt, cp_start_time, cp_end_time)) 
        self.conn.commit()
        cursor.close()

    
    def process_due_cache_update_requests(self, server_id, time_point):
        """
        Take all requests for cache update for server with server_id which 
        are due to be processed a time time_point and process them.
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT server_id, timespan, start_time, due_time 
                          FROM dscng_cache_request
                          WHERE server_id=%s AND due_time<%s""",
                          (server_id, time_point))
        for server_id, cpnt, start_time, due_time in cursor:
            self._do_cache_update(server_id, cpnt, start_time, due_time)
        # delete the cache update requests
        cursor.execute("""DELETE FROM dscng_cache_request
                          WHERE server_id=%s AND due_time<%s""",
                          (server_id, time_point))
        self.conn.commit()
        cursor.close()

    def process_all_cache_update_requests(self):
        """
        Processes all cache update requests regardless of their due time or 
        server_id
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT server_id, timespan, start_time, due_time 
                          FROM dscng_cache_request""")
        for server_id, cpnt, start_time, due_time in cursor:
            self._do_cache_update(server_id, cpnt, start_time, due_time)
        # delete the cache update requests
        cursor.execute("DELETE FROM dscng_cache_request")
        self.conn.commit()
        cursor.close()
        

    def _do_cache_update(self, server_id, cpnt, start_time, due_time):
        """
        Internal function meant to be supplied with parameters from
        cache_update_requests table.
        """
        cursor = self.conn.cursor()
        for data_type in self.dtm.get_data_types():
            dimensions = self.dtm.get_data_type_dimensions(data_type)
            dim_count = len(dimensions) + 1
            value_cols = ["value%d" % (i+1)
                           for i,_x in enumerate(dimensions)]
            select = """SELECT time,minute,count%s FROM dscng_data%dd 
                        WHERE server_id=%%s AND data_type_id=%%s
                          AND time>=%%s AND time < %%s
                          AND COUNT != -1 ORDER BY time,minute""" %\
                        (query_help.join_and_prepend(",", value_cols),
                         dim_count)
            data_stm, _x = query_help.split_time_for_query(start_time, cpnt)
            cursor.execute(select,
                           (server_id, data_type.id, data_stm, due_time))
            data = []
            for row in cursor:
                data.append((row[0] + timedelta(minutes=row[1]),) + row[2:])
            self._update_cache_internal(cursor, server_id, data_type.id,
                                        start_time, due_time, cpnt, dim_count,
                                        data)
        cursor.close()


    def _update_accum_data(self, server_id, record):
        data_type_id = record.data_type.id
        ip_type = "ip_addr" in [dim.type for dim in record.dimensions]
        # check if we already have this data
        if self.minute_update_present(server_id, data_type_id,
                                      record.start_time):
            return False
        time_record_id = self._get_or_create_timerecord_accum(server_id,
                                                              record.start_time)
        cursor = self.conn.cursor()
        skipped_vals = {}
        to_insert = []
        # process each row
        for row in record.data:
            values = []
            skip_row = 0
            for i, orig in enumerate(row[:-1]):
                # do we have skipped data here? - it yes, we add it to
                # skipped_vals for later processing and skip this row
                # for now
                if orig == "-:SKIPPED:-":
                    skip_row = 1
                elif orig == "-:SKIPPED_SUM:-":
                    skip_row = 2
                elif record.dimensions[i].type == 'ip_addr':
                    values.append(orig)
                else:
                    values.append(self.dtm.remap_user_value_to_db(orig,
                                                                  record.dimensions[i]))
            count = row[-1]
            if skip_row:
                key = tuple(values)
                if key not in skipped_vals:
                    skipped_vals[key] = [-1, -1]
                skipped_vals[key][skip_row-1] = count
                continue
            to_insert.append((time_record_id, data_type_id)+tuple(values)+
                             (count,))

        # put data into the db using a stored procedure
        dim_count = len(record.dimensions)+1
        if ip_type:
            procedure_suffix = "_ip"
            rec_type = "dscng_datapoint%dd_accum_ip" % dim_count
            row_temp = ",".join(dim_count*["%d"] + ["'%s'", "%d"])
            sql_data = ",".join(["(%s)::%s"%(row_temp%tuple(row), rec_type)
                                 for row in to_insert])
        else:
            procedure_suffix = ""
            rec_type = "dscng_datapoint%dd_accum" % dim_count
            sql_data = ",".join(["(%s)::%s"%(",".join(map(str, row)), rec_type)
                                 for row in to_insert])

        try:
            cursor.execute("SELECT update_accum_data_%dd%s(ARRAY[%s]::%s[]);" %
                           (dim_count, procedure_suffix, sql_data, rec_type))
        except Exception as exc:
            logger.critical("Exception in accum_data update: %s", exc)
            self.conn.rollback()
            raise exc
        # process skipped values here
        for key, val in skipped_vals.iteritems():
            # try update first
            conds = " ".join(["AND value%d = %%s" % i
                              for i in range(1, dim_count-1)])
            update = """UPDATE dscng_dailystats%dd
                SET skipped = skipped + %%s,
                    skipped_count = skipped_count + %%s
                WHERE time_record_id=%d AND data_type_id=%d %s;""" % \
                     (dim_count, time_record_id, data_type_id, conds)
            cursor.execute(update, tuple(val)+key)
            if cursor.rowcount == 0:
                # we need to insert
                val_cols = [",value%d" % i for i in range(1, dim_count-1)]
                insert = """INSERT INTO dscng_dailystats%dd
                        (time_record_id, data_type_id %s, skipped,
                        skipped_count) VALUES (%s)""" % \
                         (dim_count, " ".join(val_cols),
                          ",".join((2+dim_count)*["%s"]))
                try:
                    cursor.execute(insert,
                                   (time_record_id, data_type_id)+key+tuple(val))
                except Exception, exc:
                    logger.critical("Exception: %s", exc)
                    self.conn.rollback()
                    raise exc

        # commit it all to db and exit
        self.conn.commit()
        cursor.close()
        # record the fact that we have seen this data
        self.record_minute_update(server_id, data_type_id, record.start_time,
                                  commit=False)

    def _get_or_create_timerecord(self, server_id, start_time):
        """
        returns an id of a timerecord that is guaranteed to exist because
        it is created if it cannot be found in the database already.
        """
        cursor = self.conn.cursor()
        start_time = query_help.round_time_to_timespan(start_time, 60,
                                                       upper=False)
        select = """SELECT * FROM dscng_timerecord 
                    WHERE time=%s AND server_id=%s"""
        cursor.execute(select, (start_time, server_id))
        time_record = cursor.fetchone()
        if not time_record:
            cursor.execute(
                """INSERT INTO dscng_timerecord
                (server_id, time) VALUES (%s,%s) RETURNING id""",
                (server_id, start_time))
            self.conn.commit()
            time_record = cursor.fetchone()
        time_record_id = time_record[0]
        cursor.close()
        self.conn.commit()
        return time_record_id
    
        
    def _get_or_create_timerecord_accum(self, server_id, start_time):
        """
        returns an id of a timerecord that is guaranteed to exist because
        it is created if it cannot be found in the database already.
        """
        cursor = self.conn.cursor()
        start_time = start_time.replace(hour=0, minute=0, second=0,
                                        microsecond=0)
        select = """SELECT * FROM dscng_timerecord_accum
                    WHERE time=%s AND server_id=%s"""
        cursor.execute(select, (start_time, server_id))
        time_record = cursor.fetchone()
        if not time_record:
            cursor.execute(
                """INSERT INTO dscng_timerecord_accum
                (server_id, time) VALUES (%s,%s) RETURNING id""",
                (server_id, start_time))
            self.conn.commit()
            time_record = cursor.fetchone()
        time_record_id = time_record[0]
        cursor.close()
        self.conn.commit()
        return time_record_id
        
    def _get_or_create_timerecord_c(self, server_id, start_time, timespan=1):
        """
        returns an id of a timerecord that is guaranteed to exist because
        it is created if it cannot be found in the database already.
        """
        cache_key = ("time_record_c_id", server_id, start_time, timespan)
        time_record_id = self._cache.get(cache_key)
        if time_record_id is not None: 
            return time_record_id
        cursor = self.conn.cursor()
        if timespan <= 360:
            step = 360
        else:
            step = 1440
        start_time = query_help.round_time_to_timespan(start_time,
                                                       step, upper=False)
        select = """SELECT * FROM dscng_timerecord_c 
                    WHERE time=%s AND server_id=%s AND timespan=%s""" 
        cursor.execute(select, (start_time, server_id, timespan))
        time_record = cursor.fetchone()
        if not time_record:
            cursor.execute(
                """INSERT INTO dscng_timerecord_c 
                (server_id, time, timespan) VALUES (%s,%s,%s) RETURNING id""",
                (server_id, start_time, timespan))
            self.conn.commit()
            time_record = cursor.fetchone()
        time_record_id = time_record[0]
        cursor.close()
        self.conn.commit()
        self._cache.set(cache_key, time_record_id)
        return time_record_id



class MinuteDataQueryResults(object):
    """
    This object represents data returned from a query for minute based data.
    The user can apply different postprocessing methods to further manipulate
    the data.
    WARNING: All postprocessing is done in place, so once it is finished, there
    is no way back to the original data.
    """
    
    def __init__(self, data_type, start_time, end_time, timespan, data, types):
        self.start_time = start_time
        self.end_time = end_time
        self.timespan = timespan
        self.data_type = data_type
        self.data = data
        self.types = types
        self._postprocessing_steps = []
        
    def add_post_processing_step(self, name, *args, **kwargs):
        """
        Adds a postprocessing step by name. This step will be used to process
        data prior to returning them from get_time_series
        """
        if not hasattr(self, "_postprocess_"+name):
            raise ValueError("No such postprocessing step: %s" % name)
        self._postprocessing_steps.append((name, args, kwargs))
 
    
    def get_time_series(self):
        """a generator returning data one dictionary for one time point
        at a time. It applies all preprocessing steps specified before
        returning the data"""
        logger.debug("Time points: %s", len(self.data))
        # apply all post-processing methods
        for pps, args, kwargs in self._postprocessing_steps:
            post_method = getattr(self, "_postprocess_"+pps)
            post_method(*args, **kwargs)
        # yield the resulting data
        for rec_tm in sorted(self.data):
            row = self.data[rec_tm]
            row['time'] = rec_tm
            yield row
 
    
    def _postprocess_add_zeros_for_empty_timepoints(self):
        """
        For sparsely occupied data series, such as TCP traffic, etc, it is
        likely that a query between two times will result in data that span
        only a much narrower time window. This would mean, that the results
        will not cover the timespan requested, but a shorter one.
        This post-processing method adds zero value records for all time-points
        which are completely missing.
        """
        # if zeros_for_blanks is active, put zero records at the start and end
        # times if they are not there already
        st_time = query_help.round_time_to_timespan(self.start_time,
                                                    self.timespan,
                                                    upper=True)
        e_time = query_help.round_time_to_timespan(self.end_time,
                                                   self.timespan,
                                                   upper=False)
        delta = timedelta(minutes=self.timespan)
        # we ask using UTC to get over DST problems
        cur_time = st_time.astimezone(query_help.UTC())
        # we use local timezone when adding data to make all data compatible
        if self.data:
            local_tz = self.data.keys()[0].tzinfo
        else:
            local_tz = query_help.UTC()
        while cur_time <= e_time:
            if cur_time not in self.data:
                self.data[cur_time.astimezone(local_tz)] = \
                                            dict.fromkeys(self.types, 0)
            cur_time += delta

    def _postprocess_merge_timezone(self, tz_offset):
        """
        The gviz API does not work with timezones, so it is possible to use
        this post-processing step to convert all datetime values from whatever
        timezone to UTC.
        """
        # if zeros_for_blanks is active, put zero records at the start and end
        # times if they are not there already
        new_data = {}
        tz = query_help.FixedOffset(tz_offset)
        for tz_time, row in self.data.iteritems():
            new_time = tz_time.astimezone(tz)
            new_data[new_time] = row
        self.data = new_data
        
    def _postprocess_relative_scale_sum_is_1(self):
        """
        Adjust the values in such a way that sum of values for a particular
        point is 1, this allows for better detection of trends in periodically
        changing data, such as daily cycling
        """
        for _rec_time, series in self.data.iteritems():
            total = sum([val for (key,val) in series.iteritems()
                         if not key.endswith("__min")
                            and not key.endswith("__max")])
            if total != 0:
                for key in series:
                    series[key] /= total
                    
    def _postprocess_relative_scale_max_is_1(self):
        """
        Adjust the values in such a way that the max of values for a particular
        point is 1, this allows for better detection of trends in periodically
        changing data, such as daily cycling
        """
        for _rec_time, series in self.data.iteritems():
            nonzero = [val for (key,val) in series.iteritems()
                       if not key.endswith("__min")
                        and not key.endswith("__max") and val > 0]
            total = max(nonzero) if nonzero else 0
            if total != 0:
                for key in series:
                    series[key] /= total
                



class DailyDataQueryResults(object):
    """
    This object represents data returned from a query for daily data.
    """
    
    def __init__(self, data_type, start_time, end_time, data, dimensions,
                 storage):
        self.start_time = start_time
        self.end_time = end_time
        self.data_type = data_type
        self.data = data
        self.storage = storage
        self.dimensions = dimensions

    def postprocess_expand_ip_with_geoip(self):
        import GeoIP
        gi = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
        for i, dim in enumerate(self.dimensions):
            if dim.name == "ip_addr":
                ip_idx = i
                break
        else:
            raise ValueError("This data does not contain IP addresses")
        self.dimensions.insert(ip_idx+1, Dimension(db_id=-1, name="geoip"))
        new_data = []
        for row in self.data:
            ip_value = row[ip_idx]
            geo_ip_value = gi.country_code_by_addr(ip_value) or "unknown"
            new_data.append(row[:ip_idx] + (ip_value, geo_ip_value) + 
                            row[ip_idx+1:])
        self.data = new_data
            

    def get_data(self, grouping_dimension, key_count=20, order_by=None,
                 max_key_value=None, count_per_second=False, subkey_count=None,
                 key_rel_threshold=0):
        """
        grouping_dimension is the one that will determine individual columns
        in the output,
        key_count is the number of resulting series (keys, columns) to return,
        max_key_value is the value in the grouping_dimension above which we 
        should not proceed,
        count_per_second - should the counts be converted from daily to second
        based values?,
        subkey_count - how many individual values should be allowed in one
        column
        key_rel_threshold - keys with sum lower than key_rel_threshold * max_sum
        will be removed from the result. This reduces the number of keys by
        allowing only the most populated ones to appear
        """
        if count_per_second:
            divider = 86400.0
        else:
            divider = 1.0
        # prepare remaps for dimensions
        dim_remaps = []
        for dim in self.dimensions:
            dim_remaps.append(self.storage.get_db_to_user_value_remap(dim.id))
        _t = time()
        group_idx = None
        for i, dim in enumerate(self.dimensions):
            if dim.id == grouping_dimension.id:
                group_idx = i
                break
        else:
            raise ValueError(
                    "Given grouping dimension is not used in this result")
        grouped = {}
        subkey_to_count = {} # this is used to cut-off subkeys and merge them
                            # into one called 'Other'
        for row in self.data:
            key_value = row[group_idx]
            if max_key_value and key_value > max_key_value:
                continue
            key = dim_remaps[group_idx].get(key_value, key_value) 
            subkey = "-".join([dim_remaps[i].get(v,str(v))
                               for i,v in enumerate(row[:-1])
                               if i != group_idx])
            if key not in grouped:
                grouped[key] = {}
            grouped[key][subkey] = float(row[-1])/divider
            subkey_to_count[subkey] = subkey_to_count.get(subkey, 0) + row[-1]
        logger.debug("Grouping %s", time()-_t)       
        # here we potentially merge subkeys with lowest counts
        _t = time()
        if len(subkey_to_count) > subkey_count:
            to_sort = [(v,k) for k,v in subkey_to_count.iteritems()]
            to_sort.sort(reverse=True)
            subkeys_to_use = [name
                              for (count, name) in to_sort[:subkey_count-1]]
            new_grouped = {}
            for key, row in grouped.iteritems():
                new_row = {'other': 0}
                for key2, count in row.iteritems():
                    if key2 in subkeys_to_use:
                        new_row[key2] = count
                    else:
                        new_row['other'] += count
                new_grouped[key] = new_row
            grouped = new_grouped
        logger.debug("Cutting off subkeys %s", time()-_t)
        # we sort according to total count of a record in order
        # to enable cut-off of the requested data
        if order_by == "count":
            to_sort = [(sum(row.values()),point_name)
                       for point_name,row in grouped.iteritems()]
            to_sort.sort(reverse=True)
        else:
            to_sort = [(point_name,point_name)
                       for point_name,row in grouped.iteritems()]
            to_sort.sort(reverse=False)
        result = []
        types = set()
        count_threshold = 0
        if key_rel_threshold and grouped:
            max_val = max([sum(v.values()) for k,v in grouped.iteritems()])
            count_threshold = key_rel_threshold * max_val
        i = 0
        for _count_sum, point_name in to_sort:
            row = grouped[point_name]
            if count_threshold:
                count = sum(row.values())
                if count < count_threshold:
                    continue
            types |= set(row.keys())
            row['point_name'] = point_name
            result.append(row)
            if key_count and i >= key_count:
                break
            i += 1
        # sort types
        types = list(types)
        types.sort()
        return types, result


class UpdateBundle(object):
    """
    Object describing one piece of update data. It contains data for one server
    at one time. It contains many DataRecord instances for individual data types
    """
    
    def __init__(self, server_id=None, timepoint=None):
        self.server_id = server_id
        self.timepoint = timepoint
        self.records = []
        
    def __unicode__(self):
        return "UpdateBundle: server: %s, time: %s, %d records" % \
            (self.server_id, self.timepoint, len(self.records))
        
    def __str__(self):
        return unicode(self).encode('utf-8')
    

class DataRecord(object):
    """intermediate object for storage of data of one data type"""
    
    def __init__(self, **kw):
        self.data_type_name = kw.get('data_type_name')
        self.data_type = kw.get('data_type')
        self.start_time = kw.get('start_time')
        self.dim_count = kw.get('dim_count')
        self.processing = kw.get('processing')
        self.dimensions = []
        self.data = []
        
    def __unicode__(self):
        return "DataRecord: %s (%s)" % (self.data_type_name,
                                        self.start_time)
        
    def __str__(self):
        return unicode(self).encode('utf-8')
    
    def clone(self):
        new_dr = DataRecord(data_type_name=self.data_type_name,
                            start_time=self.start_time,
                            processing=self.processing,
                            dim_count=self.dim_count)
        new_dr.dimensions = copy.deepcopy(self.dimensions)
        new_dr.data = copy.deepcopy(self.data)
        return new_dr
        
        
        