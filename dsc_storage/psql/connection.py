# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# third party module for PostgreSQL communication
try:
    import psycopg2
except ImportError:
    try:
        # try psycopg2ct (c-types version of psycopg)
        import psycopg2ct as psycopg2
    except ImportError:
        raise ImportError("No psycopg2 implementation found")
    
# local import
from version import DB_SCHEMA_VERSION


class ConnectionException(Exception):
    """
    Class of exceptions related to database connection problems
    """
    pass


class ConnectionManager(object):
    """
    This object wraps a connection and performs a reconnect if needed, etc.
    """
    
    def __init__(self, ignore_version_error=False, *args, **kwargs):
        self.conn_args = args
        self.conn_kwargs = kwargs
        self._conn = None # here we store the wrapped connection
        # do a connect and potentially a check
        self._connect()
        if not ignore_version_error:
            ok, code_version, db_version = self.check_db_schema_version()
            if not ok:
                self.close()
                raise ConnectionException(
                    "Database version does not match the expected value: \n"
                    "Expected value: %d, Found: %d\n"
                    "You have to run update_db_schema.py before proceeding." \
                    % (code_version, db_version))
        
    def __getattr__(self, name):
        """
        This is used for methods and attrs that are not explicitly wrapped
        """
        self._connect()
        if hasattr(self._conn, name):
            return getattr(self._conn, name)
        else:
            raise AttributeError("Wrapped connection does not have "
                                 "attribute %s" % name)
        
    def _connect(self):
        """
        Ensures the connection is ready. It is safe to call this multiple times
        """
        # test for problems - only execute will check if connection is alive
        if self._conn:
            cursor = self._conn.cursor()
            try:
                cursor.execute("SELECT 1")
            except psycopg2.OperationalError:
                # the connection is not working anymore - get another one
                self._conn = None
        self.open()

    def open(self):
        """
        Used to explicitly open the connection again if it does not exist or
        if it's flagged as closed.
        """
        if not self._conn or self._conn.closed:
            self._conn = psycopg2.connect(*self.conn_args, **self.conn_kwargs)

    def cursor(self, *args):
        """
        Wrapper for connection cursor method
        """
        self._connect()
        return self._conn.cursor(*args)
    
    def close(self):
        """
        Wrapper for connection close method
        """
        if self._conn and not self._conn.closed:
            self._conn.close()
            
    def commit(self):
        """
        Wrapper for connection commit method
        """
        # we do not check closed connection as we want potential errors to
        # propagate from here - there is no point in opening a new connection
        # for rollback 
        if self._conn:
            self._conn.commit()
            
    def rollback(self):
        """
        Wrapper for connection rollback method
        """
        # we do not check closed connection as we want potential errors to
        # propagate from here - there is no point in opening a new connection
        # for rollback 
        if self._conn:
            self._conn.rollback()
            
    def get_db_schema_version(self):
        """returns database version stored in the dscng_metadata table"""
        cursor = self.cursor()
        cursor.execute("""SELECT value FROM dscng_metadata
                          WHERE section=%s AND name=%s""",
                          ("db_static", "schema_version"))
        db_version = cursor.fetchone()
        if db_version is None:
            raise ConnectionException(
                            "Schema version is not stored in the database")
        else:
            return int(db_version[0])  # fetchone returns a tuple
            
    def check_db_schema_version(self):
        """
        Checks if the database schema version stored in the dscng_metadata
        table matches the current version.
        Returns (ok, code_version, db_version) tuple, where ok is a boolean
        describing compatibility of the schema versions
        """
        db_version = self.get_db_schema_version()
        return (db_version == DB_SCHEMA_VERSION, DB_SCHEMA_VERSION, db_version)


def create_connection_manager_from_settings(settings,
                                            ignore_version_error=False):
    """
    Returns a connection manager based on data from the settings module
    ignore_version_error is passed to the ConnectionManager constructor
    """
    db_params = get_connection_params_from_settings(settings)
    conn = ConnectionManager(ignore_version_error=ignore_version_error,
                             **db_params)
    return conn


def get_connection_params_from_settings(settings):
    """
    Extracts connection parameters from settings and puts it into a dictionary
    """
    db_params = dict(database=settings.DATABASE_NAME,
                     user=settings.DATABASE_USER,
                     password=settings.DATABASE_PASSWORD,
                     host=settings.DATABASE_HOST,
                     port=settings.DATABASE_PORT or None)
    return db_params