# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module hosts classes dealing with data types, dimensions and value remaps.
The main class here is the DataTypeManager
"""

# built-in modules
import logging
from cStringIO import StringIO
import json

# local imports
from db_types import DataType, Dimension
from caching import Cache

class DataTypeManager(object):
    """
    This class manages data types, dimensions and value remaps.
    It provides methods for retrieval and storage of these object
    """

    def __init__(self, db_connection):
        self.conn = db_connection
        self._active_user_db_remaps = {}
        self.dimension_name_to_obj = {}
        self.defined_data_types = {}
        self._cursor = None
        self._cache = Cache()

    @property
    def cursor(self):
        if not self._cursor or self._cursor.closed:
            self._cursor = self.conn.cursor()
        return self._cursor

    def get_data_types(self, lang="en"):
        """
        returns available data types as list of instances of DataType
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT id,name,accumulated,parent,local_name
            FROM dscng_datatype
                JOIN dscng_data_type_name ON data_type_id=dscng_datatype.id
            WHERE lang=%s;""", (lang,))
        result = []
        for type_id, name, accumulated, parent_id, local_name in cursor:
            result.append(DataType(type_id, name=name, accumulated=accumulated,
                                   parent_id=parent_id, local_name=local_name))
        cursor.close()
        self.conn.commit()
        return result


    def get_data_type_dimensions(self, data_type):
        """
        returns Dimension instances for each Dimension associated with
        a data_type in form of a list
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT
                dscng_dimension.id,name,type FROM dscng_dimension
                JOIN dscng_dimensiontodatatype
                ON dimension_id = dscng_dimension.id 
                WHERE data_type_id = %s ORDER BY position""", (data_type.id,))
        result = []
        for dim_id, name, dim_type in cursor:
            result.append(Dimension(dim_id, name=name, dim_type=dim_type))
        cursor.close()
        self.conn.commit()
        return result
    
    
    def get_valid_dimension_values(self, data_type, dimension):
        """
        Returns a list of valid values for a combination of data_type and 
        dimension. Any value not in the list should be 'elsified' - aggregated
        into the 'else' value.
        None means that any value is allowed.
        (Because the valid values may be data type specific for a dimension,
        we need the data type as input as well)
        """
        cache_key = ("valid_dim_vals", data_type.id, dimension.id)
        # use cache value if available
        cached = self._cache.get(cache_key)
        if cached is not None:
            return cached
        # not in cache
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT
                dscng_dimension.meta as dim_meta,
                dscng_dimensiontodatatype.meta as d2d_meta
            FROM dscng_dimension
                    JOIN dscng_dimensiontodatatype
                    ON dimension_id = dscng_dimension.id 
            WHERE data_type_id = %s AND dimension_id = %s""",
            (data_type.id, dimension.id))
        row = cursor.fetchone()
        if not row:
            raise ValueError("No record for data type and dimension "\
                             "combination")
        dim_meta_str, d2d_meta_str = row
        values = None
        if d2d_meta_str:
            # the metadata in d2d have preference
            d2d_meta = json.loads(d2d_meta_str)
            if "values" in d2d_meta:
                values = d2d_meta.get("values")
            elif dim_meta_str:
                dim_meta = json.loads(dim_meta_str)
                values = dim_meta.get("values")
        cursor.close()
        self.conn.commit()
        self._cache.set(cache_key, values)
        return values
    
    
    @classmethod
    def decode_int_value(cls, value):
        """
        Even int type dimensions can have value 'else' and 'none' in them.
        This method can be used to decode such values from a string
        """
        if value == "else":
            return -1
        elif value == "none":
            return -2
        else:
            return int(value)

    
    def remap_user_value_to_db(self, value, dimension):
        """
        Based on the provided remap remaps the value to int.
        It takes care of special values, such as 'else' or 'none' which have
        stable default mappings.
        """
        remap = self.get_user_to_db_value_remap(dimension)
        if remap is not None:
            return remap[value]
        else:
            if value == "else":
                return -1
            elif value == "none":
                return -2
            else:
                return int(value)


    def get_user_to_db_value_remap(self, dimension):
        """
        Returns a value remap as a dict (DynamicDict) for a dimension.
        None means that the values should be stored as they are.
        The DynamicDict creates new integer remap values when queried for a
        non-existent value. Such values should be later synced with the db
        by running write_dynamic_remaps_to_db
        """
        if dimension.type == "int":
            # ints do not need a remap
            return None
        if dimension.type == "ip_addr":
            # IP addresses do not need a remap
            return None
        elif dimension.id in self._active_user_db_remaps:
            # we already have an active_remap - there is nothing more to do
            return self._active_user_db_remaps[dimension.id]
        else:
            # this creates a remap from database, possibly using default
            # data specified above in class definition
            remap = self.create_remap(dimension)
            return remap


    def create_remap(self, dimension, default=None):
        """
        creates a remap. It uses default remap if given and also reads data
        from the database if present.
        It does not store the values into the database - this must be done
        explicitly by calling write_dynamic_remaps_to_db
        """
        # default or empty remap
        if not default:
            default = {}
        remap = DynamicDict(default)
        # get what is in the db - rewriting possible duplicates
        db_remap = self.read_dynamic_remap_from_db(dimension.id)
        remap.update(db_remap)
        self._active_user_db_remaps[dimension.id] = remap
        return remap


    def read_dynamic_remap_from_db(self, dimension_id):
        """
        Fetches already stored remaps for the dimension of dimension_id
        and returns it in a form of DynamicDict.
        """
        self.cursor.execute("""SELECT original,remap 
            FROM dscng_str_to_int_remap
            WHERE dimension_id=%s""", (dimension_id,))
        remap = DynamicDict()
        for orig, value in self.cursor.fetchall():
            remap[orig] = value
        self.conn.commit()
        return remap


    def write_dynamic_remaps_to_db(self):
        """
        Takes all currently active remaps (self._active_user_db_remaps) and syncs
        them with the database.
        """
        for dimension_id in self._active_user_db_remaps:
            if self._active_user_db_remaps[dimension_id].is_dirty():
                # there are new values
                current_db_remap = self.read_dynamic_remap_from_db(dimension_id)
                to_insert = []
                for key, val in self._active_user_db_remaps[dimension_id].iteritems():
                    if key not in current_db_remap:
                        to_insert.append((key, val))
                if to_insert:
                    # insert the missing values
                    data_file = StringIO()
                    for rec in sorted(to_insert):
                        data_file.write("%d\t%s\t%d\n" % \
                                        (dimension_id,
                                         rec[0].replace("\\", "\\\\"),
                                         rec[1]))
                    data_file.seek(0)
                    try:
                        self.cursor.copy_from(
                                data_file,
                                "dscng_str_to_int_remap",
                                columns=('dimension_id','original','remap'),
                                )
                    except Exception as exc:
                        self.conn.rollback()
                        logging.error("Error when writing remaps: %s", exc)
                        raise
                    self.conn.commit()
                    data_file.close()
                self._active_user_db_remaps[dimension_id].cleanup()


    def get_dimension(self, name=None, dim_id=None):
        """
        Return a Dimension instance representing a database record for dimension
        either by name of id
        """
        cursor = self.conn.cursor()
        select = "SELECT id,name,type,meta FROM dscng_dimension WHERE %s=%%s;"
        if name is not None:
            cursor.execute(select % "name", (name,))
        elif dim_id is not None:
            cursor.execute(select % "id", (dim_id,))
        else:
            raise ValueError("You must supply either a name or dim_id.")
        db_row = cursor.fetchone()
        if db_row is None:
            raise ValueError("No matching dimension found.")
        db_id, name, dim_type, meta = db_row
        result = Dimension(db_id=db_id, name=name, dim_type=dim_type, meta=meta)
        cursor.close()
        self.conn.commit()
        return result
    
    
    def get_dimensions(self, name=None, dim_id=None):
        """
        Return a list of Dimension instances for all known dimensions
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id,name,type,meta FROM dscng_dimension;")
        result = []
        for db_id, name, dim_type, meta in cursor:
            result.append(Dimension(db_id=db_id, name=name, dim_type=dim_type,
                                    meta=meta))
        cursor.close()
        self.conn.commit()
        return result


    def get_data_type(self, data_type_id=None, name=None, lang='en'):
        """
        return a DataType instance either by its id or its name
        """
        cursor = self.conn.cursor()
        template = """SELECT id,name,accumulated,parent,local_name 
            FROM dscng_datatype 
                JOIN dscng_data_type_name ON data_type_id=dscng_datatype.id
            WHERE %s=%%s AND lang=%%s"""
        if data_type_id is not None:
            select = template % "id"
            value = data_type_id
        else:
            select = template % "name"
            value = name
        cursor.execute(select, (value, lang))
        data = cursor.fetchone()
        cursor.close()
        self.conn.commit()
        if data is None:
            return None
        type_id, name, accumulated, parent_id, local_name = data 
        return DataType(type_id, name=name, accumulated=accumulated,
                        parent_id=parent_id, local_name=local_name)
        
    def get_data_source_id(self, name=None):
        """
        Returns an object representing a data source
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id FROM dscng_data_source WHERE name=%s",
                       (name,))
        dim_row = cursor.fetchone()
        cursor.close()
        self.conn.commit()
        if dim_row:
            return dim_row[0]
        return None
    
    def get_incoming_data_types(self, source_id):
        """
        Return description of incoming data type mappings. The return value is 
        a dictionary mapping incoming data type names to 
        tuples of (mapped_data_type_id, processing_method).
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT data_type_id, name_in_data_source, processing
            FROM dscng_incoming_data_type WHERE data_source_id=%s""",
            (source_id,))
        result = {}
        for dt_id, name, processing in cursor:
            # there can be one name several times with different processing
            if name not in result:
                result[name] = []
            result[name].append((dt_id, processing))
        cursor.close()
        self.conn.commit()
        return result
    
    def get_incoming_dimensions(self, source_id):
        """
        Retrieves information about incoming dimensions.
        Returns a dictionary mapping data_type_id to a dict mapping dimension
        name is the source to dimension_id in the database 
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT data_type_id, name_in_data_source, dimension_id
            FROM dscng_incoming_dimension WHERE data_source_id=%s""",
            (source_id,))
        result = {}
        for dt_id, name, dim_id in cursor:
            # there can be one name several times with different processing
            if dt_id not in result:
                result[dt_id] = {}
            result[dt_id][name] = dim_id
        cursor.close()
        self.conn.commit()
        return result


    def get_or_create_dimension(self, name, dim_type, meta=""):
        """
        create a dimension in the database or get it if it exists
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id FROM dscng_dimension WHERE name=%s", (name,))
        dim_row = cursor.fetchone()
        if not dim_row:
            cursor.execute("""INSERT INTO dscng_dimension
            (name, type, meta)
             VALUES (%s,%s,%s) RETURNING id""", (name, dim_type, meta))
            self.conn.commit()
            dim_row = cursor.fetchone()
        dim_id = dim_row[0]
        dim_obj = Dimension(dim_id, name, dim_type, meta)
        self.dimension_name_to_obj[name] = dim_obj
        cursor.close()
        self.conn.commit()
        return dim_obj

    def connect_data_type_and_dimension(self, data_type_id, dim, position,
                                        meta=None):
        """
        Establish a connection between a data_type and a dimension in the db.
        Position is required as it specifies the order of dimensions, meta can
        describe allowed values for this combination (serialized to json)
        """
        cursor = self.conn.cursor()
        cursor.execute("""SELECT * FROM dscng_dimensiontodatatype
            WHERE data_type_id=%s AND dimension_id=%s AND position=%s""",
            (data_type_id, dim.id, position))
        dt_to_dim_row = cursor.fetchone()
        if not dt_to_dim_row:
            cursor.execute("""INSERT INTO dscng_dimensiontodatatype 
            (data_type_id, dimension_id, position, meta)
             VALUES (%s,%s,%s,%s)""", (data_type_id, dim.id, position, meta))
            self.conn.commit()
        cursor.close()
        self.conn.commit()
        
        
    def add_localized_data_type_name(self, data_type_id, lang, name):
        """
        Update or add a new localized name associated with a data type
        """
        cursor = self.conn.cursor()
        cursor.execute("""UPDATE dscng_data_type_name
            SET local_name=%s
            WHERE data_type_id=%s AND lang=%s""",
            (name.encode('utf-8'), data_type_id, lang))
        if cursor.rowcount == 0:
            cursor.execute("""INSERT INTO dscng_data_type_name
            (data_type_id, lang, local_name)
             VALUES (%s,%s,%s)""", (data_type_id, lang, name.encode('utf-8')))
        self.conn.commit()
        cursor.close()
        

    def load_data_type_defs_from_json(self, dt_filename, dim_filename):
        """
        Takes data type and dimension definitions from JSON files and loads
        them into the database.
        """
        with file(dt_filename, "r") as dt_js_file:
            dts = json.load(dt_js_file)
        with file(dim_filename, "r") as dim_js_file:
            dims = json.load(dim_js_file)
        return self.load_data_types_defs_from_dict(dts, dims)


    def load_data_types_defs_from_dict(self, dts, dims):
        """
        Takes data type and dimension definitions from a dictionary and loads
        them into the database.
        """
        self._load_dimensions_from_dict(dims)
        self._load_data_types_from_dict(dts, dims)
    
    
    def load_source_desc_from_json(self, filename):
        """
        Takes a json file describing an input format and imports the definitions
        into the database structure.
        """
        with file(filename, "r") as src_def_file:
            src_def = json.load(src_def_file)
        src_name = src_def["source_name"]
        src_id = self._get_or_create_data_source_id(src_name)
        for dt_db_name, dt_def in src_def['data_types'].iteritems():
            dt_db_obj = self.get_data_type(name=dt_db_name)
            self._sync_incoming_data_type(src_id, dt_db_obj, dt_def)
            for src_name, db_name in dt_def["dimensions"].iteritems():
                dim_db_obj = self.get_dimension(name=db_name)
                self._sync_incoming_dimension(src_id, dt_db_obj, dim_db_obj,
                                               src_name)


    def get_or_create_data_type_id(self, name, accumulated, parent=None):
        """
        Semi-private method for creation (or rather synchronization) of data
        types.
        """
        if name in self.defined_data_types:
            dt_id = self.defined_data_types[name]
        else:
            cursor = self.conn.cursor()
            cursor.execute("SELECT id FROM dscng_datatype WHERE name=%s",
                           (name,))
            dt_row = cursor.fetchone()
            if not dt_row:
                cursor.execute("""INSERT INTO dscng_datatype
                    (name, accumulated, meta, parent)
                    VALUES (%s,%s,'',%s) RETURNING id""",
                    (name, accumulated, parent))
                dt_row = cursor.fetchone()
                self.conn.commit()
            dt_id = dt_row[0]
            cursor.close()
            self.conn.commit()
            self.defined_data_types[name] = dt_id
        return dt_id

    # ----- Private methods ------
                
    def _load_data_types_from_dict(self, dts, dims):
        """
        Synchronizes a dictionary describing data types with the database;
        The dims dictionary is needed to provide information about accompanying
        dimensions.
        """
        # first sort them with virtual types at the end in order to ensure
        # existence of parents
        dts_sorted = [("parent" in dt_def, name, dt_def)
                      for name, dt_def in dts.iteritems()]
        dts_sorted.sort()
        for _is_virtual, name, dt_def in dts_sorted:
            parent = dt_def.get("parent")
            if parent:
                parent_obj = self.get_data_type(name=parent)
                parent_id = parent_obj.id
            else:
                parent_id = None
            accum = dt_def.get('accum', False)
            data_type_id = self.get_or_create_data_type_id(name, accum,
                                                           parent=parent_id)
            # localized name
            localized_names = dt_def.get("names")
            if not localized_names:
                logging.warn("No localized names for data type %s", name)
                implied_name = name.replace("_", " ")
                self.add_localized_data_type_name(data_type_id, "en",
                                                  implied_name)
            else:
                for lang, local_name in localized_names.iteritems():
                    self.add_localized_data_type_name(data_type_id, lang,
                                                      local_name)
            # dimensions
            dim_names = dt_def.get("dimensions", [])
            for i, dim_name in enumerate(dim_names):
                dim_obj = self.get_dimension(name=dim_name)
                # get the meta value
                meta = {}
                dim_def = dims.get(dim_name)
                if not dim_def:
                    raise ValueError("Unknown dimension in import %s", dim_name)
                values = dim_def.get("values")
                if values and type(values) == dict and name in values:
                    # there is a definition of values that is specific for this
                    # combination of data type and dimension
                    meta['values'] = values[name]
                meta_str = json.dumps(meta)
                self.connect_data_type_and_dimension(data_type_id, dim_obj, i,
                                                     meta=meta_str)
        

    def _load_dimensions_from_dict(self, dims):
        """
        Synchronizes a dict describing dimensions with the database
        """
        # dimensions first
        for name, dim_def in dims.iteritems():
            dim_type = dim_def["type"]
            meta = {}
            values = dim_def.get("values")
            if values:
                if type(values) == list:
                    # these are values for this type in every case
                    meta["values"] = values
                elif type(values) == dict:
                    # these values are valid only for specific parent data_types
                    # and only the value "null" is used for cases without 
                    # explicit values for data type
                    if "_null_" in values and values["_null_"] is not None:
                        meta["values"] = values["_null_"]
            meta_str = json.dumps(meta)
            dim_obj = self.get_or_create_dimension(name, dim_type, meta_str)
            default_remap = dim_def.get("default_remap")
            if default_remap:
                self.create_remap(dim_obj, default=default_remap)
        self.write_dynamic_remaps_to_db()
        
            
    def _get_or_create_data_source_id(self, name):
        """
        Get an id of a data source identified by a name. If it does not exist,
        create it.
        """
        cursor = self.conn.cursor()
        cursor.execute("SELECT id FROM dscng_data_source WHERE name=%s;",
                       (name,))
        db_row = cursor.fetchone()
        if not db_row:
            cursor.execute("""INSERT INTO dscng_data_source (name) 
                VALUES (%s) RETURNING id""", (name,))
            db_row = cursor.fetchone()
        cursor.close()
        self.conn.commit()
        return db_row[0]
    
        
    def _sync_incoming_data_type(self, src_id, dt_obj, dt_def):
        """
        For one source we can have only one definition for each data type;
        therefore we sync by overwriting potential old value
        """
        cursor = self.conn.cursor()
        cursor.execute("""UPDATE dscng_incoming_data_type
            SET name_in_data_source=%s, processing=%s
            WHERE data_source_id=%s AND data_type_id=%s;""",
            (dt_def["incoming_name"], dt_def["processing"], src_id, dt_obj.id))
        if cursor.rowcount == 0:
            # no update - the data was not there
            cursor.execute("""INSERT INTO dscng_incoming_data_type
                (data_source_id, data_type_id, name_in_data_source, processing)
                VALUES (%s,%s,%s,%s)""",
            (src_id, dt_obj.id, dt_def["incoming_name"], dt_def["processing"]))
        cursor.close()                
        self.conn.commit()


    def _sync_incoming_dimension(self, src_id, dt_obj, dim_obj, src_name):
        """
        Store description of incoming dimension into the database (or sync it
        if it is already present)
        """
        cursor = self.conn.cursor()
        cursor.execute("""UPDATE dscng_incoming_dimension
            SET dimension_id=%s
            WHERE data_source_id=%s AND data_type_id=%s 
                AND name_in_data_source=%s;""",
            (dim_obj.id, src_id, dt_obj.id, src_name))
        if cursor.rowcount == 0:
            # no update - the data was not there
            cursor.execute("""INSERT INTO dscng_incoming_dimension
                (data_source_id,data_type_id,name_in_data_source,dimension_id)
                VALUES (%s,%s,%s,%s)""",
            (src_id, dt_obj.id, src_name, dim_obj.id))
        cursor.close()                
        self.conn.commit()

    # ----- Class methods ------

    @classmethod
    def remap_value_to_int(cls, value, remap):
        """
        Based on the provided remap remaps the value to int.
        It takes care of special values, such as 'else' or 'none' which have
        stable default mappings.
        """
        if remap is not None:
            return remap[value]
        else:
            if value == "else":
                return -1
            elif value == "none":
                return -2
            else:
                return int(value)
            
        
        
        
class DynamicDict(dict):
    """dictionary that when asked about a non-existing key
    stores it and returns a new unique integer value for it

    Usage:

    >>> x = DynamicDict()
    >>> x
    {}
    >>> x['test']
    1
    >>> x['test2']
    2
    >>> x['test']
    1
    >>> x.get('unknown')
    3
    >>> 'xxx' in x
    False
    
    >>> y = DynamicDict({'aaa': 3, 'bbb': 7})
    >>> y.get('ccc')
    8

    """

    def __init__(self, *args, **kw):
        self._dirty = False
        dict.__init__(self, *args, **kw)
        if self:
            self._next_value = max(self.values()) + 1
            self._dirty = True
        else:
            self._next_value = 1

    def __getitem__(self, key):
        """overrides dict method; automatically creates a new value if the key
        is not present"""
        try:
            return dict.__getitem__(self, key)
        except KeyError:
            logging.debug("Adding new value for %s => %d", 
                          key, self._next_value)
            new_value = self._next_value
            self.__setitem__(key, new_value)
            return new_value

    def is_dirty(self):
        """is this dict dirty - did it receive any updates after last cleanup"""
        return self._dirty
    
    def cleanup(self):
        """removes the dirty attr"""
        self._dirty = False
        
    def update(self, other):
        """we override this to make sure everything works as expected"""
        other_d = dict(other)
        for key, value in other_d.iteritems():
            self[key] = value

    def get(self, key):
        """overrides dict method"""
        return self[key]

    def __setitem__(self, key, value):
        """overrides dict method"""
        # this value is not associated with this key
        if not (key in self and self[key] == value):
            if key is None:
                raise ValueError("DynamicDict key can't be None.")
            self._dirty = True
            dict.__setitem__(self, key, value)
            self._next_value = max(self._next_value, value + 1)
