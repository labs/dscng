#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains unittests for the import and update functions of DSCng.
It can be run from the command line like this
 > python import_tests.py
 
However, some parts assume that you have at least two test databases set up
which might not necessarily be the case. 
"""

# built-in imports
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import unittest
import logging
logging.basicConfig(level=logging.ERROR)
import time

# local imports
from dsc_storage.psql.tests import TestDBManager
from dsc_storage.psql.db_management import setup_db
from dsc_storage.psql.storage import StoragePSQL
from dsc_storage.psql.data_types import DataTypeManager
from dsc_storage.update.converter import RawBundleConverter
from devel_settings import TEST_DB_POOL
from tests_common import compare_lists

logging.getLogger('dsc_storage').setLevel(logging.ERROR)


class EqualDatabasesTestCase(unittest.TestCase):
    
    """
    Tests whether data in two databases are the same
    """
    
    preset = False #True
    test_db_names = ("1", "2")

    @classmethod
    def setUpClass(cls):
        """
        sets up the databases for tests - it is an expensive operation so it is
        done only once for all the tests in this test case
        """
        from dsc.dsc import settings
        username = settings.DATABASE_USER
        password = settings.DATABASE_PASSWORD
        host = settings.DATABASE_HOST
        cls.tdbm = TestDBManager(username=username, password=password,
                                 host=host)
        if len(TEST_DB_POOL) >= 2:
            cls.tdbs = [cls.tdbm.connect_to_test_db(TEST_DB_POOL[i])
                        for i, name in enumerate(cls.test_db_names)]
            cls.using_pool = True
        else:
            cls.tdbs = [cls.tdbm.create_test_db("dsc_test_%s" % name)
                        for i, name in enumerate(cls.test_db_names)]
            cls.using_pool = False
        if not cls.preset:
            # setup the tables
            for tdb in cls.tdbs:
                t = time.time()
                setup_db(tdb.conn, "data/db.sql")
                dtm = DataTypeManager(tdb.conn)
                dtm.load_data_type_defs_from_json("data/data_types.json",
                                               "data/dimensions.json")
                dtm.load_source_desc_from_json(
                                            "data/dsc_xml.data_source.json")
                logging.info("DB CREATE %f", time.time()-t)
        
    @classmethod    
    def tearDownClass(cls):
        """
        disposes of this test case. It is not done explicitly, so it probably
        does not get called at all, but who cares...
        """
        if not cls.using_pool:
            for tdb in cls.tdbs:
                cls.tdbm.destroy_test_db(tdb)
        cls.tdbm.close()
        cls.tdbm = None
    
    
    def setUp(self):
        """
        Sets up this TestCase instance - uses class set-up method to do the 
        expensive stuff only once
        """
        pass
    
    
    def _test_minute_data_equalness(self):
        """
        Fetch data from both databases and see if they match - for minute data
        """
        data = {}
        for db_num, name in enumerate(self.test_db_names):
            conn = self.tdbs[db_num].conn
            cursor = conn.cursor()
            storage = StoragePSQL(conn)
            data_types = storage.dtm.get_data_types()
            to_sort = [(dt.name, dt) for dt in data_types if not dt.accumulated]
            to_sort.sort()
            data[name] = []
            for _dt_name, data_type in to_sort:
                dims = storage.dtm.get_data_type_dimensions(data_type)
                dim_count = len(dims)
                val_cols = ",".join(["value%d"%(i+1) for i in range(dim_count)])
                if val_cols:
                    val_cols = ","+val_cols
                select = """SELECT server_id,time,minute,count%s
                    FROM dscng_data%dd 
                    WHERE data_type_id = %%s""" % (val_cols, dim_count+1)
                cursor.execute(select, (data_type.id,))
                remaps = [storage.get_db_to_user_value_remap(dim.id) for dim in dims]
                for row in cursor:
                    server_id, time, minute, count = row[:4]
                    new_vals = [remap.get(row[4+i], row[4+i])
                                for i,remap in enumerate(remaps)]
                    data[name].append((server_id, data_type.name, time,
                                       minute) + tuple(new_vals) + (count,))
                    data[name].sort()
            cursor.close()
            self._check_lists_equal(data)
      
        
    def _test_accum_data_equalness(self):
        """
        Fetch data from both databases and see if they match - for daily data
        """
        data = {}
        for db_num, name in enumerate(self.test_db_names):
            conn = self.tdbs[db_num].conn
            cursor = conn.cursor()
            storage = StoragePSQL(conn)
            data_types = storage.dtm.get_data_types()
            to_sort = [(dt.name, dt) for dt in data_types if dt.accumulated]
            to_sort.sort()
            data[name] = []
            for _dt_name, data_type in to_sort:
                dims = storage.dtm.get_data_type_dimensions(data_type)
                dim_count = len(dims)
                add_to_table_name = ""
                val_cols = []
                if "ip_addr" in [dim.name for dim in dims]:
                    add_to_table_name = "_ip"
                val_cols += ["value%d" % (i+1) for i in range(dim_count)]
                if val_cols:
                    val_cols = ","+",".join(val_cols)
                select = """SELECT server_id,time,count%s
                    FROM dscng_accumdata%dd%s 
                    WHERE data_type_id = %%s""" % (val_cols, dim_count+1,
                                                   add_to_table_name)
                cursor.execute(select, (data_type.id,))
                remaps = [storage.get_db_to_user_value_remap(dim.id) for dim in dims]
                for row in cursor:
                    server_id, time, count = row[:3]
                    
                    new_vals = []
                    start_col = 3
                    new_vals += [remap.get(row[start_col+i], row[start_col+i])
                                for i,remap in enumerate(remaps)]
                    data[name].append((server_id, data_type.name, time) +
                                       tuple(new_vals) + (count,))
                    data[name].sort()
            cursor.close()
        self._check_lists_equal(data)


    def _test_daily_stats_equalness(self):
        """
        Fetch data from both databases and see if they match - for dailystats
        (where skipped ips and counts are stored)
        """
        data = {}
        for db_num, name in enumerate(self.test_db_names):
            conn = self.tdbs[db_num].conn
            cursor = conn.cursor()
            storage = StoragePSQL(conn)
            data_types = storage.dtm.get_data_types()
            to_sort = [(dt.name, dt) for dt in data_types if dt.accumulated]
            to_sort.sort()
            data[name] = []
            for _dt_name, data_type in to_sort:
                dims = storage.dtm.get_data_type_dimensions(data_type)
                dim_count = len(dims)
                val_cols = []
                if "ip_addr" not in [dim.name for dim in dims]:
                    continue
                val_cols += ["value%d" % i for i in range(1, dim_count)]
                if val_cols:
                    val_cols = ","+",".join(val_cols)
                else:
                    val_cols = ""
                select = """SELECT server_id,time,skipped,skipped_count%s
                    FROM dscng_accumskipped%dd 
                    WHERE data_type_id = %%s""" % (val_cols, dim_count+1)
                cursor.execute(select, (data_type.id,))
                remaps = [storage.get_db_to_user_value_remap(dim.id)
                          for dim in dims[:-1]]
                for row in cursor:
                    server_id, time, skpd_ips, skpd_count = row[:4]
                    new_vals = []
                    start_col = 4
                    new_vals += [remap.get(row[start_col+i], row[start_col+i])
                                for i,remap in enumerate(remaps)]
                    data[name].append((server_id, data_type.name, time) +
                                      tuple(new_vals) + (skpd_ips, skpd_count))
                    data[name].sort()
            cursor.close()
        self._check_lists_equal(data)
       
    def _test_data_type_equalness(self):
        """
        Test if the same data types with the same dimensions are in the imported
        and updated database
        """
        data = {}
        for db_num, name in enumerate(self.test_db_names):
            conn = self.tdbs[db_num].conn
            cursor = conn.cursor()
            storage = StoragePSQL(conn)
            data_types = storage.dtm.get_data_types()
            to_sort = [(dt.name, dt) for dt in data_types if dt.accumulated]
            to_sort.sort()
            data[name] = []
            for _dt_name, data_type in to_sort:        
                rec = (data_type.name,)
                select = """SELECT
                    name,type FROM dscng_dimension
                    JOIN dscng_dimensiontodatatype
                    ON dimension_id = dscng_dimension.id 
                    WHERE data_type_id = %s ORDER BY position;"""
                cursor.execute(select, (data_type.id,))
                for row in cursor:
                    rec += row
                data[name].append(rec)
            cursor.close()
        self._check_lists_equal(data)

        
    def _check_lists_equal(self, data):
        """
        takes data in form of a dict of lists and compares the lists to each
        other, asserting that they should be equal
        """
        for name1 in data:
            for name2 in data:
                if name1 != name2:
                    list1 = data[name1]
                    list2 = data[name2]
                    if set(list1) != set(list2):
                        logging.warn("Database diff: %d records",
                                     len(set(list1)^set(list2))) 
                        for side, row in compare_lists(list1, list2):
                            if side == 1:
                                name = name1
                            else:
                                name = name2
                            logging.warn("%12s %s", name, row) 
                    self.assertEqual(set(list1), set(list2),
                            msg="Imported and updated data are not equal")

                        

class TestImportVsUpdate(EqualDatabasesTestCase):
    
    """
    Tests whether data imported from the DSC .dat files lead to the same
    database content as when the data are imported from .xml files using the
    update algorithm.
    """
    
    import_dir = "data/test_input/cloud-1/ns0/"
    update_dir = "data/test_input/cloud-1/ns0/done/20101020/dscdata/"
    
    preset = False #True
    test_db_names = ("imported", "updated")

    @classmethod
    def setUpClass(cls):
        """
        sets up the databases for tests - it is an expensive operation so it is
        done only once for all the tests in this test case
        """
        EqualDatabasesTestCase.setUpClass()
        if not cls.preset:
            t = time.time()
            # import data into the first one
            from import_dsc_dat_files import import_dirs
            conn_params = cls.tdbm.get_connection_params(
                                                database=cls.tdbs[0].name)
            import_dirs(conn_params, [cls.import_dir], quiet=True)
            logging.info("IMPORT %f", time.time()-t)
            # update data in the second
            t = time.time()
            from update_from_xml import DscXmlImporter
            conn_params = cls.tdbm.get_connection_params(
                                                database=cls.tdbs[1].name)
            storage = StoragePSQL(conn_params)
            converter = RawBundleConverter(storage,
                                           storage.insert_update_bundle)
            importer = DscXmlImporter(converter.process_raw_update_bundle)
            for fname in os.listdir(cls.update_dir):
                fullname = os.path.join(cls.update_dir, fname)
                importer.import_xml_file(fullname)
            storage.process_all_cache_update_requests()
            logging.info("UPDATE %f", time.time()-t)
    
    # we make parent test methods non-private here to activate them
    def test_accum_data_equalness(self):
        self._test_accum_data_equalness()
        
    def test_daily_stats_equalness(self):
        self._test_daily_stats_equalness()
        
    def test_data_type_equalness(self):
        self._test_data_type_equalness()
        
    def test_minute_data_equalness(self):
        t = time.time()
        self._test_minute_data_equalness()
        logging.info("MINUTE EQ %f", time.time()-t)
          


class TestDoubleImport(EqualDatabasesTestCase):
    
    """
    Tests whether data imported from the DSC .dat files remain the same
    when re-import is run
    """
    
    import_dir = "data/test_input/cloud-1/ns0/"
    
    preset = False #True
    test_db_names = ("imported", "reimported")

    @classmethod
    def setUpClass(cls):
        """
        sets up the databases for tests - it is an expensive operation so it is
        done only once for all the tests in this test case
        """
        EqualDatabasesTestCase.setUpClass()
        if not cls.preset:
            # import data into the first one
            from import_dsc_dat_files import import_dirs
            conn_params = cls.tdbm.get_connection_params(
                                                database=cls.tdbs[0].name)
            import_dirs(conn_params, [cls.import_dir], quiet=True)
            # import and reimport data in the second
            conn_params = cls.tdbm.get_connection_params(
                                                database=cls.tdbs[1].name)
            import_dirs(conn_params, [cls.import_dir], quiet=True)
            import_dirs(conn_params, [cls.import_dir], quiet=True)
    
    # we make parent test methods non-private here to activate them
    def test_accum_data_equalness(self):
        self._test_accum_data_equalness()
        
    def test_daily_stats_equalness(self):
        self._test_daily_stats_equalness()
        
    def test_data_type_equalness(self):
        self._test_data_type_equalness()
        
    def test_minute_data_equalness(self):
        self._test_minute_data_equalness()

        
if __name__ == "__main__":
    unittest.main()