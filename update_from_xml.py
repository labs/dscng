#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# build in modules
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import sys
import xml.sax
import logging
from datetime import datetime
from optparse import OptionParser
from base64 import b64decode
import string

# local imports
from dsc_storage.util.input import FileReader
from dsc_storage.update.config import Defaults
from dsc_storage.update.converter import RawDataRecord, RawUpdateBundle,\
                                         RawBundleConverter
from dsc_storage.update.security import \
    pyro_set_client_authentication_from_file, AuthenticationFileError

class UnknownDirectorySchemaError(Exception):
    pass


class DscSaxHandler(xml.sax.ContentHandler):
    """SAX Handler capable of extracting data from DSC transport XML files"""

    PRINTABLE_CHARS = set(string.letters + string.digits + string.punctuation)
    PRINTABLE_CHARS.add(" ")

    def __init__(self, data_type_callback, document_end_callback):
        """
        data_type_callback is a function that this handler pushes data to once
        it has parsed a whole data type
        """
        xml.sax.ContentHandler.__init__(self)
        self.data_type_callback = data_type_callback
        self.document_end_callback = document_end_callback
        self._record = None
        self._above = []
        self._val1 = None
    
    @classmethod
    def extract_value_from_attrs(cls, attrs):
        val = attrs['val']
        if 'base64' in attrs and attrs['base64'] == "1":
            val = b64decode(val)
            try:
                val = val.decode('utf-8')
            except UnicodeDecodeError:
                pass
            # we need to have a look inside the string
            out = []
            recoded = False
            for char in val:
                if char not in cls.PRINTABLE_CHARS:
                    num = ord(char)
                    out.append("\\%x" % num)
                    recoded = True
                else:
                    out.append(char)
            val = "".join(out)
            if recoded:
                logging.info("Recoding suspicious base64 value '%s' as '%s'",
                             attrs['val'], val)
        return val
    
    def startElement(self, name, attrs):
        if name == 'array':
            # this is a new data type
            start_time = datetime.utcfromtimestamp(int(attrs['start_time']))
            self._record = RawDataRecord(data_type_name=attrs['name'],
                                         start_time=start_time)
            self._vals = []
        elif name == 'dimension':
            number = int(attrs['number'])
            dim_type = attrs['type']
            if number <= len(self._record.dimensions):
                raise ValueError("Unexpected dimension number %d" % number)
            else:
                self._record.dimensions.append(dim_type)
        elif len(self._above) >= 1 and self._above[-1] == 'data':
            # it is something right below data
            self._val1 = self.extract_value_from_attrs(attrs)
        elif len(self._above) >= 2 and self._above[-2] == 'data':
            # this is the lowest level
            val = self.extract_value_from_attrs(attrs)
            count = int(attrs['count'])
            if self._val1 == 'ALL':
                self._record.data.append([val, count])
            else:
                self._record.data.append([self._val1, val, count])
        self._above.append(name)
            
    def endElement(self, name):
        if name == 'array':
            self.data_type_callback(self._record)
            self._record = None
        self._above.pop(-1)
        
    def endDocument(self):
        self.document_end_callback()
    

class DscXmlImporter(object):
    
    DATA_SOURCE_NAME = "dsc_xml"
    
    def __init__(self, update_callback):
        self._timerecord_cache = {}
        # state variables
        self._parent_name, self._server_name = None, None   # current server
        self._current_record_time = None  # what is the time of current update
        self._current_updated = None # did current file result in actual update?
        self._current_bundle = None
        # startup stuff
        self.update_callback = update_callback
    
   
    def import_xml_file(self, filename):
        self._records = []
        self._parent_name, self._server_name = self._parse_filename(filename)
        self._current_bundle = RawUpdateBundle(self.DATA_SOURCE_NAME,
                                               server=(self._parent_name,
                                                       self._server_name))
                                
        handler = DscSaxHandler(self.process_data, self.finish_data_file_import)
        data = FileReader.get_file_content(filename)
        xml.sax.parseString(data, handler)
        # process pending cache update requests
        self._current_bundle.timepoint = self._current_record_time
        self.update_callback(self._current_bundle)


    def _parse_filename(self, filename):
        parts = os.path.abspath(filename).split("/")
        parts = parts[:-1]
        if len(parts) >= 5 and parts[-1] == "dscdata" and "done" == parts[-3]:
            # we assume this is a location with already processed data
            # something like /usr/local/dsc/data/dns-s/ns0/done/20101020/dscdata
            server_name = parts[-4]
            parent_name = parts[-5]
        elif len(parts) >= 4 and parts[-2] == "incoming":
            # this is a path to incoming dsc data, similar to 
            # /usr/local/dsc/data/dns-s/ns0/incoming/2010-10-14
            server_name = parts[-3]
            parent_name = parts[-4]
        else:
            raise UnknownDirectorySchemaError(filename)
        return parent_name, server_name

    def finish_data_file_import(self):
        # record that we have already seen data for this time
        pass

    def process_data(self, record):
        """
        This method will be called once the handler has finished one data type.
        """
        self._current_bundle.records.append(record)
        self._current_record_time = record.start_time


def main():
    # parse options
    opsr = OptionParser(usage="python %prog [options] xml_file(s)")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-d", "--direct-db-access", action="store_true",
                    dest="direct_db_access", default=False,
                    help="Do not use an update daemon, but use direct access "
                        "to the database")
    opsr.add_option("-p", "--port", action="store", type="int",
                    dest="port", default=Defaults.port,
                    help="Port of the server to connect to")
    opsr.add_option("-n", "--hostname", action="store", type="string",
                    dest="hostname", default=Defaults.hostname,
                    help="Server hostname (address) to connect to")
    opsr.add_option("-f", "--auth-file", action="store", type="string",
                    dest="auth_file", default="",
                    help="Authentication file - where authentication data "
                         "should be read from when the client connects to the "
                         "server. Empty value means no client authentication")
    (options, args) = opsr.parse_args()
    
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if options.direct_db_access:
        os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
        try:
            from dsc.dsc import settings
        except ImportError as exc:
            logging.error("Could not import DSC settings module - cannot use "
                          "direct database access")
            logging.error("Error content: %s", exc)
        from dsc_storage.psql.connection import \
                            create_connection_manager_from_settings
        from dsc_storage.psql import db_maintenance
        from dsc_storage.psql.storage import StoragePSQL
        conn = create_connection_manager_from_settings(settings)
        # do checks on db in case some previous update or import left it in
        # inconsistent state
        db_maintenance.fix_db_health(conn)
        # continue with update
        storage = StoragePSQL(conn)
        logging.info("Using (slower) direct database access")
        converter = RawBundleConverter(storage,
                                       storage.insert_update_bundle)
        xml_importer = DscXmlImporter(converter.process_raw_update_bundle)
    else:
        import Pyro.core
        rpc_uri = 'PYROLOC://{hostname}:{port}/dsc_updater'.format(
                                                    hostname=options.hostname,
                                                    port=options.port)
        logging.debug("Connecting to %s", rpc_uri)
        try:
            updater = Pyro.core.getProxyForURI(rpc_uri)
            if options.auth_file:
                pyro_set_client_authentication_from_file(updater,
                                                         options.auth_file)
            assert updater.test() == True
            xml_importer = DscXmlImporter(updater.process_update_bundle)
        except Exception as exc:
            # we cannot connect to the RPC server
            logging.error("Could not connect to the update daemon.")
            logging.error("Error content: %s", exc)
            if isinstance(exc, Pyro.core.ConnectionDeniedError):
                if options.auth_file:
                    logging.info("Connection was denied. The supplied "
                                 "authentication file is probably not correct "
                                 "for this connection.")
                else:
                    logging.info("Connection was denied. You probably need to "
                                 "supply an authentication file using the -f "
                                 "command line argument.")
                    opsr.print_help()
            else:
                logging.info("You can try the (slower) direct database access "
                             "by providing the -d command line option")
                opsr.print_help()
            sys.exit(103)
        else:
            logging.info("Connected to update daemon at %s", rpc_uri)
        
    if not args:
        print >> sys.stderr, "Give me at least one DSC XML file"
    else:
        for fname in args:
            logging.debug("Processing file: %s", fname)
            try:
                xml_importer.import_xml_file(fname)
            except UnknownDirectorySchemaError as e:
                print >> sys.stderr, "I do not know how to extract server name\
 from the path '{0}'".format(fname)
            except Exception as exc:
                print >> sys.stderr, \
                        "An error occured when importing '{0}'".format(fname)
                print >> sys.stderr, exc
                break
        if options.direct_db_access:
            storage.process_all_cache_update_requests()

if __name__ == "__main__":
    main()
