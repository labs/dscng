#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from time import time
from datetime import timedelta
import sys


class TimingProfiler(object):
    '''
    This profiler tracks calls of parts of code marked by calls
    to start and end with the same key
    
    Usage:
    
    >>> tp = TimingProfiler()
    >>> tp.start("code 1")
    >>> tp.end().start("code 2")
    >>> tp.end()
    >>> tp.print_stats()
    ...
    
    '''

    def __init__(self):
        self._timers = {}
        self._time_stats = {}
        self._count_stats = {}
        self._last_keys = []
        self._start_time = time()
        
    def start(self, key_part):
        self._last_keys.append(key_part)
        key = tuple(self._last_keys)
        if key not in self._timers:
            self._timers[key] = time()
        return self
        
    def end(self):
        key = tuple(self._last_keys)
        if key in self._timers:
            timing = time() - self._timers[key]
            self._time_stats[key] = self._time_stats.get(key, 0) + timing
            self._count_stats[key] = self._count_stats.get(key, 0) + 1
            del self._timers[key]
        self._last_keys.pop(-1)
        return self
            
    def end_all(self):
        for _key in list(self._last_keys):
            self.end()
        return self
    
    def print_stats(self, where=sys.stdout):
        tmplt = "%-30s %6d calls %8.2f s (%4.1f%%; %4.1f%%; %4.1f%%) %6.3f spc"
        def print_level(parent):
            level = len(parent)
            if level == 0:
                parent_time = total_time
            else:
                parent_time = self._time_stats[parent]
            for key, timing in sorted(self._time_stats.items()):
                count = self._count_stats[key]
                if len(key) == level+1 and key[:level] == parent:
                    data = (level*"  "+key[-1], count, timing,
                            100*timing/total_time, 100*timing/wall_time,
                            100*timing/parent_time, timing/count)
                    print >> where, tmplt % data
                    print_level(key)
                    
        # compute and print summary data
        total_time = 0
        for key, val in self._time_stats.iteritems():
            if len(key) == 1:
                total_time += val
        total_td = timedelta(seconds=total_time)
        wall_time = time() - self._start_time
        wall_td = timedelta(seconds=wall_time)
        print >> where, "----------- Timing stats ----------------"
        print >> where, "  Total tracked time: %8.2f s (%s)" % \
                                                (total_time, total_td)
        print >> where, "  Wall clock time:    %8.2f s (%s)" % \
                                                (wall_time, wall_td)
        # print the table
        print >> where, "%54s (TOTAL; WALL ;PARENT)" % ""
        print_level(())
        # print untracked time
        print >> where
        timing = wall_time-total_time
        data = ("*untracked*", timing, 100*timing/wall_time)
        print >> where, "%-30s              %8.2f s        (%4.1f%%)" % data
        print >> where, "-----------------------------------------"
        where.flush()
        
           
if __name__ == "__main__":
    tp = TimingProfiler()
    tp.start("A")
    tp.start("B").start("C")
    tp.end_all()
    tp.print_stats()