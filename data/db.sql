-- Parse::SQL::Dia      version 0.20                              
-- Documentation        http://search.cpan.org/dist/Parse-Dia-SQL/
-- Environment          Perl 5.014002, /usr/bin/perl              
-- Architecture         x86_64-linux-gnu-thread-multi             
-- Target Database      postgres                                  
-- Input file           db_structure_uml.dia                      
-- Generated at         Tue Apr 23 16:06:57 2013                  
-- Typemap for postgres not found in input file                   

-- get_constraints_drop 
alter table dscng_server_group drop constraint dscng_server_group_fk_Parent_id ;
alter table dscng_server drop constraint dscng_server_fk_Group_id ;
alter table dscng_servertimespan drop constraint dscng_servertimespan_fk_Server_id ;
alter table dscng_cache_request drop constraint dscng_cache_request_fk_Server_id ;
alter table dscng_timerecord_c drop constraint dscng_timerecord_c_fk_Server_id ;
alter table dscng_timerecord drop constraint dscng_timerecord_fk_Server_id ;
alter table dscng_serverupdates drop constraint dscng_serverupdates_fk_Server_id ;
alter table dscng_timerecord_accum drop constraint dscng_timerecord_accum_fk_Server_id ;
alter table dscng_dataarray1d_c drop constraint dscng_dataarray1d_c_fk_Time_record_id ;
alter table dscng_dataarray2d_c drop constraint dscng_dataarray2d_c_fk_Time_record_id ;
alter table dscng_dataarray3d_c drop constraint dscng_dataarray3d_c_fk_Time_record_id ;
alter table dscng_dataarray1d drop constraint dscng_dataarray1d_fk_Time_record_id ;
alter table dscng_dataarray2d drop constraint dscng_dataarray2d_fk_Time_record_id ;
alter table dscng_dataarray3d drop constraint dscng_dataarray3d_fk_Time_record_id ;
alter table dscng_serverupdates drop constraint dscng_serverupdates_fk_Data_type_id ;
alter table dscng_datapoint3d_accum_ip drop constraint dscng_datapoint3d_accum_ip_fk_Time_record_id ;
alter table dscng_datapoint3d_accum drop constraint dscng_datapoint3d_accum_fk_Time_record_id ;
alter table dscng_datapoint2d_accum_ip drop constraint dscng_datapoint2d_accum_ip_fk_Time_record_id ;
alter table dscng_datapoint2d_accum drop constraint dscng_datapoint2d_accum_fk_Time_record_id ;
alter table dscng_dailystats2d drop constraint dscng_dailystats2d_fk_Time_record_id ;
alter table dscng_dailystats3d drop constraint dscng_dailystats3d_fk_Time_record_id ;
alter table dscng_datapoint3d_accum_ip drop constraint dscng_datapoint3d_accum_ip_fk_Data_type_id ;
alter table dscng_datapoint3d_accum drop constraint dscng_datapoint3d_accum_fk_Data_type_id ;
alter table dscng_datapoint2d_accum_ip drop constraint dscng_datapoint2d_accum_ip_fk_Data_type_id ;
alter table dscng_datapoint2d_accum drop constraint dscng_datapoint2d_accum_fk_Data_type_id ;
alter table dscng_dailystats2d drop constraint dscng_dailystats2d_fk_Data_type_id ;
alter table dscng_dailystats3d drop constraint dscng_dailystats3d_fk_Data_type_id ;
alter table dscng_dimensiontodatatype drop constraint dscng_dimensiontodatatype_fk_Data_type_id ;
alter table dscng_dimensiontodatatype drop constraint dscng_dimensiontodatatype_fk_Dimension_id ;
alter table dscng_str_to_int_remap drop constraint dscng_str_to_int_remap_fk_Dimension_id ;
alter table dscng_int_to_str_remap drop constraint dscng_int_to_str_remap_fk_Dimension_id ;
alter table dscng_incoming_dimension drop constraint dscng_incoming_dimension_fk_Dimension_id ;
alter table dscng_datatype drop constraint dscng_datatype_fk_Parent ;
alter table dscng_incoming_dimension drop constraint dscng_incoming_dimension_fk_Data_type_id ;
alter table dscng_dataarray2d_c drop constraint dscng_dataarray2d_c_fk_Data_type_id ;
alter table dscng_dataarray1d_c drop constraint dscng_dataarray1d_c_fk_Data_type_id ;
alter table dscng_dataarray3d_c drop constraint dscng_dataarray3d_c_fk_Data_type_id ;
alter table dscng_dataarray1d drop constraint dscng_dataarray1d_fk_Data_type_id ;
alter table dscng_dataarray3d drop constraint dscng_dataarray3d_fk_Data_type_id ;
alter table dscng_dataarray2d drop constraint dscng_dataarray2d_fk_Data_type_id ;
alter table dscng_data_type_name drop constraint dscng_data_type_name_fk_Data_type_id ;
alter table dscng_incoming_data_type drop constraint dscng_incoming_data_type_fk_Data_source_id ;
alter table dscng_incoming_data_type drop constraint dscng_incoming_data_type_fk_Data_type_id ;
alter table dscng_incoming_dimension drop constraint dscng_incoming_dimension_fk_Data_source_id ;
drop index idx_timerecord_server_time cascade;
drop index idx_timerecord_c_server_time cascade;
drop index idx_timerecord_c_timespan cascade;
drop index idx_timerecord_accum_server_time cascade;

-- get_permissions_drop 

-- get_view_drop

-- get_schema_drop
drop table dscng_server;
drop table dscng_dataarray2d;
drop table dscng_dataarray1d;
drop table dscng_dataarray3d;
drop table dscng_datatype;
drop table dscng_dimensiontodatatype;
drop table dscng_dimension;
drop table dscng_timerecord;
drop table dscng_str_to_int_remap;
drop table dscng_dataarray2d_c;
drop table dscng_dataarray1d_c;
drop table dscng_dataarray3d_c;
drop table dscng_timerecord_c;
drop table dscng_int_to_str_remap;
drop table dscng_timerecord_accum;
drop table dscng_datapoint3d_accum;
drop table dscng_datapoint3d_accum_ip;
drop table dscng_datapoint2d_accum_ip;
drop table dscng_datapoint2d_accum;
drop table dscng_servertimespan;
drop table dscng_serverupdates;
drop table dscng_dailystats2d;
drop table dscng_dailystats3d;
drop table dscng_data_source;
drop table dscng_incoming_data_type;
drop table dscng_incoming_dimension;
drop table dscng_data_type_name;
drop table dscng_metadata;
drop table dscng_cache_request;
drop table dscng_server_group;

-- get_smallpackage_pre_sql 
DROP TYPE intrange CASCADE;
CREATE TYPE intrange AS (avg int, min int, max int);
CREATE TYPE processing_method AS ENUM ('as-is','reduce-trace','reduce-count','accum');
-- get_schema_create
create table dscng_server (
   id       serial      not null            ,
   name     varchar(64)                     ,
   group_id int                             ,
   meta     text         default '' NOT NULL,
   constraint pk_dscng_server primary key (id)
)   ;
create table dscng_dataarray2d (
   time_record_id int   not null,
   data_type_id   int   not null,
   value1         int   not null,
   counts         int[] NOT NULL,
   constraint pk_dscng_dataarray2d primary key (time_record_id,data_type_id,value1)
)   ;
create table dscng_dataarray1d (
   time_record_id int   not null,
   data_type_id   int   not null,
   counts         int[] NOT NULL,
   constraint pk_dscng_dataarray1d primary key (time_record_id,data_type_id)
)   ;
create table dscng_dataarray3d (
   time_record_id int   not null,
   data_type_id   int   not null,
   value1         int   not null,
   value2         int   not null,
   counts         int[] NOT NULL,
   constraint pk_dscng_dataarray3d primary key (time_record_id,data_type_id,value1,value2)
)   ;
create table dscng_datatype (
   id          serial      not null,
   name        varchar(64) NOT NULL,
   accumulated BOOLEAN     NOT NULL,
   parent      int         NULL    ,
   meta        TEXT        NOT NULL,--  This is where metadata in JSON format could be stored
   constraint pk_dscng_datatype primary key (id)
)   ;
create table dscng_dimensiontodatatype (
   data_type_id int  not null,
   dimension_id int  not null,
   position     int  not null,
   meta         TEXT NOT NULL,
   constraint pk_dscng_dimensiontodatatype primary key (data_type_id,dimension_id,position)
)   ;
create table dscng_dimension (
   id   serial      not null,
   name varchar(64)         ,
   type varchar(64) NOT NULL,
   meta TEXT        NOT NULL,--  This is where metadata in JSON format will be stored
   constraint pk_dscng_dimension primary key (id)
)   ;
create table dscng_timerecord (
   id        serial    not null,
   server_id int       NOT NULL,
   time      timestamp NOT NULL,
   constraint pk_dscng_timerecord primary key (id)
)   ;
create table dscng_str_to_int_remap (
   dimension_id int          not null,
   original     varchar(255) not null,
   remap        int          NOT NULL,
   constraint pk_dscng_str_to_int_remap primary key (dimension_id,original)
)   ;
create table dscng_dataarray2d_c (
   time_record_id int   not null,
   data_type_id   int   not null,
   value1         int   not null,
   avg_counts     int[] NOT NULL,
   min_counts     int[] NOT NULL,
   max_counts     int[] NOT NULL,
   constraint pk_dscng_dataarray2d_c primary key (time_record_id,data_type_id,value1)
)   ;
create table dscng_dataarray1d_c (
   time_record_id int   not null,
   data_type_id   int   not null,
   avg_counts     int[] NOT NULL,
   min_counts     int[] NOT NULL,
   max_counts     int[] NOT NULL,
   constraint pk_dscng_dataarray1d_c primary key (time_record_id,data_type_id)
)   ;
create table dscng_dataarray3d_c (
   time_record_id int   not null,
   data_type_id   int   not null,
   value1         int   not null,
   value2         int   not null,
   avg_counts     int[] NOT NULL,
   min_counts     int[] NOT NULL,
   max_counts     int[] NOT NULL,
   constraint pk_dscng_dataarray3d_c primary key (time_record_id,data_type_id,value1,value2)
)   ;
create table dscng_timerecord_c (
   id        serial    not null,
   server_id int       NOT NULL,
   time      timestamp NOT NULL,
   timespan  int       NOT NULL,
   constraint pk_dscng_timerecord_c primary key (id)
)   ;
create table dscng_int_to_str_remap (
   dimension_id int          not null,
   original     int          not null,
   remap        varchar(255) NOT NULL,
   constraint pk_dscng_int_to_str_remap primary key (dimension_id,original)
)   ;
create table dscng_timerecord_accum (
   id        serial    not null,
   server_id int       NOT NULL,
   time      timestamp NOT NULL,
   constraint pk_dscng_timerecord_accum primary key (id)
)   ;
create table dscng_datapoint3d_accum (
   time_record_id int    not null,
   data_type_id   int    not null,
   value1         int    not null,
   value2         int    not null,
   count          bigint NOT NULL,
   constraint pk_dscng_datapoint3d_accum primary key (time_record_id,data_type_id,value1,value2)
)   ;
create table dscng_datapoint3d_accum_ip (
   time_record_id int    not null,
   data_type_id   int    not null,
   value1         int    not null,
   value2         inet   not null,
   count          bigint NOT NULL,
   constraint pk_dscng_datapoint3d_accum_ip primary key (time_record_id,data_type_id,value1,value2)
)   ;
create table dscng_datapoint2d_accum_ip (
   time_record_id int    not null,
   data_type_id   int    not null,
   value1         inet   not null,
   count          bigint NOT NULL,
   constraint pk_dscng_datapoint2d_accum_ip primary key (time_record_id,data_type_id,value1)
)   ;
create table dscng_datapoint2d_accum (
   time_record_id int    not null,
   data_type_id   int    not null,
   value1         int    not null,
   count          bigint NOT NULL,
   constraint pk_dscng_datapoint2d_accum primary key (time_record_id,data_type_id,value1)
)   ;
create table dscng_servertimespan (
   server_id  int       not null,
   start_time timestamp NOT NULL,
   end_time   timestamp NOT NULL,
   constraint pk_dscng_servertimespan primary key (server_id)
)   ;
create table dscng_serverupdates (
   server_id     int       not null,
   data_type_id  int       not null,
   day           timestamp not null,
   minutes       bit(1440)         ,
   dat_file_sha1 char(40)          ,
   constraint pk_dscng_serverupdates primary key (server_id,data_type_id,day)
)   ;
create table dscng_dailystats2d (
   time_record_id int    not null,
   data_type_id   int    not null,
   skipped        int    not null,
   skipped_count  bigint not null,
   constraint pk_dscng_dailystats2d primary key (time_record_id,data_type_id)
)   ;
create table dscng_dailystats3d (
   time_record_id int    not null,
   data_type_id   int    not null,
   value1         int    not null,
   skipped        int    not null,
   skipped_count  bigint NOT NULL,
   constraint pk_dscng_dailystats3d primary key (time_record_id,data_type_id,value1)
)   ;
create table dscng_data_source (
   id   serial      not null,
   name varchar(64) NOT NULL,
   constraint pk_dscng_data_source primary key (id)
)   ;
create table dscng_incoming_data_type (
   data_source_id      int               not null,
   data_type_id        int               not null,
   name_in_data_source varchar(64)       NOT NULL,
   processing          processing_method NOT NULL,
   constraint pk_dscng_incoming_data_type primary key (data_source_id,data_type_id)
)   ;
create table dscng_incoming_dimension (
   data_source_id      int         not null,
   data_type_id        int         not null,
   name_in_data_source varchar(64) not null,
   dimension_id        int         NOT NULL,
   constraint pk_dscng_incoming_dimension primary key (data_source_id,data_type_id,name_in_data_source)
)   ;
create table dscng_data_type_name (
   data_type_id int          not null,
   lang         varchar(8)   not null,
   local_name   varchar(128) NOT NULL,
   constraint pk_dscng_data_type_name primary key (data_type_id,lang)
)   ;
create table dscng_metadata (
   section varchar(64) not null,
   name    varchar(64) not null,
   value   text        NOT NULL,
   constraint pk_dscng_metadata primary key (section,name)
)   ;
create table dscng_cache_request (
   server_id  int       not null,
   timespan   int       not null,
   start_time timestamp not null,
   due_time   timestamp NOT NULL,
   constraint pk_dscng_cache_request primary key (server_id,timespan,start_time)
)   ;
create table dscng_server_group (
   id        serial      not null            ,
   name      varchar(64)                     ,
   parent_id int                             ,
   meta      text         default '' NOT NULL,
   constraint pk_dscng_server_group primary key (id)
)   ;

-- get_view_create

-- get_permissions_create

-- get_inserts

-- get_smallpackage_post_sql

-- get_associations_create
create index idx_timerecord_server_time on dscng_timerecord (server_id,time) ;
create index idx_timerecord_c_server_time on dscng_timerecord_c (server_id,time) ;
create index idx_timerecord_c_timespan on dscng_timerecord_c (timespan) ;
create index idx_timerecord_accum_server_time on dscng_timerecord_accum (server_id,time) ;
alter table dscng_server_group add constraint dscng_server_group_fk_Parent_id 
    foreign key (parent_id)
    references dscng_server_group (id) on delete cascade;
alter table dscng_server add constraint dscng_server_fk_Group_id 
    foreign key (group_id)
    references dscng_server_group (id) on delete set NULL;
alter table dscng_servertimespan add constraint dscng_servertimespan_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_cache_request add constraint dscng_cache_request_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_timerecord_c add constraint dscng_timerecord_c_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_timerecord add constraint dscng_timerecord_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_serverupdates add constraint dscng_serverupdates_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_timerecord_accum add constraint dscng_timerecord_accum_fk_Server_id 
    foreign key (server_id)
    references dscng_server (id) on delete cascade;
alter table dscng_dataarray1d_c add constraint dscng_dataarray1d_c_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_c (id) on delete cascade;
alter table dscng_dataarray2d_c add constraint dscng_dataarray2d_c_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_c (id) on delete cascade;
alter table dscng_dataarray3d_c add constraint dscng_dataarray3d_c_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_c (id) on delete cascade;
alter table dscng_dataarray1d add constraint dscng_dataarray1d_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord (id) on delete cascade;
alter table dscng_dataarray2d add constraint dscng_dataarray2d_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord (id) on delete cascade;
alter table dscng_dataarray3d add constraint dscng_dataarray3d_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord (id) on delete cascade;
alter table dscng_serverupdates add constraint dscng_serverupdates_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_datapoint3d_accum_ip add constraint dscng_datapoint3d_accum_ip_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_datapoint3d_accum add constraint dscng_datapoint3d_accum_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_datapoint2d_accum_ip add constraint dscng_datapoint2d_accum_ip_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_datapoint2d_accum add constraint dscng_datapoint2d_accum_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_dailystats2d add constraint dscng_dailystats2d_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_dailystats3d add constraint dscng_dailystats3d_fk_Time_record_id 
    foreign key (time_record_id)
    references dscng_timerecord_accum (id) on delete cascade;
alter table dscng_datapoint3d_accum_ip add constraint dscng_datapoint3d_accum_ip_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_datapoint3d_accum add constraint dscng_datapoint3d_accum_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_datapoint2d_accum_ip add constraint dscng_datapoint2d_accum_ip_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_datapoint2d_accum add constraint dscng_datapoint2d_accum_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dailystats2d add constraint dscng_dailystats2d_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dailystats3d add constraint dscng_dailystats3d_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dimensiontodatatype add constraint dscng_dimensiontodatatype_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dimensiontodatatype add constraint dscng_dimensiontodatatype_fk_Dimension_id 
    foreign key (dimension_id)
    references dscng_dimension (id) on delete cascade;
alter table dscng_str_to_int_remap add constraint dscng_str_to_int_remap_fk_Dimension_id 
    foreign key (dimension_id)
    references dscng_dimension (id) on delete cascade;
alter table dscng_int_to_str_remap add constraint dscng_int_to_str_remap_fk_Dimension_id 
    foreign key (dimension_id)
    references dscng_dimension (id) on delete cascade;
alter table dscng_incoming_dimension add constraint dscng_incoming_dimension_fk_Dimension_id 
    foreign key (dimension_id)
    references dscng_dimension (id) on delete cascade;
alter table dscng_datatype add constraint dscng_datatype_fk_Parent 
    foreign key (parent)
    references dscng_datatype (id) on delete cascade;
alter table dscng_incoming_dimension add constraint dscng_incoming_dimension_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray2d_c add constraint dscng_dataarray2d_c_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray1d_c add constraint dscng_dataarray1d_c_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray3d_c add constraint dscng_dataarray3d_c_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray1d add constraint dscng_dataarray1d_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray3d add constraint dscng_dataarray3d_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_dataarray2d add constraint dscng_dataarray2d_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_data_type_name add constraint dscng_data_type_name_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_incoming_data_type add constraint dscng_incoming_data_type_fk_Data_source_id 
    foreign key (data_source_id)
    references dscng_data_source (id) on delete cascade;
alter table dscng_incoming_data_type add constraint dscng_incoming_data_type_fk_Data_type_id 
    foreign key (data_type_id)
    references dscng_datatype (id) on delete cascade;
alter table dscng_incoming_dimension add constraint dscng_incoming_dimension_fk_Data_source_id 
    foreign key (data_source_id)
    references dscng_data_source (id) on delete cascade;
