#!/bin/sh
#
# Post-commit hook to update version number in the dscng_version module.

version=`git describe --tags`
version_file=dscng_version.py

sed "s/\$\$VERSION\$\\$/$version/" dscng_version.py.in > dsc/dsc/$version_file
echo "Version number in dscng_version.py updated to $version.\n"