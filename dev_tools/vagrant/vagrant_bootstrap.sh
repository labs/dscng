#!/usr/bin/env bash

# whether to create an Apache vhost and virtual environment
create_vhost=true
# set to false if you do not want to init DSCng and Django tables
init_db=true

# fix locale errors
echo -e "\nexport LANGUAGE=en_US.UTF-8\nexport LANG=en_US.UTF-8\nexport LC_ALL=en_US.UTF-8" >> /etc/bash.bashrc
locale-gen en_US.UTF-8
export LC_ALL=en_US.UTF-8
dpkg-reconfigure locales

# set timezone to CET
mv /etc/localtime /etc/localtime.bak
ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime

# install DSC requirements & some development stuff
apt-get update
apt-get install -y postgresql apache2 libapache2-mod-wsgi python-psycopg2 python-geoip memcached python-memcache python-pip mc

# setup database

# create user, same as:
#     sudo -u postgres createuser -P dsc
# with password = pass
sudo -u postgres psql -c "CREATE ROLE dsc PASSWORD 'md529ab8c246640632cb7ebcc0e91eac627' NOSUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN;"

sudo -u postgres createdb -O dsc dsc

if $create_vhost ; then
    apt-get install -y libgeoip-dev python-dev postgresql-server-dev-9.3 python-virtualenv
    mkdir /opt/virtualenv/
    cd /opt/virtualenv/

    virtualenv --no-site-packages BASELINE

    touch /etc/apache2/conf.d/python-virtualenv-baseline
    echo "WSGIPythonHome /opt/virtualenv/BASELINE" > /etc/apache2/conf.d/python-virtualenv-baseline

    virtualenv --no-site-packages DSCNG

    /opt/virtualenv/DSCNG/bin/pip install python-memcached Django==1.5.8 psycopg2==2.5.3 GeoIP==1.3.1

    chown -R vagrant:vagrant /opt/virtualenv/

    cp /vagrant/dev_tools/vagrant/vagrant_vhost /etc/apache2/sites-available/000-default.conf
    service apache2 reload
fi

if $init_db ; then
    . /opt/virtualenv/DSCNG/bin/activate
    python /vagrant/create_tables.py
    /vagrant/dsc/manage.py syncdb --noinput
fi