#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This script takes care of importing data from a directory structure created
by the original DSC.
It is meant to be run from command line like this:

% python import_dsc_dat_files.py dirname(s)

where dirname(s) is one or more names of directories containing DSC data.
"""

# built-in modules
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
import sys
import time
from datetime import datetime, timedelta
from cStringIO import StringIO
import logging
import multiprocessing
import hashlib
import traceback
import calendar
from optparse import OptionParser
from Queue import Empty
import re
import gzip
import bz2

# local imports
from timing_profiler import TimingProfiler
from dsc_storage.psql.connection import ConnectionManager
from dsc_storage.psql.query_help import round_time_to_timespan
from dsc_storage.psql.storage import StoragePSQL
from dsc_storage.psql import db_maintenance

# crate a top level logger
logger = logging.getLogger("dscng.dsc.import")

disconnect_error = \
    """
    A critical error occurred and connection to the SQL server was lost.
    This is probably due to an old version of the psycopg2 library, which
    contains an error when copying data to a server using ssl encryption.
    
    Please see the doc/KNOW_PROBLEMS.txt file for more info and tips how to
    fix this.
    """

class DatFileReader(object):
    
    extensions = (".gz", ".bz2", "")
    file_openers = {".gz": gzip.open,
                    ".bz2": bz2.BZ2File,
                    "": open}
    
    @classmethod
    def exists(cls, filename):
        for ext in cls.extensions:
            if os.path.exists(filename+ext):
                return True
        return False
    
    @classmethod
    def get_file_content(cls, filename):
        """
        This is a convenience function which returns content of a file.
        If there is a compressed version of the file, it decompresses it. 
        """
        for ext in cls.extensions:
            if os.path.exists(filename+ext):
                opener = cls.file_openers[ext]
                # open the file
                try:
                    fobj = opener(filename+ext, "r")
                except IOError:
                    continue
                # read the content
                try:
                    content = fobj.read()
                except IOError:
                    continue
                else:
                    return content
                finally:
                    fobj.close()
        

class SQLDumpThread(multiprocessing.Process):
    
    """
    Defines a separate process used to push large amounts of data into the 
    database.
    """
    
    def __init__(self, db_conn_params, stats=True, stat_out_stream=sys.stderr,
                 *args, **kw):
        multiprocessing.Process.__init__(self, *args, **kw)
        self.error = False
        self.conn = ConnectionManager(**db_conn_params)
        self.cursor = self.conn.cursor()
        self.data_queue = multiprocessing.Queue(64)
        self.stats = stats
        self.stat_out_stream = stat_out_stream

      
    def run(self):
        """
        The is the main loop waiting for data from the data_queue
        """
        logger.debug("Starting SQL thread")
        tp = TimingProfiler()
        end = False
        try:
            while not ((end and self.data_queue.empty()) or self.error):
                tp.start("Waiting for SQL data")
                data = self.data_queue.get()
                tp.end()
                if data:
                    logger.debug("Processing SQL data")
                    tp.start("Processing SQL data")
                    self.flush_data(data)
                    tp.end()
                else:
                    end = True
        except KeyboardInterrupt:
            pass  # just normally close after keyboard interrupt
        logger.debug("END OF SQL THREAD")
        if not self.error:
            if self.stats:
                tp.print_stats(where=self.stat_out_stream)
            self.cursor.close()
            self.conn.close()
            

    def flush_data(self, data):
        """
        Process all data in the queue and push them to the database server
        """
        logger.debug("Flushing data to the SQL server")
        cursor = self.cursor
        if data:
            table_name, columns, sql_data = data
            data_buffer = StringIO(sql_data)
            data_buffer.seek(0)
            try:
                cursor.copy_from(data_buffer, table_name, columns=columns)
            except Exception as exc:
                if self.conn.closed:
                    # the connection is closed, this is bad, we exit - probably
                    # a psycopg2 problem - new version should help
                    logger.critical(disconnect_error)
                    self.error = True
                else:
                    logger.error("Error occurred: %s", exc)
                    self.conn.rollback()
                    self.error = True
                    raise exc
            else:
                self.conn.commit()
            finally:
                data_buffer.close()
        logger.debug("Finished flushing data to the SQL server")


class FilePreloadThread(multiprocessing.Process):
    
    """
    Defines a separate process used to read data from files and push them
    to the processing process.
    """
    
    def __init__(self, *args, **kw):
        multiprocessing.Process.__init__(self, *args, **kw)
        self.error = False
        self.name_input_queue = multiprocessing.Queue(32)
        self.data_output_queue = multiprocessing.Queue(32)


    def run(self):
        """
        The is the main loop waiting for data from the name_input_queue
        """
        end = False
        try:
            while not ((end and self.name_input_queue.empty()) or self.error):
                logger.debug("-- Waiting: %d", self.name_input_queue.qsize())
                try:
                    name_record = self.name_input_queue.get(timeout=0.5)
                except Empty:
                    continue
                if name_record:
                    self.process_file(name_record)
                else:
                    end = True
        except KeyboardInterrupt:
            pass  # ignore the error - do normal close
        self.data_output_queue.close()
        self.data_output_queue.join_thread()
        logger.debug("END OF PREFETCH PROCESS")


    def process_file(self, name_record):
        """
        Process filename in the input queue and put its sha1 checksum into the
        output queue
        """
        filename, dt_name = name_record
        logger.debug("-- Reading file %s", filename)
        sha1sum = hashlib.sha1()
        data = DatFileReader.get_file_content(filename)
        sha1sum.update(data)
        self.data_output_queue.put((filename, dt_name, sha1sum.hexdigest(),
                                    data))
        logger.debug("-- Finished file %s", filename)



class RawBundleConverter(object):
    """
    Takes care of reading .dat files from individual DSC output directories
    and storing them in a DSC database
    """

    caching_points = StoragePSQL.caching_points

    record_type_to_db_params = {
        1: ("dscng_dataarray1d", ('time_record_id', 'data_type_id', 'counts')),
        2: ("dscng_dataarray2d",
            ('time_record_id', 'data_type_id', 'value1', 'counts')),
        3: ("dscng_dataarray3d",
            ('time_record_id', 'data_type_id', 'value1', 'value2', 'counts')),
        "timerecords": ("dscng_timerecord", ('id', 'server_id', 'time')),
        "timerecords_c": ("dscng_timerecord_c",
                          ('id', 'server_id', 'time', 'timespan')),
        "timerecords_accum": ("dscng_timerecord_accum",
                              ('id', 'server_id', 'time')),
        "accum2_ip": ("dscng_datapoint2d_accum_ip",
                      ("time_record_id", "data_type_id", "value1", "count")),
        "accum3_ip": ("dscng_datapoint3d_accum_ip",
                      ("time_record_id", "data_type_id", "value1",
                       "value2", "count")),
        "accum2": ("dscng_datapoint2d_accum",
                   ("time_record_id", "data_type_id", "value1", "count")),
        "accum3": ("dscng_datapoint3d_accum",
                   ("time_record_id", "data_type_id", "value1", "value2",
                    "count")),
        "stats2": ("dscng_dailystats2d",
                   ("time_record_id", "data_type_id", "skipped",
                    "skipped_count")),
        "stats3": ("dscng_dailystats3d",
                   ("time_record_id", "data_type_id", "value1", "skipped",
                    "skipped_count")),

    }
    record_type_ord = ["timerecords", "timerecords_c", "timerecords_accum", 
                       1, 2, 3, "accum3_ip","accum3", "accum2_ip", "accum2",
                       "stats2", "stats3"]
    
    # here we automatically fill in caching tables
    for cp in caching_points:
        record_type_to_db_params[cp+1] = \
                    ("dscng_dataarray1d_c%d" % cp,
                     ('time_record_id', 'data_type_id', 'avg_counts',
                                  'min_counts','max_counts'))
        record_type_to_db_params[cp+2] = \
                    ("dscng_dataarray2d_c%d" % cp,
                     ('time_record_id', 'data_type_id', 'value1', 'avg_counts',
                                  'min_counts','max_counts'))
        record_type_to_db_params[cp+3] = \
                    ("dscng_dataarray3d_c%d" % cp,
                     ('time_record_id', 'data_type_id', 'value1', 'value2',
                      'avg_counts','min_counts','max_counts'))
        record_type_ord += [cp+1, cp+2, cp+3]
    counter = 0
    
    ip4_matcher = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    ip6_matcher = re.compile("^[:\da-f](:?[\da-f]{0,4})+$")



    def __init__(self, db_conn_params, sql_dumper):
        self.sql_thread = sql_dumper
        self.timerecord_ids = {}
        self.timerecord_c_ids = {}
        self.timerecord_accum_ids = {}
        self.conn = ConnectionManager(**db_conn_params)
        self.storage_iface = StoragePSQL(self.conn)
        self.data_type_manager = self.storage_iface.dtm
        self.active_data_types = {}
        self.cursor = self.conn.cursor()
        self.servers = {}
        self.server_timespans = {} 
        self.tp = TimingProfiler()
        self.sql_thread.start()
        self._last_timerecord_id = None
        self._last_timerecord_c_id = None
        self._last_timerecord_accum_id = None
        self._minute_bitmap = None
        self._seen_data_type_ids = None
        self._time_map = None
        self._day_minutes = None
        self.sql_data_buffers = {}
        self._create_sql_data_buffers()
        self.file_preload_process = FilePreloadThread()
        self._startup()
        
    def _startup(self):
        """
        Some database related stuff that has to be run on start - loading of 
        sequence values etc.
        """
        # maintenance first - see if there is something wrong, maybe because
        # the previous import failed
        db_maintenance.fix_db_health(self.conn)
        # some sequence counters that are used later
        self.cursor.execute("SELECT nextval ('dscng_timerecord_id_seq');")
        self._last_timerecord_id = self.cursor.fetchone()[0]
        self.cursor.execute("SELECT nextval ('dscng_timerecord_c_id_seq');")
        self._last_timerecord_c_id = self.cursor.fetchone()[0]        
        self.cursor.execute("SELECT nextval ('dscng_timerecord_accum_id_seq');")
        self._last_timerecord_accum_id = self.cursor.fetchone()[0]
        # load the data types defined in the database
        self._load_data_types()
        self.file_preload_process.start()
        
    def _create_sql_data_buffers(self):
        """
        creates StringIO buffers that are used to store SQL data and pass them
        to the SQL thread later on
        """
        self.sql_data_buffers = dict([(rtp, StringIO()) for rtp
                                       in self.record_type_to_db_params])

    def _load_data_types(self):
        data_types = self.data_type_manager.get_data_types()
        self.active_data_types = dict([(dtype.name, dtype)
                                       for dtype in data_types
                                        if dtype.parent_id is None])
        

    def import_data_dir(self, dirname):
        """
        The main method - imports all the .dat files from directory dirname
        for which it knows the datatype (uses the active_data_types attribute
        to decide which ones these are).
        """
        # do some preliminary stuff to prepare for data parsing
        self.timerecord_ids = {}
        self.timerecord_c_ids = {}
        self.timerecord_accum_ids = {}
        self._minute_bitmap = 1440*[0]
        self._seen_data_type_ids = {} # its id->file sha1sum map
        server_name, parent_name, start_time = self.get_data_from_dirname(
                                                                    dirname)
        self._time_map = {}
        self._day_minutes = []
        for i in xrange(1440):
            _tm = start_time+timedelta(minutes=i)
            self._time_map[str(int(calendar.timegm(_tm.utctimetuple())))] = _tm
            self._day_minutes.append(_tm)
        self.tp.start("Init").start("get server")
        server_id = self.storage_iface.get_or_create_server(server_name,
                                                            parent_name)
        self.tp.end().start("load timerecords")
        self.load_timerecord_ids_for_day(server_id, start_time)
        self.tp.end().end()
        # prepare list of files to be processed - these are pushed to a 
        # separate process in order to better utilize IO bandwidth
        files_to_do = []
        for dt_name, data_type in self.active_data_types.iteritems():
            dat_filename = os.path.join(dirname, data_type.name+".dat")
            if not DatFileReader.exists(dat_filename):
                logger.warn("File %s does not exist - skipping", dat_filename)
            else:
                files_to_do.append((dat_filename, dt_name))
        # process files as they are returned from the file_preload_process
        file_count = len(files_to_do)
        done_count = 0
        while done_count < file_count:
            # push as much filenames as possible to the preload process
            while not self.file_preload_process.name_input_queue.full() and \
                    files_to_do:
                self.file_preload_process.name_input_queue.put(
                                                        files_to_do.pop())
            # wait for some results from the preload process
            filename, dt_name, sha1sum, content = self.file_preload_process.\
                                                    data_output_queue.get()
            logger.debug("Retreived data for %s", filename)
            data_type = self.active_data_types[dt_name]
            if data_type.accumulated:
                self.process_accum_type(data_type, server_id, start_time,
                                        content, sha1sum=sha1sum)
            else:
                self.process_minute_type(data_type, server_id, start_time,
                                         content, sha1sum, dirname)
            done_count += 1
            # periodic check for the SQL Thread health
            if not self.sql_thread.is_alive():
                self.crash_finish("SQL process died")
            if not self.file_preload_process.is_alive():
                self.crash_finish("File pre-load process died")
        self.tp.start("Syncing remaps")
        logger.debug("Syncing dynamic remaps")
        self.data_type_manager.write_dynamic_remaps_to_db()
        self.tp.end()
        self.counter += 1
        # here we flush the datapoints
        self.tp.start("Flush datapoints")
        self.flush_data_points()
        self.tp.end().start("Update bitmaps").start("update")
        self._flush_minute_bitmaps(server_id, start_time)
        self.tp.end().end()
        

    def process_accum_type(self, data_type, server_id, start_time,
                           file_content, sha1sum):
        """
        Process ccumulated (daily aggregated) data for a server and data type
        with data supplied as a string in 'file_content';
        sha1sum is a pre-computed checksum of the string;
        """
        logger.info("Starting data type %s", data_type.name)
        self.tp.start("Prepare data type")
        dims = self.data_type_manager.get_data_type_dimensions(data_type)
        dim_count = len(dims)+1 # count is the last dim
        # is this an ip based datatype
        ip_type = "ip_addr" in [dim.type for dim in dims]    
        # check of reimport
        #
        # check if this file was already imported with the exact same content
        self.tp.end().start("Check reimport")
        update_rec = self._fetch_serverupdate_record(server_id, data_type.id,
                                                     start_time)
        if update_rec and (sha1sum == update_rec.get('dat_file_sha1')):
            logger.info("This file was already imported - skipping")
            self.tp.end()
            return
        # we do not have exactly this file, but we might have some data
        # for the same for this server, data type and day already
        self.tp.start("partial")
        if update_rec:
            # we have - we need to remove the old data first
            logger.warn("Data for this server, data type and day is already \
stored - removing it first")
        # we try removing the data anyway to be on the safe side
        # we just do not advertise it
        self._remove_accum_records(data_type.id, dims, ip_type=ip_type)
        self.tp.end().end()
        self._seen_data_type_ids[data_type.id] = sha1sum
        # parse the data
        logger.debug("Reading data from file")
        self.tp.start("Parse file").start("accum data")
        if dim_count not in (2, 3):
            raise NotImplementedError(
                "Only 2D and 3D accumulated data are supported.")
        # reorder the dimensions if necessary
        if dim_count == 3 and dims[1].type == "ip_addr":
            # store ip_addr in the last column - we need to
            # change the ordering of dimensions as well
            dims_in_src = (dims[1], dims[0])
        else:
            dims_in_src = dims
        data = self.parse_accum_data_file(file_content, *dims_in_src)
        self.tp.end().end()
        # fill the data points into the database
        logger.debug("Inserting accum data points")
        self._insert_datapoints_accum(server_id, dims, start_time,
                                      data_type.id, data, ip_type)
        logger.debug("Data points inserted")

    def _insert_datapoints_accum(self, server_id, dims, start_time,
                                 data_type_id, data, ip_type):
        """
        private convenience function for accumulated data
        """
        dim_count = len(dims) + 1  # count is the last dim
        logger.debug("Inserting accum data points")
        self.tp.start("Add datapoints").start("accum data")
        buffer_suffix = "_ip" if ip_type else ""
        data_buffer = self.sql_data_buffers[("accum%d%s") %
                                            (dim_count, buffer_suffix)]
        time_record_id = self.get_timerecord_accum_id(server_id, start_time)
        skipped_vals = {}  # here we accumulate skipped data from records
        # The following code is not very clean (normalized) on purpose
        # this is to improve speed by unwinding the loops and conditions
        if dim_count == 2:
            for row in data:
                if row[0] == "-:SKIPPED:-":
                    if None not in skipped_vals:
                        skipped_vals[None] = [-1, -1]  # [ips, count sum]
                    skipped_vals[None][0] = row[1]
                elif row[0] == "-:SKIPPED_SUM:-":
                    if None not in skipped_vals:
                        skipped_vals[None] = [-1, -1]  # [ips, count sum]
                    skipped_vals[None][1] = row[1]
                elif ip_type and not self.is_valid_ip(row[0]):
                    logger.warn("Invalid value for IP address %s, skipping",
                                row[0])
                else:
                    data_buffer.write("%d\t%d\t%s\t%d\n" %
                                      (time_record_id, data_type_id, row[0],
                                       row[1]))
        elif dim_count == 3:
            for row in data:
                if row[0] == "-:SKIPPED:-":
                    if row[1] not in skipped_vals:
                        skipped_vals[row[1]] = [-1, -1]  # [ips, count sum]
                    skipped_vals[row[1]][0] = row[2]
                elif row[0] == "-:SKIPPED_SUM:-":
                    if row[1] not in skipped_vals:
                        skipped_vals[row[1]] = [-1, -1]  # [ips, count sum]
                    skipped_vals[row[1]][1] = row[2]
                elif not ip_type:
                    data_buffer.write("%d\t%d\t%s\t%d\t%d\n" %
                                      (time_record_id, data_type_id, row[0],
                                       row[1], row[2]))
                elif self.is_valid_ip(row[0]):
                    # we switch IP addressed to be in value2
                    # to make indexes work better
                    data_buffer.write("%d\t%d\t%d\t%s\t%d\n" %
                                      (time_record_id, data_type_id, row[1],
                                       row[0], row[2]))
                else:
                    logger.warn("Invalid value for IP address %s, skipping",
                                row[0])
        # now store the skipped values
        # the note above about code normalization applies here as well
        stats_data_buffer = self.sql_data_buffers["stats%d" % dim_count]
        if dim_count == 2:
            for _key, val in skipped_vals.iteritems():
                stats_data_buffer.write("%d\t%d\t%d\t%d\n" %
                                        (time_record_id, data_type_id,
                                         val[0], val[1]))
        elif dim_count == 3:
            for key, val in skipped_vals.iteritems():
                stats_data_buffer.write("%d\t%d\t%d\t%d\t%d\n" %
                                        (time_record_id, data_type_id, key,
                                         val[0], val[1]))
        self.tp.end().end()
        logger.debug("Data points inserted")

    def process_minute_type(self, data_type, server_id, start_time,
                            file_content, sha1sum, dirname):
        """
        Process minute based data for a server and data type with data supplied
        as a string in 'file_content';
        sha1sum is a pre-computed checksum of the string;
        dirname is supplied in order to make merging of dnssec and normal qtypes
        possible
        """
        logger.info("Starting data type %s", data_type.name)
        self.tp.start("Prepare data type")
        # get the data type
        dims = self.data_type_manager.get_data_type_dimensions(data_type)
        # check of reimport
        #
        # check if this file was already imported with the exact same content
        self.tp.end().start("Check reimport")
        update_rec = self._fetch_serverupdate_record(server_id, data_type.id,
                                                     start_time)
        if update_rec and (sha1sum == update_rec.get('dat_file_sha1')):
            logger.info("This file was already imported - skipping")
            self.tp.end()
            return
        # we do not have exactly this file, but we might have some data
        # for the same for this server, data type and day already
        self.tp.start("partial")
        if update_rec:
            # we have - we remove the old data first
            logger.warn("Data for this server, data type and day is already \
stored - removing it first")
        # we try removing the data anyway to be on the safe side
        # we just do not advertise it
        self._remove_minute_records(data_type.id, dims)
        self._seen_data_type_ids[data_type.id] = sha1sum
        self.tp.end().end()
        # parse and fill in the data
        logger.debug("Reading data from file")
        self.tp.start("Parse file").start("minute data")
        if len(dims) == 0:
            data = self.parse_data_file_1d(file_content)
        elif len(dims) == 1:
            if data_type.name == "dnssec_qtype":
                # dnssec_qtype is processed together with qtype
                self.tp.end().end()
                return
            data = self.parse_data_file_2d(file_content, dims[0])
            if data_type.name == "qtype":
                # we merge dnssec_qtype and qtype in the database
                dnssec_filename = os.path.join(dirname, "dnssec_qtype.dat")
                dnssec_data = DatFileReader.get_file_content(dnssec_filename)
                if dnssec_data:
                    data2 = self.parse_data_file_2d(dnssec_data, dims[0])
                    self.tp.start("merge dnssec_qtype")
                    self._merge_types(data, data2)
                    self.tp.end()
        elif len(dims) == 2:
            data = self.parse_data_file_3d(file_content, dims[0],
                                           dims[1])
        self.tp.end().end()
        self.tp.start("Caching data")
        logger.debug("Create caching data")
        self.tp.start("create")
        data2 = self.get_caching_data(data, start_time)
        self.tp.end()
        # skip the rest if there is no data
        if not data2:
            logger.warn("No data found")
            return
        logger.debug("Store caching data")
        self.tp.start("store")
        data2 = self.put_caching_data_into_sql_buffers(server_id, data_type.id,
                                                       data2)
        self.tp.end()
        self.tp.end()
        logger.debug("Inserting regular data points")
        self.tp.start("Add datapoints").start("minute data")
        self.put_data_into_sql_buffers(server_id, data_type.id, data)
        self.tp.end().end().start("Update bitmaps").start("update")
        self._update_minute_bitmap(data)
        self.tp.end().end()
        logger.debug("Data points inserted")

    @classmethod
    def _merge_types(cls, data, data2):
        """
        When two data types should be merged in the database (for example 
        normal and DNSSEC related record types), here the data is put together
        """
        data2dict = dict(data2)
        for rec_tm, records in data:
            records2 = data2dict.get(rec_tm)
            subst = 0
            for rec2 in records2:
                if rec2[0] != (-1,):
                    records.append(rec2)
                    subst += rec2[1]
            # the counts from data2 are part of 'else' in data
            # we need to substract the new defined data here
            else_rec_idx = None
            else_count = None
            for i, rec in enumerate(records):
                if rec[0] == (-1,):
                    # this is the 'else' record
                    else_rec_idx = i
                    else_count = rec[1]
                    break
            if else_count:
                if else_count < subst:
                    raise ValueError("Merging data would lead to negative \
value for 'else'. This is probably an error.")
                records[else_rec_idx] = ((-1,), else_count-subst)

    def parse_data_file_1d(self, file_content):
        """
        Parse a 1D .dat file
        """
        to_insert = []
        for line in file_content.splitlines():
            if line.startswith("#"):
                continue
            if line.count(" ") == 0:
                rec_tm = line.strip()
                value = 0
            else:
                rec_tm, rest = line.split(" ", 1)
                value = int(rest)
            rec_dt = self._time_map[rec_tm]
            to_insert.append((rec_dt, [((), value)]))
        return to_insert


    def parse_data_file_2d(self, file_content, dim1):
        """
        Parse a 2D .dat file - dim1_name is used to determine if remap is 
        needed for storage of this data into the DB
        """
        to_insert = []
        remap1 = self.data_type_manager.get_user_to_db_value_remap(dim1)
        # we use a local variable for speed and readability sake
        value_to_int = self.data_type_manager.remap_user_value_to_db
        local_int = int
        for line in file_content.splitlines():
            if line.startswith("#"):
                continue
            data = line.split()
            rec_dt = self._time_map[data[0]]
            row = []
            i = 1
            len_data = len(data)
            if len_data % 2 == 0:
                # even number of items - a problem
                logger.error("Wrong number of items in line '%s', removing last"
                             " item", line)
                len_data -= 1
            while i < len_data:
                if remap1 is None and data[i].isdigit():
                    value1 = local_int(data[i])
                else:
                    value1 = value_to_int(data[i], dim1)
                count = local_int(data[i+1])
                row.append(((value1,), count))
                i += 2
            to_insert.append((rec_dt, row))
        return to_insert


    def parse_data_file_3d(self, file_content, dim1, dim2):
        """
        Parse a 3D .dat file - dim1_name and dim2_name areused to determine if
        remap is needed for storage of this data into the DB
        """
        to_insert = []
        remap1 = self.data_type_manager.get_user_to_db_value_remap(dim1)
        remap2 = self.data_type_manager.get_user_to_db_value_remap(dim2)
        # we use a local variable for speed and readability sake
        value_to_int = self.data_type_manager.remap_user_value_to_db
        local_int = int
        for line in file_content.splitlines():
            if line.startswith("#"):
                continue
            data = line.split()
            rec_dt = self._time_map[data[0]]
            row = []
            i = 1
            len_data = len(data)
            if len_data % 2 == 0:
                # even number of items - a problem
                logger.error("Wrong number of items in line '%s', removing last"
                             " item", line)
                len_data -= 1
            while i < len_data:
                if remap1 is None and data[i].isdigit():
                    value1 = local_int(data[i])
                else:
                    value1 = value_to_int(data[i], dim1)
                # third level of splitting
                parts = data[i+1].split(":")
                j = 0
                len_parts = len(parts)
                if len_parts % 2 == 1:
                    # odd number of items - a problem
                    logger.error("Wrong number of items in record '%s', "
                                 "removing last item", data[i+1])
                    len_parts -= 1
                while j < len_parts:
                    if remap2 is None and parts[j].isdigit():
                        value2 = local_int(parts[j])
                    else:
                        value2 = value_to_int(parts[j], dim2)
                    count = local_int(parts[j+1])
                    row.append(((value1, value2), count))
                    j += 2
                i += 2
            to_insert.append((rec_dt, row))
        return to_insert

    def parse_accum_data_file(self, file_content, *dims):
        """
        Parse a data file with accumulated values. This differs from the
        previous method because it does not expect timestamp to be present in
        the first column of each line
        """
        to_insert = []
        dim_count = len(dims)+1 # count is the last dim
        for line in file_content.splitlines():
            if line.startswith("#"):
                continue
            data = line.split()
            if len(data) != dim_count:
                raise ValueError("Wrong number of columns in '%s': %d" %\
                                 (line, len(data)))
            row = []
            for i, dim in enumerate(dims):
                if dim.type != "ip_addr" and data[i][:9] != '-:SKIPPED':
                    value = self.data_type_manager.\
                                remap_user_value_to_db(data[i], dim)
                else:
                    value = data[i]
                row.append(value)
            count = int(data[-1])
            row.append(count)
            to_insert.append(tuple(row))
        return to_insert

    @classmethod
    def is_valid_ip(cls, ip):
        return cls.ip4_matcher.match(ip) or cls.ip6_matcher.match(ip)

    @classmethod
    def _int_array_to_str(cls, array):
        """
        Format an array of ints to string that is usable in SQL commands
        """
        return "{%s}" % ",".join(map(str, array))
    
    @classmethod
    def _range_array_to_str(cls, array):
        """
        Format an array of intrange values to string that is usable in SQL
        commands
        """
        return "{%s}" % ",".join(['"(%s)"' % ",".join(map(str, row))
                                  for row in array])


    def put_data_into_sql_buffers(self, server_id, data_type_id, data):
        """
        Take data and process it into SQL format from which the SQL thread
        would be able to directly copy it into the database
        """
        if not data:
            return
        date_to_data = dict(data)
        min_date = min(date_to_data.keys())
        max_date = max(date_to_data.keys())
        self._record_server_timespan(server_id, min_date, max_date)
        frm_date = min_date.replace(minute=0)
        date = frm_date
        dim_count = None
        while date <= max_date:
            arrays = {}
            for minute in range(60):
                date = frm_date.replace(minute=minute)
                minute_data = date_to_data.get(date)
                if minute_data:
                    for key, count in minute_data:
                        if not key in arrays:
                            arrays[key] = []
                        for _ign in range(len(arrays[key]), minute):
                            # here we add missing data
                            arrays[key].append(-1)
                        arrays[key].append(count)
                        dim_count = len(key)
            if dim_count is None:
                frm_date = frm_date.replace(minute=0) + timedelta(hours=1)
                continue
            data_buffer = self.sql_data_buffers[dim_count+1]
            for dims, array in arrays.iteritems():
                if len(array) != 60:
                    for _ign in range(60 - len(array)):
                        array.append(-1)
                time_record_id = self.get_timerecord_id(server_id, frm_date)
                if len(dims) == 0:
                    data_buffer.write("%d\t%d\t%s\n" % \
                                      (time_record_id, data_type_id,
                                       self._int_array_to_str(array)))
                elif len(dims) == 1:
                    data_buffer.write("%d\t%d\t%d\t%s\n" % \
                                      (time_record_id, data_type_id, dims[0],
                                       self._int_array_to_str(array)))
                elif len(dims) == 2:
                    data_buffer.write("%d\t%d\t%d\t%d\t%s\n" % \
                                      (time_record_id, data_type_id, dims[0],
                                       dims[1], self._int_array_to_str(array)))
                    
            frm_date = frm_date.replace(minute=0) + timedelta(hours=1) 


    def put_caching_data_into_sql_buffers(self, server_id, data_type_id, data):
        """
        Put caching data into buffers formatted for direct copying into the
        database by the SQL thread 
        """
        if not data:
            return
        for cpnt, date_to_data in data.iteritems():
            min_date = min(date_to_data.keys())
            max_date = max(date_to_data.keys())
            frm_date = round_time_to_timespan(min_date, 360, upper=False)
            date = frm_date
            dim_count = None
            while date <= max_date:
                arrays = {}
                date = frm_date
                for minute in range(0, 360, cpnt):
                    date = frm_date + timedelta(minutes=minute)
                    minute_data = date_to_data.get(date)
                    if minute_data is not None:
                        for key, count in minute_data:
                            if not key in arrays:
                                arrays[key] = []
                            for _ign in range(len(arrays[key]), (minute//cpnt)):
                                # here we add missing data
                                arrays[key].append((-1, -1, -1))
                            arrays[key].append(count)
                            dim_count = len(key)
                if dim_count is None:
                    frm_date = frm_date + timedelta(hours=6)
                    continue
                for dims, array in arrays.iteritems():
                    # here we add missing data that were at the end to they
                    # could not be added in the code above
                    if cpnt < 360 and len(array) != (360 / cpnt):
                        for _ign in range((360 / cpnt) - len(array)):
                            array.append((-1, -1, -1))
                    data_buffer = self.sql_data_buffers[dim_count+cpnt+1]
                    time_record_id = self.get_timerecord_c_id(server_id,
                                                              frm_date, cpnt)
                    avgs = [x[0] for x in array]
                    mins = [x[1] for x in array]
                    maxs = [x[2] for x in array]
                    if len(dims) == 0:
                        data_buffer.write("%d\t%d\t%s\t%s\t%s\n" % \
                                          (time_record_id, data_type_id,
                                           self._int_array_to_str(avgs),
                                           self._int_array_to_str(mins),
                                           self._int_array_to_str(maxs),
                                           ))
                    elif len(dims) == 1:
                        data_buffer.write("%d\t%d\t%d\t%s\t%s\t%s\n" % \
                                          (time_record_id, data_type_id,
                                           dims[0],
                                           self._int_array_to_str(avgs),
                                           self._int_array_to_str(mins),
                                           self._int_array_to_str(maxs),
                                           ))
                    elif len(dims) == 2:
                        data_buffer.write("%d\t%d\t%d\t%d\t%s\t%s\t%s\n" % \
                                          (time_record_id, data_type_id,
                                           dims[0], dims[1],
                                           self._int_array_to_str(avgs),
                                           self._int_array_to_str(mins),
                                           self._int_array_to_str(maxs),
                                           ))
                        
                frm_date = frm_date + timedelta(hours=6)


    def flush_data_points(self):
        """
        Flush data from the sql buffers to the SQL thread
        """
        logger.debug("Pushing data points to SQL thread (%d KB)",
                      sum([df.tell() 
                           for df in self.sql_data_buffers.values()])//1000)
        for name in self.record_type_ord:
            data_buffer = self.sql_data_buffers[name]
            table_name, columns = self.record_type_to_db_params[name]
            data = data_buffer.getvalue()
            if data:
                self.tp.start("waiting for SQL thread")
                if self.sql_thread.is_alive():
                    self.sql_thread.data_queue.put((table_name, columns, data))
                else:
                    self.crash_finish("SQL thread died")
                self.tp.end()
        self._create_sql_data_buffers()
        logger.debug("Data pushed to SQL thread")


    def get_timerecord_id(self, server_id, rec_time):
        """
        get an id for time record - either find it in a dictionary of already
        existing time records or add it into a list of time records to be
        later created (in the SQL thread).
        """
        key = (server_id, rec_time)
        if key in self.timerecord_ids:
            return self.timerecord_ids[key]
        else:
            data_file = self.sql_data_buffers["timerecords"]
            tr_id = self._last_timerecord_id
            self._last_timerecord_id += 1
            data_file.write("%d\t%d\t%s\n" % (tr_id, server_id, rec_time))
            self.timerecord_ids[key] = tr_id
            return tr_id
        
    def get_timerecord_c_id(self, server_id, rec_time, timespan):
        """
        get an id for time record - either find it in a dictionary of already
        existing time records or add it into a list of time records to be
        later created (in the SQL thread).
        """
        key = (server_id, rec_time, timespan)
        if key in self.timerecord_c_ids:
            return self.timerecord_c_ids[key]
        else:
            data_file = self.sql_data_buffers["timerecords_c"]
            tr_id = self._last_timerecord_c_id
            self._last_timerecord_c_id += 1
            data_file.write("%d\t%d\t%s\t%d\n" % (tr_id, server_id, rec_time,
                                                  timespan))
            self.timerecord_c_ids[key] = tr_id
            return tr_id

    def get_timerecord_accum_id(self, server_id, rec_time):
        """
        get an id for time record - either find it in a dictionary of already
        existing time records or add it into a list of time records to be
        later created (in the SQL thread).
        """
        key = (server_id, rec_time)
        if key in self.timerecord_accum_ids:
            return self.timerecord_accum_ids[key]
        else:
            data_file = self.sql_data_buffers["timerecords_accum"]
            tr_id = self._last_timerecord_accum_id
            self._last_timerecord_accum_id += 1
            data_file.write("%d\t%d\t%s\n" % (tr_id, server_id, rec_time))
            self.timerecord_accum_ids[key] = tr_id
            return tr_id

    def load_timerecord_ids_for_day(self, server_id, day_start):
        """
        Loads time records from a database in order to pre-populate the 
        timerecord_???_ids attributes
        """
        # normal timerecords
        self.cursor.execute("""SELECT id, time FROM dscng_timerecord
            WHERE server_id=%s AND time>=%s AND time<%s""",
            (server_id, day_start, day_start+timedelta(days=1)))
        for tr_id, rec_time in self.cursor:
            self.timerecord_ids[(server_id, rec_time)] = tr_id
        # cached data
        for caching_point in self.caching_points:
            # timerecords
            self.cursor.execute("""SELECT id, time FROM dscng_timerecord_c
                WHERE server_id=%s AND time>=%s AND time<%s AND timespan=%s""",
                (server_id, day_start, day_start+timedelta(days=1),
                 caching_point))
            for tr_id, rec_time in self.cursor:
                key = (server_id, rec_time, caching_point)
                self.timerecord_c_ids[key] = tr_id
        # accum timerecords
        self.cursor.execute("""SELECT id, time FROM dscng_timerecord_accum
            WHERE server_id=%s AND time>=%s AND time<%s""",
            (server_id, day_start, day_start+timedelta(days=1)))
        for tr_id, rec_time in self.cursor:
            self.timerecord_accum_ids[(server_id, rec_time)] = tr_id
      

    def _update_minute_bitmap(self, data):
        """
        update the bitmap describing the minutes for which data are available
        using data for the current data type. The value that is stored is 
        a logic sum of values for all data types.
        """
        if data:
            date_to_data = dict(data)
            min_date = min(date_to_data.keys())
            day_start = min_date.replace(hour=0, minute=0)
            for minute_tm in date_to_data:
                idx = int((minute_tm - day_start).seconds // 60)
                self._minute_bitmap[idx] = 1
    
            
    def _flush_minute_bitmaps(self, server_id, day):
        """
        Store the current minute bitmap into the database.
        Because for accum data types there is no way how to find out which 
        underlying minute data was available, we use the logic sum of data
        for all data types for all data types.
        At present it is deemed the best approximation of the right values 
        """
        bitstring = "".join(map(str, self._minute_bitmap))
        for dt_id, dat_file_sha1 in self._seen_data_type_ids.iteritems():
            self.cursor.execute("""UPDATE dscng_serverupdates
                SET minutes=%s, dat_file_sha1=%s 
                WHERE server_id=%s AND data_type_id=%s AND day=%s""",
                (bitstring, dat_file_sha1, server_id, dt_id, day))
            if self.cursor.rowcount == 0:
                self.cursor.execute("""INSERT INTO dscng_serverupdates 
                (server_id, data_type_id, day, minutes, dat_file_sha1)
                VALUES (%s,%s,%s,%s,%s)""",
                (server_id, dt_id, day, bitstring, dat_file_sha1))
        self.conn.commit()

        
    def _fetch_serverupdate_record(self, server_id, data_type_id, date):
        """
        Return data store in the dscng_serverupdates table for the
        server, data type and day at hand
        """
        day_start = date.replace(hour=0, minute=0, second=0)
        self.cursor.execute("""SELECT minutes, dat_file_sha1 
            FROM dscng_serverupdates 
            WHERE server_id=%s AND data_type_id=%s AND day=%s""",
            (server_id, data_type_id, day_start))
        if self.cursor.rowcount > 0:
            bitmap, sha1sum = self.cursor.fetchone()
            return {'update_bitmap': bitmap, "dat_file_sha1": sha1sum}
        return None

 
    def _remove_minute_records(self, data_type_id, dims):
        """
        Remove all existing data for current time records as defined in the
        timerecord_ids attribute
        Remove all existing data for the server, data_type and date at hand.
        This is used to clean the database before the same file is reimported.
        This code relies on load_timerecord_ids_for_day being run before it
        so that the ids of timerecords are available
        """
        self.tp.start("remove old data")
        dim_count = len(dims) + 1
        # normal data
        # timerecord_ids is pre-filled with time_record ids for the date
        # that is currently processed - we can use it in cleanup 
        tr_ids = self.timerecord_ids.values()
        self.tp.start("minute - normal")
        if tr_ids:
            self.cursor.execute("""DELETE FROM dscng_dataarray%dd 
                WHERE time_record_id IN (%s) AND data_type_id=%d""" % \
                (dim_count, ",".join(map(str, tr_ids)), data_type_id))
            logger.debug("Removed %d records", self.cursor.rowcount)
        self.tp.end().start("minute - caching")
        # cached data
        for caching_point in self.caching_points:
            # timerecords
            # the same as with timerecord_ids is valid for timerecord_c_ids
            tr_ids = [tr_id for (key, tr_id) in self.timerecord_c_ids.items()
                      if key[2] == caching_point]
            logger.debug(tr_ids)
            if tr_ids:
                self.cursor.execute("""DELETE FROM dscng_dataarray%dd_c%d 
                    WHERE time_record_id IN (%s) AND data_type_id=%d""" % \
                    (dim_count, caching_point, ",".join(map(str, tr_ids)),
                     data_type_id))
        self.tp.end().end()
     
        
    def _remove_accum_records(self, data_type_id, dims, ip_type=False):
        """
        Remove all existing data for current time records as defined in the
        timerecord_accum_ids attribute
        This is used to clean the database before the same file is reimported.
        This code relies on load_timerecord_ids_for_day being run before it
        so that the ids of timerecords are available
        """
        self.tp.start("remove old data").start("accum")
        dim_count = len(dims) + 1
        # normal data
        # timerecord_ids is pre-filled with time_record ids for the date
        # that is currently processed - we can use it in cleanup 
        tr_ids = self.timerecord_accum_ids.values()
        logger.debug(tr_ids)
        if tr_ids:
            add = "_ip" if ip_type else ""
            self.cursor.execute("""DELETE FROM dscng_datapoint%dd_accum%s 
                WHERE time_record_id IN (%s) AND data_type_id=%d""" % \
                (dim_count, add, ",".join(map(str, tr_ids)), data_type_id))
            logger.debug("Removed %d records", self.cursor.rowcount)
            # remove the dailystats record as well
            self.cursor.execute("""DELETE FROM dscng_dailystats%dd
                WHERE time_record_id IN (%s) AND data_type_id=%d""" %
                (dim_count, ",".join(map(str, tr_ids)), data_type_id))
        self.tp.end().end()
          

    def _record_server_timespan(self, server_id, min_time, max_time):
        """
        Store min and max times for a server in an attribute - the data will be
        dumped into the database as part of the finish method to save time
        """
        if server_id not in self.server_timespans:
            self.server_timespans[server_id] = [min_time, max_time]
        else:
            if self.server_timespans[server_id][0] > min_time:
                self.server_timespans[server_id][0] = min_time
            if self.server_timespans[server_id][1] < max_time:
                self.server_timespans[server_id][1] = max_time

    def _flush_server_timespans(self):
        """
        Flush the information about server timespan into the database
        """
        for server_id, (min_tm, max_tm) in self.server_timespans.iteritems():
            self.storage_iface.update_server_timespan(server_id, min_tm,
                                                      max_tm)
        self.server_timespans = {}
        
    def flush_data(self):
        """
        Does sync all unwritten data with the database
        """
        # flush server timespans
        self._flush_server_timespans()
        # synchronize seq counters side-stepped using copy
        if self._last_timerecord_id > 1:
            self.cursor.execute(
                "SELECT setval('dscng_timerecord_id_seq', %s);",
                (self._last_timerecord_id-1,))
        if self._last_timerecord_c_id > 1:
            self.cursor.execute(
                "SELECT setval('dscng_timerecord_c_id_seq', %s);",
                (self._last_timerecord_c_id-1,))
        if self._last_timerecord_accum_id > 1:
            self.cursor.execute(
                "SELECT setval('dscng_timerecord_accum_id_seq', %s);",
                (self._last_timerecord_accum_id-1,)) 
        

    def finish(self):
        """
        do all the stuff needed to synchronize database before the import script
        finishes
        """
        logger.debug("Stopping SQL thread")
        self.sql_thread.data_queue.put(())
        logger.debug("Stopping file preload process")
        self.file_preload_process.name_input_queue.put(())
        self._finish()

    def crash_finish(self, error_msg=None):
        """
        called to cleanup when an error occured and the SQL thread is no longer
        reachable
        """
        # empty the queue and join it
        print >> sys.stderr, " - Emptying data queues"
        # sql thread queues
        while not self.sql_thread.data_queue.empty():
            self.sql_thread.data_queue.get_nowait()
        if self.sql_thread.is_alive():
            self.sql_thread.data_queue.put(())
        # file pre-fetch process queues
        # empty the input queue
        while not self.file_preload_process.name_input_queue.empty():
            self.file_preload_process.name_input_queue.get_nowait()
        # put there an empty value to signal end, close it and join the thread
        if self.file_preload_process.is_alive():
            self.file_preload_process.name_input_queue.put(())
            self.file_preload_process.name_input_queue.close()
            self.file_preload_process.name_input_queue.join_thread()
            self.file_preload_process.error = True
        # clean the output queue - until the process is alive - there is no
        # other reliable way of determining that the queue is empty
        while self.file_preload_process.is_alive():
            try:
                self.file_preload_process.data_output_queue.get_nowait()
            except Empty:
                pass
        print >> sys.stderr, " - Closing database and external processes"
        self._finish()
        if error_msg:
            raise Exception(error_msg)


    def _finish(self):
        if not self.conn.closed:
            self.flush_data()
        if self.sql_thread.is_alive():
            logger.debug("Joining SQL thread")
            self.sql_thread.join()
        self.file_preload_process.join()

    #@classmethod
    def get_caching_data(self, data, start_time):
        """aggregate data into caching data based on timespans defined in the
        caching_points class attribute"""
        date_to_data = dict(data)
        caching_data = dict([(pnt, {}) for pnt in self.caching_points])
        cp_to_date_to_data = dict([(pnt, {}) for pnt in self.caching_points])
        for mnt, cur_time in enumerate(self._day_minutes):
            data_set = date_to_data.get(cur_time)
            if data_set:
                for (dim1, count) in data_set:
                    for cpnt in self.caching_points:
                        cp_data = caching_data[cpnt]
                        # put data into the cache
                        if dim1 not in cp_data:
                            cp_data[dim1] = []
                        cp_data[dim1].append(count)
            for cpnt in self.caching_points:
                if (mnt+1) % cpnt == 0:
                    # we need to output caching data for this caching point
                    cp_data = caching_data[cpnt]
                    st_tm = cur_time - timedelta(minutes=cpnt-1)
                    cp_to_date_to_data[cpnt][st_tm] = []
                    for dim1, values in cp_data.items():
                        if values:
                            avg = sum(values) // len(values)
                            cval = (avg, min(values), max(values))
                            cp_to_date_to_data[cpnt][st_tm].append((dim1, cval))
                    caching_data[cpnt] = {}
        return cp_to_date_to_data


    @classmethod
    def get_data_from_dirname(cls, dirname):
        """parse directory name to obtain date, server and parent server name"""
        dname = ""
        while not dname:
            _base, dname = os.path.split(dirname)
        assert dname.isdigit()
        assert len(dname) == 8
        date = datetime(year=int(dname[:4]),
                        month=int(dname[4:6]),
                        day=int(dname[6:]))
        _base, dname = os.path.split(_base)
        _base, parent = os.path.split(_base)
        return dname, parent, date
    
    
    def get_unknown_data_types(self, dirname): 
        """returns a set of .dat files that are not known"""
        known = set([data_type.name+".dat"
                     for data_type in self.data_type_manager.get_data_types()])
        known.add('dnssec_qtype.dat') # this is not in the list but gets merged
        found = [flnm for flnm in os.listdir(dirname) if flnm.endswith('.dat')]
        return set(found) - known
    

    def get_active_dat_files(self, dirname):
        """return filenames for active data types"""
        return self.filter_active_dat_files(os.listdir(dirname))


    def filter_active_dat_files(self, files):
        """return those files from files that represent active data types"""
        active = set([data_type+".dat" for data_type in self.active_data_types])
        result = set()
        for fname in files:
            if fname in active:
                result.add(fname)
            else:
                for ext in DatFileReader.extensions:
                    if ext and fname.endswith(ext) and \
                       (fname[:-len(ext)] in active):
                        result.add(fname)
        return result
    

def import_dirs(db_conn_params, names, quiet=False, show_progress=False,
                stat_out_stream=sys.stderr):
    """
    db_conn_params is a dictionary describing a connection to the database
    in a way meaningful to the database at hand.
    names is a list of directory names to be imported 
    """
    sql_dumper = SQLDumpThread(db_conn_params, stats=(not quiet),
                               stat_out_stream=stat_out_stream)
    importer = RawBundleConverter(db_conn_params, sql_dumper=sql_dumper)
    unknown_dat_files = set()

    files_to_process = []
    logger.info("Searching for dirs..")
    if show_progress:
        log_to_progressbar("Searching for data files..")
    total_size = 0
    total_count = 0
    # search for importable directories
    for fname in names:
        fname = fname.rstrip("/")
        if os.path.isdir(fname):
            sys.stdout.flush()
            for top, _dirs, files in os.walk(fname, followlinks=False):
                if os.path.split(top)[1].isdigit():
                    this_size = 0
                    for dat_file in importer.filter_active_dat_files(files):
                        this_size += os.path.getsize(
                                            os.path.join(top, dat_file))
                    if this_size != 0:
                        files_to_process.append(top)
                        total_size += this_size
            total_count += len(files_to_process)
        else:
            print >> sys.stderr, "Input should be a directory"
    # process what was found
    logger.info("Found %d dirs with %.1f MB data",
                 total_count, total_size/1000000.0)
    start_time = time.time()
    processed_size = 0
    count = 0
    if show_progress:
        log_to_progressbar("Starting processing..")
    try: # this catches keyboard interrupts only
        for fullname in sorted(files_to_process):
            count += 1
            logger.info("Processing %s", fullname)
            for dat_file in importer.get_active_dat_files(fullname):
                processed_size += os.path.getsize(
                                        os.path.join(fullname, dat_file))
            round_start = time.time()
            try:
                importer.import_data_dir(fullname)
            except Exception as exc:
                _err_type, _err_val, trace = sys.exc_info()
                tb_str = "".join(traceback.format_tb(trace))
                logger.critical("Import was unsuccessful - error: %s", exc)
                logger.critical("Traceback:\n%s", tb_str)
                print >> sys.stderr
                print >> sys.stderr, "Import was unsuccessful - error:", exc
                print >> sys.stderr, "Traceback:\n", tb_str
                print >> sys.stderr
                print >> sys.stderr, "Cleaning up"
                importer.crash_finish()
                sys.exit(101)
            unknown_dat_files |= importer.get_unknown_data_types(fullname)
            if processed_size:
                # we do not want to devide by zero here
                to_go_sec = int((total_size-processed_size)*
                                (time.time()-start_time)/processed_size)
                to_go = timedelta(seconds=to_go_sec)
                done_part = 1.0*processed_size/total_size
                log_string = "%s, %.2f s/dir, %d kB/s, %.1f%%, ~%s to go" % \
                    (timedelta(seconds=int(time.time()-start_time)),
                     (time.time()-start_time)/count,
                     processed_size/(time.time()-start_time)/1000,
                     100.0*done_part,
                     to_go)
                logger.info(" => %.2f s  (%s)", time.time()-round_start, log_string)
                if show_progress:
                    log_to_progressbar(format_percent_bar(done_part) + " " + \
                                       log_string)
            if count % 100 == 0:
                # do this after every 100 files
                importer.flush_data()
    except KeyboardInterrupt:
        print >> sys.stderr
        print >> sys.stderr, "* Caught keyboard interrupt - cleaning up"
        importer.crash_finish()
        sys.exit(100)
            
    importer.finish()
    if show_progress:
        print
    if not quiet:
        print
        importer.tp.print_stats(where=stat_out_stream)
    # if unknown .dat files are found during parsing, we report them here
    if unknown_dat_files and not quiet:
        print "*************************************************************"
        print "*            Warning: unknown data files found              *"
        print "*************************************************************"
        print """* The following data files were found, which are not known  *
* to the import script. It would be great if you could send *
* examples of these to the developers of DSCng. Thanks.    *"""
        print "*                                                           *"
        print "* Here are the names:                                       *"
        for udf in sorted(unknown_dat_files):
            print "*   %-55s *" % udf
        print "*************************************************************"


def format_percent_bar(percent, bar_length=20, comp_char="#", incomp_char="-"):
    """Format a progress bar of specified bar_length based on the percentage
    complete"""
    # assert could fail because of rounding errors
    #assert 0 <= percent <= 1
    comp_part = int(round(bar_length * percent))
    rest = bar_length - comp_part
    return comp_part*comp_char + rest*incomp_char

def log_to_progressbar(text):
    """clean current line on console and print text"""
    sys.stdout.write(chr(27) + '[2K' + chr(27) + '[G')
    sys.stdout.write(text)
    sys.stdout.flush()


def main():
    """
    This is the main function that is run when this script is run from the
    command line.
    It mainly calls import_dirs for dirs passed from command line
    """
    print "# DSCng - importing original DSC .dat files"
    print 
    # parse options
    opsr = OptionParser(usage="python %prog [options] dsc_data_directory")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-t", "--timing-stats", action="store_true",
                    dest="timing_stats", default=False,
                    help="Print timing statistics after the import finishes")
    opsr.add_option("-s", "--log-to-stderr", action="store_true",
                    dest="log_to_stderr", default=False,
                    help="Print logging output to stderr instead of a file")
    (options, filenames) = opsr.parse_args()
    
    now_str = datetime.now().strftime("%Y-%m-%d_%H%M%S")
    log_filename = "dscng_import-%s.log" % now_str
    log_level = logging.DEBUG if options.verbose else logging.INFO
    if options.log_to_stderr:
        logging.basicConfig(level=log_level,
                format="%(relativeCreated)6d ms  %(levelname)s: %(message)s",
                stream=sys.stderr)
    else:
        logging.basicConfig(level=log_level,
                format="%(relativeCreated)6d ms  %(levelname)s: %(message)s",
                filename=log_filename)
    # check arguments
    if len(sys.argv) == 1:
        print >> sys.stderr, "Give me a name of a directory containing DSC \
data files"
        sys.exit(100)
    # import django settings
    from dsc.dsc import settings
    from dsc_storage.psql.connection import get_connection_params_from_settings
    conn_params = get_connection_params_from_settings(settings)
    #import cProfile 
    #cProfile.run('main()')
    print "* Importing into database '%s'" % conn_params.get("database",
                                                             "unknown")
    if not options.log_to_stderr:
        print "* Logging into '%s'" % log_filename
        stat_out_stream = open(log_filename, 'a')
    else:
        stat_out_stream = sys.stderr
    print
    import_dirs(conn_params, filenames, quiet=(not options.timing_stats),
                show_progress=True, stat_out_stream=stat_out_stream)

if __name__ == "__main__":
    main()