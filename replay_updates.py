#!/usr/bin/env python
#
# This software is covered by a BSD license:
#
# Copyright (c) 2011-2012, CZ.NIC, z.s.p.o. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
#  * Neither the name of CZ.NIC, z.s.p.o. nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This script is designed to stress-test the update routines by replaying
the updates from selected XML files in real time.
"""

# built-in modules
import os
import sys
import datetime
import time
import logging

# local imports
from update_from_xml import DscXmlImporter
from dsc_storage.psql.connection import create_connection_manager_from_settings
from dsc_storage.psql.storage import StoragePSQL

def create_list_of_xml_files(directories):
    """
    creates a dictionary {time => {server => filename}} where server is a tuple
    of parent name and server name
    """
    result = {}
    for directory in directories:
        for top, _dirs, files in os.walk(directory, followlinks=False):
            if os.path.split(top)[1] == "dscdata":
                # find server name
                abstop = os.path.abspath(top)
                parent_name = None
                server_name = None
                for i in range(5):
                    server_name = parent_name
                    abstop, parent_name = os.path.split(abstop)
                server = (parent_name, server_name)
                # process files
                abstop = os.path.abspath(top)
                for fname in files:
                    if "dscdata.xml" in fname:
                        timepoint = int(fname.split(".")[0])
                        if timepoint not in result:
                            result[timepoint] = {}
                        result[timepoint][server] = os.path.join(abstop, fname)
    return result
            

def find_start_time(data):
    return min(data.keys())

def has_more_data(data, timepoint):
    for key in data:
        if key >= timepoint:
            return True
    return False

def print_stats(stats):
    print "----- STATISTICS -----"
    # get all servers
    servers = set()
    for row in stats.values():
        servers |= set(row)
    # print the header
    print "Start", "Time", "Count"
    for server in sorted(servers):
        print "/".join(server),
    print
    # print the stats
    for start_time in sorted(stats):
        row = stats[start_time]
        total = sum(row.values())
        print start_time, "%.2f" % total, len(row),
        for server in sorted(row):
            print "%.3f" % row[server],
        print

def run(importer, data, start_time, end_time=None):
    stats = {}
    try:
        while has_more_data(data, start_time):
            logging.info("Starting time %s", start_time)
            cur_data = data.get(start_time)
            cur_stats = {}
            t1 = time.time()
            if cur_data:
                for server, filename in cur_data.iteritems():
                    logging.debug("Doing server '%s' (%s)",
                                  "/".join(server), filename)
                    t2 = time.time()
                    importer.import_xml_file(filename)
                    diff2 = time.time() - t2
                    cur_stats[server] = diff2
            else:
                logging.warn("No data for %s", start_time)
            diff1 = time.time() - t1
            logging.info("Spent time %.1f s (%.1f %% of window)",
                          diff1, diff1/0.6)
            stats[start_time] = cur_stats
            start_time += 60
            if start_time > end_time:
                break
    except KeyboardInterrupt:
        logging.error("KEYBOARD INTERRUPT")
    except Exception as e:
        logging.error("ERROR %s", e)
        
    print_stats(stats)
    
    
def main():
    """
    The main code of this script
    """
    os.environ['DJANGO_SETTINGS_MODULE'] = 'dsc.dsc.settings'
    from dsc.dsc import settings
    conn = create_connection_manager_from_settings(settings)
    storage = StoragePSQL(conn)
    importer = DscXmlImporter(storage)
    
    # parse options
    from optparse import OptionParser
    opsr = OptionParser(usage="python %prog [options] input-dir(s)")
    opsr.add_option("-s", "--start-time", action="store",
                    dest="start_time", default=None,
                    help="Override the start time - instead of using the " 
                         "lowest value found in input data, use the supplied "
                         "value")
    opsr.add_option("-e", "--end-time", action="store",
                    dest="end_time", default=None,
                    help="End replay at specific time (inclusive) instead of "
                         "running as long as there is input data.")
    opsr.add_option("-c", "--cycles", action="store",
                    dest="cycles", default=0,
                    help="Similar to end_time. Specifies the number of cycles "
                         "(minutes) to be imported relative to start time. "
                         "-e and -c cannot be used at the same time.")
    opsr.add_option("-i", "--info", action="store_true",
                    dest="info", default=False,
                    help="Only load the input data and print out some info "
                         "on it. It can be useful to find out the input values "
                         "for later customized runs.")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Print extra info about what the script is doing")
    (options, args) = opsr.parse_args()
    
    # check input
    if len(args) == 0:
        opsr.error("You must supply a directory in which to search for input "
                   "data.")
    if options.end_time and options.cycles:
        opsr.error("You cannot use -e and -c options simultaneously.")
    # setup logging
    level = logging.INFO
    if options.verbose:
        level = logging.DEBUG
    logging.basicConfig(level=level)
    # get input data
    data = create_list_of_xml_files(args)
    # process the arguments
    # start time
    if options.start_time:
        try:
            start_time = int(options.start_time)
        except ValueError:
            logging.error("Start time must be an integer, not %s",
                          options.start_time)
            sys.exit(100)
    else:
        start_time = find_start_time(data)
    # end time
    if options.end_time:
        try:
            end_time = int(options.end_time)
        except ValueError:
            logging.error("End time must be an integer, not %s",
                          options.end_time)
            sys.exit(101)
    elif options.cycles:
        try:
            cycles = int(options.cycles)
        except ValueError:
            logging.error("Cycles must be an integer, not %s", options.cycles)
            sys.exit(102)
        end_time = start_time + 60*(cycles-1)
    else:
        end_time = None
    # print out some stats
    logging.info("Start time: %s", start_time)
    logging.info("End time: %s", end_time)
    # check end_time
    if end_time and end_time < start_time:
        logging.error("Sorry, but end time is lower than start time.")
        sys.exit(103)
    if options.info:
        # we do no do a run
        # and we already printed out the info, so there is nothing more to do
        pass
    else:
        # run the code
        run(importer, data, start_time, end_time=end_time)

if __name__ == '__main__':
    main()