from dsc.dsc import settings
from dsc_storage.psql import connection
import time
import logging
logging.basicConfig(level=logging.DEBUG)

def test_timespan_sync():
    from dsc_storage.psql.db_maintenance import sync_timespans
    conn = connection.create_connection_manager_from_settings(settings)
    t = time.time()
    sync_timespans(conn)
    print time.time() - t
    
test_timespan_sync()